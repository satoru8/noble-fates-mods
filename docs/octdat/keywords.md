# Octdat Keywords Reference

This reference guide outlines the key properties available for defining and manipulating objects in mod development.

## Properties

- **`id`**: The unique ID assigned to the object.

- **`type`**: Specifies the type of the object. This property is linked to the C# class name used in the game's code, determining the object's behavior and attributes.

- **`alias`**: Defines an alternative reference name for the object. During the game's load process, any reference to the alias is replaced with the corresponding `id`.

- **`appendonly`**: A flag indicating that the object definition is intended to update an existing object rather than create a new one. This is particularly useful for mods that aim to enhance or modify objects from other mods, facilitating compatibility and cross-mod support.

- **`abstract`**: Used to declare a base object. This keyword sets the object's `id` to be inheritable only, serving as a template for other objects.

- **`inherit`**: Allows an object to inherit properties from another object, identified by its `id`. This mechanism supports the creation of object variants and extensions without redundant definitions.



## List Modifiers

- **`$clear`**: A keyword to clear an array list within the object's definition. This can be used to reset or remove default elements before adding new ones, ensuring a clean slate for modifications.

- **`?([property] == [value])`**: A list property modifier. The property value will be set to the new values.

- **`-([property] == [value])`**: Removes the property value from the list. You can then set a new one if needed.


## Examples

````
{
	id Oct.Characters.Humanoids.Elf
	type CharacterType
	
	attributePotentialDeltas = 
	[
		$clear
		{
			type CharacterTypeAttributePotentialDelta
			attribute = Ranged
			delta = 1
		}
		{
			type CharacterTypeAttributePotentialDelta
			attribute = Foraging
			delta = 1
		}
	]
}
````
  
````
stats =
[
    ?(name == Damage) {
        type InstancedItemStat
        min = 22
        max = 50
    }
    -(name == FoodPerDay)
    {
        id Fantasy.Creatures.Cyclops.FoodPerDay
        type StatType    
        inherit Oct.Characters.Stats.FoodPerDay
        initial = -10
    }
]
````