This is the full example mod used in the modding section of the guide.

<a href="https://gitlab.com/satoru8/noble-fates-mods/-/tree/main/OctDats/Example" target="_blank">Click here to view the example in the gitlab repo</a>

```
// Author: Satoru
// Version: 1.0
// Description: Example mod from the modding guide.

{
	id MyMod.Research
	type ResearchEntry
	abstract
	
	verb = to Craft
	skill = Crafting
}
{
	id MyMod.Research.NewStuff
	type ResearchEntry
	inherit MyMod.Research
	
	name = Mod Items
	description = Makes some fancy wood
	category = true
	tierOffset = 1
}
{
	id Oct.Settings.Game.RaidChance
	type SliderSettingDefinition
	
	max = 5
}
{
	id Oct.Characters.Needs.Light
	type CharacterStatNeedType
	inherit Oct.Characters.Needs.Need
	
	unmetOpinion = 0
	decreasePerHour = 0
	subceededOpinion = 0
}
{
	id MyMod.Items.FancyWood
	type StackedItemType
	inherit Oct.Items.Stackable
	
	name = Fancy Wood
	itemClass = StackedItem
	category = <Oct.Items.Categories.Resources>
	stackLimit = 12
	indoorDecayAfter = 24y
	outdoorDecayAfter = 12y
	value = 2.50
	showCount = true
	dropSound = CommonDrop
	
    // We use the relative path to the games asset.
	prefab = 
	{
		type Prefab
		path = /Art/Items/Wood/Wood.fbx
		name = Wood
		behavior = StackedItemBehavior
	}
}
{
	id MyMod.Recipe.FancyWood
	type ItemRecipe
	
	name = FancyWood
	type = <MyMod.Items.FancyWood>
	commandType = CraftItemCommand
	progressMultiplier = .9
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = .1
	skillCurve = .3
	yield = 2
	batch = 1
	
	craftAt = 
	[
		<Oct.Props.CraftingTable>
	]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 4
		}
	]
	
	researchEntry = <MyMod.Research.FancyWood>
    // You can "link" a research entry into the recipe.
    // or include it in the recipe directly.

    // 	{
	// 	id MyMod.Research.FancyWood
	// 	type ResearchEntry
	// 	inherit MyMod.Research
		
	// 	name = FancyWood
	// 	description = Fancy Wood
	// 	category = false
	// 	prerequisite = <MyMod.Research.NewStuff>
	// 	tooltipProvider = <MyMod.Recipe.FancyWood>
	// 	tierOffset = 1
	// 	free = true
	// }
}
{
	id MyMod.Research.FancyWood
	type ResearchEntry
	inherit MyMod.Research
	
	name = FancyWood
	description = Fancy Wood
	category = false
	prerequisite = <MyMod.Research.NewStuff>
	tooltipProvider = <MyMod.Recipe.FancyWood>
	tierOffset = 1
	free = true
}
```