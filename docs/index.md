# Welcome!
The guide is a WIP. Some parts might not be finished yet.  
If you have comments or suggestions, please do not hesitate to <a href="https://gitlab.com/satoru8/noble-fates-mods/-/issues" target="_blank">open an issue</a> on Github.  
You can also contact me on discord: <a href="https://discordapp.com/channels/@me/135913674538745856/" target="_blank">spicy.katsu</a>.
  
This is an overall guide for modding Noble Fates.  
The NF User Echos are a good place to start. It will give you a basic understanding of how mods work.

<h3>Noble Fates Modding User Echos</h3>

- <a href="https://noblefates.userecho.com/knowledge-bases/2/articles/746-modding-noble-fates" target="_blank">Modding Noble Fates</a>
- <a href="https://noblefates.userecho.com/knowledge-bases/2/articles/1213-uploading-mods-to-the-noble-fates-steam-workshop" target="_blank">Uploading Mods to the Steam Workshop</a>
- <a href="https://noblefates.userecho.com/knowledge-bases/2/articles/1311-mod-version-compatibility" target="_blank">Mod Version Compatibility</a>
- <a href="https://noblefates.userecho.com/knowledge-bases/2/articles/1345-modding-the-noble-fates-source-code" target="_blank">Modding the Noble Fates Source Code</a>