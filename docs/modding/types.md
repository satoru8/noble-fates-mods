## Intro

In the guide, we've touched upon types, but let's delve deeper into them. Understanding types is pivotal for mod development, as they define the behavior and characteristics of objects within the game.

Think of octdat objects as extensions of the C# classes in the games code. The `type` assigns that class to be used by the IDs in the octdats.

In these examples we will be using JetBrains dotPeek and Visual Studio, feel free to use whatever you prefer!

Noble Fates Assembly-CSharp.dll path: `..\SteamLibrary\steamapps\common\Noble Fates\Noble Fates_Data\Managed\Assembly-CSharp.dll`

### Exploring the Assembly

- In dotPeek we can open the `Assembly-CSharp.dll` and look at the `WorkbenchPropType` class.

- In Visual Studio we add it as a reference in the project.

<video width="500" height="320" src="../../assets/types1.mp4" controls></video>

In this example we have a prop type from Advanced Forge mod. The type is `WorkbenchPropType`. When we look at this the first thing you notice is it seems empty. We can see that it inherits from another class. So lets follow it back.

Not all properties will be used in the octdats all the time. Learning to check the classes and see what else might be possible or set as a default can be quite helpful.