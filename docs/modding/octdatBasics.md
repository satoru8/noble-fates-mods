# Octdat Basics

## Understanding IDs

IDs play a crucial role in mod development, serving as unique identifiers for octdat objects within your mod. While you have the freedom to create IDs as you see fit, it's essential to keep them simple, descriptive, and easy to read.

<h3>Naming Convention Example</h3>

Here's a suggested naming convention for your IDs:
- Start with your mod name, such as 'Alchemy'.
- Followed by the item category, such as 'Props' or 'Items'.
- Specify the item name, and optionally append a tier or naming convention if needed.

**Examples:**  

- `Alchemy.Props.Chair.Stone`  
- `Alchemy.Props.Chair.Wood`  
- `Alchemy.Props.WoodChair`  

By following this convention, you can easily reference specific sections or items in your mod and avoid potential conflicts with other mods using similar IDs.

## Utilizing Inherits

Inherits are a powerful feature that allows you to minimize repetition and efficiently manage your mod's content. While not mandatory, they're highly recommended for maintaining a clean and organized mod structure.  

Inherits enable you to inherit properties & attributes from another ID. Let's explore some examples to illustrate their usage:

#### Example 1: Buff Inheritance
```
{
    id Gem.Items.BuildingGem.T1
    type InstancedItemType
    inherit Gem.Items.Buff.Building.T1
}
```
In this example, the `Gem.Items.BuildingGem.T1` ID inherits buffs from another ID `Gem.Items.Buff.Building.T1`, ensuring consistency and simplifying the management of gem properties.

### Example 2: Character Stat Need

Here we will change a few properties to the `Oct.Characters.Needs.Light` ID. This will be used in the next section while making your first mod.

```
{
    id Oct.Characters.Needs.Light
    type CharacterStatNeedType

    unmetOpinion = 0
    decreasePerHour = 0
    subceededOpinion = 0
}
```

<a href="../../assets/example2.png" target="_blank"><img src="../../assets/example2.png" alt="Character Stat Need"></a>
<a href="../../assets/example2.png" target="_blank"><img src="../../assets/example21.png" alt="Character Stat Need"></a>

## Abstract IDs

When exploring the `Oct.Characters.Needs.Need` ID, you may notice that it's labeled as 'abstract'. 

### Understanding Abstract IDs

An abstract ID signifies that it's an abstract object, designed not to be referenced directly. Instead, its purpose is to serve as a blueprint or template that can be inherited by other IDs.

### Why Use Abstract IDs?

Abstract IDs provide a way to organize your mod's content. By defining common attributes and behaviors in abstract IDs, you can ensure consistency across your mod and simplify the management of shared properties.

### Example Usage

For instance, consider the `Oct.Characters.Needs.Need` ID. While it may not be directly referenced in your mod, it serves as a foundational element for defining character needs. Other IDs, such as specific character needs like `Oct.Characters.Needs.Light` or `Oct.Characters.Needs.Food`, can then inherit from this abstract ID, inheriting its properties and behaviors.