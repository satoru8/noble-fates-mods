
# Modding Setup Guide

## Editor Recommendation

For an optimal modding experience, we recommend using [Visual Studio Code](https://code.visualstudio.com). 
We've included a Noble Fates Octdat extension for Visual Studio Code. You can easily find it by searching for 'octdat' in the extensions window.  

Get the NF Octdat extension: <a href="https://marketplace.visualstudio.com/items?itemName=Satoru.NF-OctDat" target="_blank"><img src="https://img.shields.io/badge/NF%20Octdat-Development-green.svg" alt="NF Octdat" ></a>


**Choosing the Right Theme**  
By default, we recommend using the Default Dark+ theme for Visual Studio Code. The NF Octdat Extension was built around this theme. However, you also have the option to use NF Dark theme, which is a high contrast theme. You can switch between themes like you normally would in Visual Studio Code by pressing `CTRL+K CTRL+T`.

<video width="500" height="320" src="../../assets/vscext.mp4" controls></video>

## Workspace
Open the Noble Fates game folder in your steam install location. Create a "Mods" folder if you don't have one already. 

- Create another folder inside the Mods folder for your mod.
- Create a new text file and rename it MyMod.octdat, or whatever your mod name will be.

**Don't Forget to Save Your Workspace**

To ensure your workspace settings are preserved, go to `File > Save Workspace As…`  

<video width="500" height="320" src="../../assets/workspace.mp4" controls></video>


## Development Branch Selection

For mod development, it's recommended to work with the development branch of Noble Fates. To access it:

1. Open your Steam library.
2. Right-click on Noble Fates and select `Properties`.
3. Go to the `Betas` tab.
4. From the dropdown, select `Development`.

<a href="../../assets/dev.png" target="_blank"><img src="../../assets/dev.png" alt="Selecting the Development Branch"></a>

The Development branch of Noble Fates introduces several command-line options designed to enhance and streamline your modding workflow. These options can be extremely helpful in bypassing usual game startup sequences or tweaking the game environment for testing purposes. For convenience, consider creating desktop shortcuts with these options.

Here's a breakdown of available command-line options and their functions:

- ` -skipintro `: Bypasses the introductory sequence when starting a new game.
- ` -skipmenu `: Skips the Main Menu, allowing for a quicker game start.
- ` -smallworld `: Initiates the game with small testing maps, ideal for focused mod testing.
- ` -nohistory `: Omits world history generation for faster load times.
- ` -unhide `: A developer option to reveal caverns without the need for in-game discovery.
- ` -mapstats `: Outputs map statistics after generation, useful for analyzing map characteristics.
- ` -mapgen `: Provides detailed map generation data, aiding in understanding map layouts.
- ` -moddev `: Combines `-skipintro`, `-skipmenu`, `-smallworld`, and `-nohistory` for an optimized mod development startup.

Implementing these commands can significantly reduce the time spent on repetitive tasks, allowing you to focus more on the creative aspects of mod development.

## Helpful Mods

The Secret Menu mod offers a convenient shortcut to a variety of debug options. With the option to enable full debug capabilities same as those found in the game's development branch. Modders can debug and refine their creations without the need to switch to a development environment.  

<a href="https://steamcommunity.com/sharedfiles/filedetails/?id=3133167401&searchtext=secret" target="_blank">Secret Menu Steam Workshop Link</a>