# Making New Items

Creating new items for your mod can be as straightforward or as intricate as you like. If you're comfortable with 3D modeling software, feel free to craft your own models and assets. However, if you're new to this, don't worry! You can repurpose existing in-game assets, utilize free online resources, or even collaborate with other developers and modders to bring your ideas to life.

### Using Noble Fates Extension for VSC

For those using Visual Studio Code, the Noble Fates extension can help streamline your workflow. Press `Ctrl + Space`, select `newitem`, and let the extension generate a template for you. You can then customize it by typing the name of your mod and navigating through the item properties using the `TAB` key.

### Manual Approach

Alternatively, you can manually add new items to your mod by copying and modifying existing item files. Let's start by locating the `Wood.octdat` file in your workspace (`OctDats` > `Items` > `Stackable` > `Wood.octdat`). Copy the contents of this file to your mod file and adjust the ID and name of your new item as needed. 

**Sample New Item Configuration**
```
{
	id MyMod.Items.FancyWood
	type StackedItemType
	inherit Oct.Items.Stackable
	
	name = Fancy Wood
	itemClass = StackedItem
	category = <Oct.Items.Categories.Resources>
	stackLimit = 12
	indoorDecayAfter = 24y
	outdoorDecayAfter = 12y
	value = 2.50
	showCount = true
	dropSound = CommonDrop
	
    // We use the relative path to the games asset.
	prefab = 
	{
		type Prefab
		path = /Art/Items/Wood/Wood.fbx
		name = Wood
		behavior = StackedItemBehavior
	}
}
```

### Crafting Recipes

Once you've added your new item, it's time to put it to use. You can create crafting recipes to craft your item using the Noble Fates extension or manually. Here is an example of a crafting recipe for your new item.

```
{
	id MyMod.Recipe.FancyWood
	type ItemRecipe
	
	name = FancyWood
	type = <MyMod.Items.FancyWood>
	commandType = CraftItemCommand
	progressMultiplier = .9
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = .1
	skillCurve = .3
	yield = 2
	batch = 1
	
	craftAt = 
	[
		<Oct.Props.CraftingTable>
	]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 4
		}
	]
	
	researchEntry = <MyMod.Research.FancyWood>
    // You can "link" a research entry into the recipe.
    // or include it in the recipe directly.

    // 	{
	// 	id MyMod.Research.FancyWood
	// 	type ResearchEntry
	// 	inherit MyMod.Research
		
	// 	name = FancyWood
	// 	description = Fancy Wood
	// 	category = false
	// 	prerequisite = <MyMod.Research.NewStuff>
	// 	tooltipProvider = <MyMod.Recipe.FancyWood>
	// 	tierOffset = 1
	// 	free = true
	// }
}
```

### Research Entries
Research entries allow players to unlock new crafting recipes. You can define custom research entries or use existing ones. Here is an example of a research entry for your new item. For this example we can set free = true.

```
{
	id MyMod.Research.FancyWood
	type ResearchEntry
	inherit MyMod.Research
	
	name = FancyWood
	description = Fancy Wood
	category = false
	prerequisite = <MyMod.Research.NewStuff>
	tooltipProvider = <MyMod.Recipe.FancyWood>
	tierOffset = 1
	free = true
}
```
Give the mod a try! Assuming theres no errors, you should be able to craft your new item and see it in-game.

Congratulations! You've learned how to add new items and crafting recipes to your mod. With these tools and techniques, you can continue to expand your modding capabilities.