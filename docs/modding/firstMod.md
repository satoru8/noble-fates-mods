
# Making Your First Mod

Congratulations on starting your modding journey! Let's walk through the process of creating your first mod step by step.

## Setting Up Your Mod

1. Navigate to your new mod folder, `MyMod`.
2. Create or open the `MyMod.octdat` file.

If you like, you can add some basic metadata to your mod file:

```
// Author: Sato
// Version: 1.0
// Description: New mod
```

## Editing Game Settings

1. Open the OctDats folder in your workspace.
2. Navigate to Settings > GameSettings.octdat.
3. 
Here's an example of the game’s Raid Chance settings slider:
```
{
    id Oct.Settings.Game.RaidChance
    type SliderSettingDefinition
    name = Raid Chance
    order = -900
    category = <Oct.Settings.Game>
    min = 0
    max = 2
    step = .1
    unit = x
    defaultValue = 1
}
```
The ID is specific to each object. If you want to edit an existing value in the game, use the same ID and TYPE. You only need to add the property/value you wish to change, which will be added to the current ID in the game.

## Implementing Your Mod

Back in your MyMod.octdat paste the following content. Set the value to whatever you want:

```
{
    id Oct.Settings.Game.RaidChance
    type SliderSettingDefinition

    max = 5
}
```

Save the file and load the game.

To check this setting, load into a new or saved game, then navigate to Options > Game  
Congratulations on your first mod!