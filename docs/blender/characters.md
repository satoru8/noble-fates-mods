# Character Creation Guide

This part of the guide is dedicated to helping you craft unique character mods.

### Step 1: Download the Biped Model

- Begin by **downloading the biped model**. This file is available in the **mod-dev chat**, pinned in the upper right corner.
- Once downloaded, open it on your desktop to start with the process illustrated below.

<video width="500" height="320" src="../../assets/step1.mp4" controls></video>

### Step 2: Prepare Your Workspace

- Navigate to the `+` at the top right, select `General`, and then `Layout`.
- Choose the **armature** from the right-hand side menu, copy it, and then create a new Blender file via `FILE -> NEW -> GENERAL`.
- Paste the armature into your new file and remove any unnecessary items from the scene to clear your workspace.

<video width="500" height="320" src="../../assets/step2.mp4" controls></video>

### Step 3: Import the Body Model

- Go to `FILE -> IMPORT -> FBX`, and open the body FBX file.  
Body Model: `..\steamapps\common\Noble Fates\Noble Fates_Data\StreamingAssets\Art\Characters\Humanoid\Bodies`
- Before importing, set the **scale to 100**. This step provides you with a base body, which can be customized or used as a guideline for your mod.

<video width="500" height="320" src="../../assets/step3.mp4" controls></video>

### Step 4: Clean Up Materials

- To remove materials correctly, navigate through the illustrated tabs to the materials section and delete all pre-existing materials.
- Rename the default material to something more descriptive, like `Skin`, and ensure to delete any vertex data to avoid future issues.

<video width="500" height="320" src="../../assets/step4.mp4" controls></video>

<video width="500" height="320" src="../../assets/step4-2.mp4" controls></video>


### Step 5: Adding a Human Head (Optional)

- For better reference and detailing, you can upload a human head model as shown in the guide.

### Step 6: Linking Materials

- After designing your headpiece, connect the materials according to the instructions provided in the visuals below.

<a href="../../assets/step6.png" target="_blank"><img src="../../assets/step6.png" width="500" alt="Linking Materials"></a>

### Step 7: Adding Facial Features with Meshes

<video width="500" height="320" src="../../assets/step7.mp4" controls></video>

It's time to infuse your character with personality by adding diverse meshes for facial features. 

To facilitate your modeling process, employing the `type MirroredStaticCharacterCosmetic` in the octdats for symmetrical elements such as eyes is recommended. This allows you to fine-tune one side, with the changes being automatically reflected on the opposite side. Don't hesitate to explore various facial configurations.  However, be mindful of the interaction between these features and in-game elements like helmets.

- **Systematic Naming**: Organize your work by adopting a clear naming strategy for your meshes (e.g., `Eyes01`, `Mouth01`, `Ears01`). This organizational tactic proves invaluable when adjusting elements in the octdats, as it simplifies the process of making numerical adjustments or applying more descriptive names (such as `DragonMouth`) at a later stage.