### New Items

In this section we will create a simple object with a material. Then assign it a texutre and bring it into the game as standard stacked item.

For this we will use the default cube. Feel free to delete the camera and light.

1. Change the scale to match the size of the item you want to make. If you need a reference you can import any fbx from the game or other mods. 

2. Move the object into place depending on the size of the item.

3. Create a new material and assign it to the object or section of the object you want. We will use the default material. Renaming to MyMat.

4. Apply all transofrms.

<video width="500" height="320" src="../../assets/blenderbasics1.mp4" controls></video>

### Octdat
Next lets create a simple octdat file.

<video width="500" height="320" src="../../assets/blenderbasics2.mp4" controls></video>
