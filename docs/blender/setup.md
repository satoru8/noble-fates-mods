## **Export**

Depending on what you want to export, you can use the following steps.  
For basic items and props you can use Selected Objects & Mesh only.

These may not always be exactly what you want but its a good idea to save these in blender for easily setting your import/export settings.

**Animations**

- Remove the body/head
- Mark the new actions F (force user)
- Kill any NLA tracks

**Meshes**

- Apply transforms

**Export settings**

-  FBX 7.4 binary
- Scale: .01
- Apply Scalings: FBX All
- Check Apply Transforms !EXPERIMENTAL!

**Armatures**

- Uncheck Add Leaf Bones
- Primary Bone Axis: Y
- Secondary Bone Axis: -X

**Animation**

- Uncheck Key All Bones
- Uncheck NLA Strips

<a href="../../assets/export.png" target="_blank"><img src="../../assets/export.png" alt="Export"></a>

## **Import**

Mainly we want to make sure to import at 100x scale.  
These setting may be different depending on what you are importing.

<a href="../../assets/import.png" target="_blank"><img src="../../assets/import.png" alt="Import"></a>