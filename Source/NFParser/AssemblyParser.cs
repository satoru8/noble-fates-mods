﻿using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AssemblyParser
{
    public class ClassInfo
    {
        public string ClassName { get; set; }
        public List<string> Properties { get; set; }
       // public bool IsUnityClass { get; set; } // Potential addition
    }

    public List<ClassInfo> AnalyzeAssembly(string assemblyPath)
    {
        var assembly = Assembly.LoadFrom(assemblyPath);

        var classes = assembly.GetTypes()
            .Where(type => type.IsClass && type.IsPublic)
            .Select(type => new ClassInfo
            {
                ClassName = type.Name,
                Properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                 .Select(p => p.Name)
                                 .ToList(),
               // IsUnityClass = type.IsSubclassOf(typeof(MonoBehaviour)) // Check Unity derivation
            })
            .ToList();

        return classes;
    }
}