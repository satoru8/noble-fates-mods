﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CreatureProfileType : FacetType
{
    public string title { get; private set; } = (LocalizedString)"";
    public string description { get; private set; } = (LocalizedString)"";
    public float descFontSize { get; private set; } = 14f;
    public Texture profileIcon { get; private set; }
    public float profileIconSize { get; private set; } = 150f;
    public string traitTitle { get; private set; } = (LocalizedString)"Traits";
    public List<string> traits { get; private set; }
    public float traitFontSize { get; private set; } = 14f;
    public string profileButton { get; private set; } = (LocalizedString)"Profile";
    public TextAlignmentOptions alignment { get; private set; } = TextAlignmentOptions.Center;

    public CreatureProfileType(OctDatGlobalInitializer initializer)
        : base(initializer)
    {
    }
}