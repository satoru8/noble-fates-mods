﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class FantasyCreatureMoment : Moment
{
    private ScriptableConversationInstance conversation;
    private bool spawned;

    public FantasyCreatureMomentType creatureType => base.type as FantasyCreatureMomentType;

    public Notification notification { get; private set; }

    public FantasyCreatureMoment(MomentType type, OctScriptContext context)
      : base(type, context)
    {
    }

    public FantasyCreatureMoment(OctSaveInitializer initializer)
      : base(initializer)
    {
    }

    protected override void Discovered()
    {
        base.Discovered();
        foreach (Character participant in participants)
        {
            participant.PinPerceived("moment");
            participant.ShowHealthBar("moment");
        }
        Manager<BreachManager>.Instance.IncrementMomentsSinceLast();
        Manager<ToastManager>.Instance.QueueToast(creatureType.bigToast, priority: 1f);
        notification = new Notification(creatureType.bigToast, map);
        notification.AddOnClick(new NotificationDelegate(((Moment)this).CycleParticipants));
        ManagerBehavior.Instance.InCombat();
        if (Manager<CameraManager>.Instance.captureMode)
            return;
        Manager<TimeManager>.Instance.AutoPause();
    }

    protected override void Ended()
    {
        if (notification != null)
        {
            notification.Destroy();
            notification = (Notification)null;
        }
        base.Ended();
    }

    public override bool HasTruesight(Character character) => character.type.genus == CharacterType.Genus.Animal;

    public override bool PreventDispatch(Character character, out string reason)
    {
        Pawn pawn;
        if ((pawn = character.pawn) == null || !Manager<KingdomManager>.Instance.IsPlayerRuler(pawn))
            return base.PreventDispatch(character, out reason);
        reason = creatureType.preventDispatch;
        return true;
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);
        if (discovered)
            ManagerBehavior.Instance.InCombat();
        if (!spawned)
        {
            if (conversation != null && !conversation.destroyed)
                return;
            FantasyCreatureMomentType type1 = base.type as FantasyCreatureMomentType;
            Func<TilePos> func = (Func<TilePos>)null;
            if (type1.spawn == FantasyCreatureMomentType.Spawn.NearHome)
            {
                bool underground = false;
                bool onRoof = false;
                NearHomeQuery query = new NearHomeQuery(Manager<FactionManager>.Instance.homeFaction, pred: new Func<TilePos, bool>(QueryPred));
                func = new Func<TilePos>(Spot);

                bool QueryPred(TilePos potTile) => (onRoof || !(Manager<PathingManager>.Instance.GetFloorProp((Actor)null, potTile, BlockingQuery.Movement) is RoofPropActor)) && (underground || potTile.y >= Manager<TerrainManager>.Instance.GetHeight(potTile.x, potTile.z));

                TilePos Spot()
                {
                    underground = (double)UnityEngine.Random.value < 0.125;
                    onRoof = (double)UnityEngine.Random.value < 0.125;
                    return query.Spot(Mathf.RoundToInt(Mathf.Pow(UnityEngine.Random.value, 3f) * 5f));
                }
            }
            else if (type1.spawn == FantasyCreatureMomentType.Spawn.Underground)
            {
                UndergroundAreaQuery query = new UndergroundAreaQuery();
                func = new Func<TilePos>(Spot);

                TilePos Spot() => query.Spot();
            }
            if (context.GetValue(type1.castingKey) is Dictionary<CharacterType, int> dictionary)
            {
                bool flag = (double)UnityEngine.Random.value <= 0.5;
                foreach (KeyValuePair<CharacterType, int> keyValuePair in dictionary)
                {
                    for (int index = 0; index < keyValuePair.Value; ++index)
                    {
                        TilePos tile = func();
                        if (!(tile == TilePos.Invalid))
                        {
                            float num = (float)(0.25 + (double)OctoberMath.DistributedRandom(0.0f, 1f) * 0.5) + (float)((double)Mathf.Pow(OctoberMath.DistributedRandom(0.0f, 1f), 3f) * (double)(keyValuePair.Value - 1) * 0.125);
                            TrySpawn(keyValuePair.Key, tile, num * keyValuePair.Key.lootValue, !flag || (double)UnityEngine.Random.value > 0.25);
                        }
                    }
                }
                spawned = true;
            }
            if (!spawned)
            {
                float num1 = creatureType.difficulty != null ? Manager<SettingsManager>.Instance.GetGameFloatValue(creatureType.difficulty.id) : 1f;
                float num2 = Mathf.Clamp(Mathf.Round(Mathf.Lerp((float)Manager<KingdomManager>.Instance.playerKingdom.home.present.Count, (float)Manager<KingdomManager>.Instance.playerKingdom.members.Count, 0.5f) * Manager<MomentManager>.Instance.difficulty * num1 * OctoberMath.DistributedRandom(0.5f, 1f)), 1f, creatureType.maxBudget);
                float num3 = num2;
                while ((double)num3 >= 1.0)
                {
                    CharacterType type2 = (CharacterType)null;
                    float num4 = 1f;
                    if (type1.creatures != null)
                    {
                        float num5 = 0.0f;
                        foreach (MonsterMomentMonsterCost monster in type1.creatures)
                        {
                            if ((double)monster.cost > 0.0 && (double)monster.cost <= (double)num3)
                            {
                                float num6 = 1f / monster.cost;
                                num5 += num6;
                                if ((double)UnityEngine.Random.value * (double)num5 <= (double)num6)
                                {
                                    type2 = monster.monsterType;
                                    num4 = monster.cost;
                                }
                            }
                        }
                    }
                    else
                        type2 = type1.monsterType;
                    if (type2 != null)
                    {
                        num3 -= num4;
                        TilePos tile = func();
                        if (!(tile == TilePos.Invalid))
                        {
                            float num7 = (float)(0.25 + (double)OctoberMath.DistributedRandom(0.0f, 1f) * 0.5) + (float)((double)Mathf.Pow(OctoberMath.DistributedRandom(0.0f, 1f), 3f) * (((double)num2 - (double)num4) / (double)num4) * 0.125);
                            TrySpawn(type2, tile, num7 * type2.lootValue, true);
                        }
                    }
                    else
                        break;
                }
            }
            spawned = true;
        }
        if (!spawned)
            return;
        if (notification != null)
        {
            if (notification.actor != null)
            {
                if (notification.actor is Character actor)
                {
                    if (actor.dead)
                        notification.SetActor((Actor)null);
                    else if (actor.offMap)
                        notification.SetActor((Actor)null);
                }
                else
                    notification.SetActor((Actor)null);
            }
            if (notification.actor == null)
                CycleParticipants(notification);
        }
        bool flag1 = true;
        foreach (Character participant in participants)
        {
            Character monster = participant;
            if (!monster.dead && (!monster.offMap || monster.portal != null))
            {
                flag1 = false;
                Faction homeFaction;
                if (monster.perception != null && (homeFaction = Manager<FactionManager>.Instance.homeFaction) != null)
                    homeFaction.VisitMembers(new Action<Character>(PerceiveTarget), FactionMembers.MembersSerfsAndPrisoners);
            }

            void PerceiveTarget(Character potTarget)
            {
                if (!potTarget.onCurrentMap || potTarget.dead)
                    return;
                monster.perception.PerceiveCharacter(potTarget);
            }
        }
        if (!flag1)
            return;
        if (notification != null)
        {
            Manager<ToastManager>.Instance.QueueToast(creatureType.victory, ToastStinger.Positive, 1f);
            Manager<FactionManager>.Instance.playerFaction.Celebrate();
        }
        End();

        bool TrySpawn(CharacterType type, TilePos tile, float loot, bool portalIn)
        {
            Character character = Manager<CharacterManager>.Instance.GenerateCharacter(type);
            character.SetLootValue(loot);
            portalIn = false;
            if (portalIn)
            {
                float begin = (float)(7.5 * (0.5 + (double)UnityEngine.Random.value));
                float end = begin + (float)(60.0 * (1.0 + 0.125 * (0.5 + (double)UnityEngine.Random.value)));
                character.PortalIn((Vector3)tile, begin, end);
            }
            else if (!character.TryPlaceOnMap((Vector3)tile, 3f))
            {
                character.Destroy();
                return false;
            }
            AddParticipant(character);
            return true;
        }
    }
}