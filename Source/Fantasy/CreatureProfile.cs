﻿using TMPro;
using UnityEngine;

public class CreatureProfile : CharacterFacet
{
    public CreatureProfileType fcfType => type as CreatureProfileType;
    private UIManager uiManager = UIManager.Instance;
    private UIWindowBehavior profileParent;
    private UIWindowBehavior profileWindow;

    public CreatureProfile(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    public CreatureProfile(Character character, CreatureProfileType type)
        : base(character, type)
    {
    }

    public void ShowProfileWindow()
    {
        profileParent = (UIWindowBehavior)uiManager.AddModalPopupWindow().SetAlphaScale(1f);
        profileWindow = (UIWindowBehavior)uiManager.AddWindow(profileParent).SetSprite(ToolsUIAssets.Instance.window).SetHeight(420).SetWidth(550);

        UITextBehavior profileTitle = uiManager.AddLabel(profileWindow, (ResolvedString)fcfType.title);
        profileTitle
            .SetFontSize(36)
            .SetFontColor(ManagerBehavior.Instance.titleTextColor)
            .SetAlignment(TextAlignmentOptions.Center)
            .SetWidth(550)
            .SetHeight(75);

        UILayoutBehavior contentLayout = uiManager.AddVerticalGroup(profileWindow).SetChildAlignment(TextAnchor.MiddleCenter);

        UILayoutBehavior profileGroupLayout = uiManager.AddHorizontalGroup(contentLayout);
        profileGroupLayout.SetChildAlignment(TextAnchor.UpperCenter).SetPadding(new RectOffset(20, 0, 0, 10));

        UIWindowBehavior imageWrapper = (UIWindowBehavior)uiManager.AddWindow(profileGroupLayout).SetAlphaScale(0f);
        UIImageBehavior profileImage = uiManager.AddImage(imageWrapper, fcfType.profileIcon, sizeToImage: true, sizeToImageScale: 1f);

        if (fcfType.profileIcon != null)
        {
            profileImage.SetWidth(fcfType.profileIconSize).SetHeight(fcfType.profileIconSize);
        }
        else
        {
            profileImage.SetWidth(fcfType.profileIconSize).SetHeight(fcfType.profileIconSize);
            Color fallbackColor = new Color(0.5f, 0.5f, 0.5f);
            profileImage.SetColor(fallbackColor);
        }

        UILayoutBehavior traitGroupLayout = uiManager.AddVerticalGroup(profileGroupLayout).SetPadding(new RectOffset(10, 0, 0, 0));

        UITextBehavior traitTitle = (UITextBehavior)uiManager.AddLabel(traitGroupLayout, (ResolvedString)fcfType.traitTitle)
            .SetFontSize(22)
            .SetFontStyle(FontStyles.Bold)
            .SetFontColor(ManagerBehavior.Instance.headerTextColor)
            .SetAlignment(TextAlignmentOptions.Center)
            .SetWrapping(true)
            .SetWidth(300)
            .SizeHeightToContent();

        foreach (string traitText in fcfType.traits)
        {
            UITextBehavior traitLabel = uiManager.AddLabel(traitGroupLayout, (ResolvedString)traitText);
            traitLabel.SetFontSize(fcfType.traitFontSize).SetAlignment(TextAlignmentOptions.Left).SetWrapping(true).SetWidth(320).SizeHeightToContent();
        }

        UIWindowBehavior profileInfoWindow = (UIWindowBehavior)uiManager.AddWindow(contentLayout);
        UILayoutBehavior profileInfoWindowPadded = profileInfoWindow.GetComponent<UILayoutBehavior>();
        profileInfoWindowPadded.SetPadding(new RectOffset(0, 0, 10, 15)).SizeHeightToContent().SizeHeightToContent();

        UITextBehavior profileInfo = (UITextBehavior)uiManager.AddLabel(profileInfoWindow, (ResolvedString)fcfType.description)
            .SetWrapping(true)
            .SetOverflowMode(TextOverflowModes.Overflow)
            .SetFontSize(fcfType.descFontSize)
            .SetWidth(400)
            .SizeHeightToContent();
        profileInfo.SetAlignment(fcfType.alignment);

        UIButtonBehavior closeButton = (UIButtonBehavior)uiManager.AddButton(profileParent, (ResolvedString)"Close", () =>
        {
            CloseProfileWindow();
        }).SetHeight(30).SetWidth(150);

        UILayoutBehavior contentLayoutBehavior = profileParent?.GetComponent<UILayoutBehavior>();
        contentLayoutBehavior?.SetChildAlignment(TextAnchor.MiddleCenter);
        closeButton.transform.SetParent(profileParent?.transform, false);
    }

    public override void VisitProperties(IActorPropertyVisitor visitor)
    {
        base.VisitProperties(visitor);
        visitor.VisitSharedButtonProperty((LocalizedString)fcfType.profileButton, () =>
        {
            if (profileWindow)
            {
                CloseProfileWindow();
            }
            else
            {
                ShowProfileWindow();
            }
        });
    }

    private void CloseProfileWindow()
    {
        if (profileParent)
        {
            Manager<UIManager>.Instance.popupStack.Remove(profileParent);
            UnityEngine.Object.Destroy(profileParent.gameObject);
        }
    }
}