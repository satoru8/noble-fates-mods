﻿using System.Collections.Generic;
using UnityEngine;

public class FantasyCreatureMomentType : MomentType
{
    public enum Spawn
    {
        NearHome,
        Underground
    }

    public List<MonsterMomentMonsterCost> creatures;

    public CharacterType monsterType;

    public float maxBudget = float.MaxValue;

    public string castingKey;

    public Spawn spawn = Spawn.NearHome;

    public SettingDefinition difficulty { get; private set; }

    public string toolTip { get; private set; } = "Creatures";
    public string bigToast { get; private set; } = "Creatures are attacking!";
    public string victory { get; private set; } = "Creatures have been defeated!";
    public string preventDispatch { get; private set; } = "Creature Attack in Progress";


    public FantasyCreatureMomentType(OctDatGlobalInitializer initializer)
        : base(initializer)
    {
    }

    public override void Cast(OctScriptContext context, StrengthSnapshot strength)
    {
        if (castingKey == null)
        {
            return;
        }

        Dictionary<CharacterType, int> dictionary = new Dictionary<CharacterType, int>();
        float num = Mathf.Max(Mathf.Round(strength.total * OctoberMath.DistributedRandom(0.125f, 0.375f)), 1f);
        float num2 = num;
        while (num2 >= 1f)
        {
            CharacterType characterType = null;
            float num3 = 1f;
            if (creatures != null)
            {
                float num4 = 0f;
                foreach (MonsterMomentMonsterCost monster in creatures)
                {
                    if (monster.cost > 0f && monster.cost <= num2)
                    {
                        float num5 = 1f / monster.cost;
                        num4 += num5;
                        if (Random.value * num4 <= num5)
                        {
                            characterType = monster.monsterType;
                            num3 = monster.cost;
                        }
                    }
                }
            }
            else
            {
                characterType = monsterType;
            }

            if (characterType == null)
            {
                break;
            }

            dictionary.TryGetValue(characterType, out var value);
            dictionary[characterType] = value + 1;
            num2 -= num3;
        }

        context.SetValue(castingKey, dictionary);
    }

    public override void CastingTooltip(UIWindowBehavior popup, OctScriptContext context)
    {
        Dictionary<CharacterType, int> value = context.GetValue<Dictionary<CharacterType, int>>(castingKey);
        if (true)
        {
            HostilesTooltipWindowContents.InstanceTooltip(popup, toolTip, value);
        }
    }
}