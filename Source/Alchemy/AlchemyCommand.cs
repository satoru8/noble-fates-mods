﻿public class AlchemyItemCommand : CraftItemCommand
{
    public AlchemyCommandType alchemyType
    {
        get
        {
            return type as AlchemyCommandType;
        }
    }

    public override string RelevantSkill()
    {
        if (alchemyType == null)
        {
            return "Alchemy";
        }

        return alchemyType.relevantSkill;
    }

    public override string ToString()
    {
        string skill = alchemyType?.relevantSkill ?? "Alchemy";

        if (partial != null)
        {
            return skill + " " + partial.item.type.GetName();
        }

        return skill + " " + recipe;
    }

    public AlchemyItemCommand(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    public AlchemyItemCommand(Actor owner, WorkbenchPropActor workbench, ItemRecipe recipe)
        : base(owner, workbench, recipe)
    {
    }
}

public class AlchemyCommandType : CommandType
{
    public string relevantSkill { get; private set; } = "Alchemy";

    public AlchemyCommandType(OctDatGlobalInitializer initializer)
        : base(initializer)
    {
    }
}