﻿using System.Collections.Generic;
using UnityEngine;

public class ExtraMoment : Moment
{
    private ScriptableConversationInstance conversation;
    private bool spawned;
    public ExtraMomentType extraMoment
    {
        get
        {
            return type as ExtraMomentType;
        }
    }

    public Notification notification { get; private set; }

    public ExtraMoment(MomentType type, OctScriptContext context)
        : base(type, context)
    {
    }

    public ExtraMoment(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    protected override void Discovered()
    {
        Discovered();
        foreach (Character participant in participants)
        {
            participant.PinPerceived("moment");
            participant.ShowHealthBar("moment");
        }

        Manager<BreachManager>.Instance.IncrementMomentsSinceLast();
        Manager<ToastManager>.Instance.QueueToast(extraMoment.toastNotif, ToastStinger.Neutral, 1f);
        notification = new Notification(extraMoment.mainNotif, map);
        notification.AddOnClick(CycleParticipants);
        ManagerBehavior.Instance.InCombat();
        if (!Manager<CameraManager>.Instance.captureMode)
        {
            Manager<TimeManager>.Instance.AutoPause();
        }
    }

    protected override void Ended()
    {
        if (notification != null)
        {
            notification.Destroy();
            notification = null;
        }

        Ended();
    }

    public override void Tick(float dt)
    {
        Tick(dt);
        if (discovered)
        {
            ManagerBehavior.Instance.InCombat();
        }

        bool underground;
        bool onRoof;
        if (!spawned)
        {
            if (conversation != null && !conversation.destroyed)
            {
                return;
            }

            ExtraMomentType extraMomentType = type as ExtraMomentType;
            underground = false;
            onRoof = false;
            NearHomeQuery nearHomeQuery = new NearHomeQuery(Manager<FactionManager>.Instance.homeFaction, null, QueryPred);
            Dictionary<CharacterType, int> dictionary;
            if ((dictionary = context.GetValue(extraMomentType.castingKey) as Dictionary<CharacterType, int>) != null)
            {
                bool flag = Random.value <= 0.5f;
                foreach (KeyValuePair<CharacterType, int> item in dictionary)
                {
                    for (int i = 0; i < item.Value; i++)
                    {
                        underground = Random.value < 0.125f;
                        onRoof = Random.value < 0.125f;
                        int targetDist = Mathf.RoundToInt(Mathf.Pow(Random.value, 3f) * 5f);
                        TilePos tilePos = nearHomeQuery.Spot(targetDist);
                        if (!(tilePos == TilePos.Invalid))
                        {
                            float num = OctoberMath.DistributedRandom(0f, 1f);
                            float f = OctoberMath.DistributedRandom(0f, 1f);
                            float num2 = Mathf.Pow(f, 3f) * (float)(item.Value - 1) * 0.125f;
                            float num3 = 0.25f + num * 0.5f + num2;
                            TrySpawn(item.Key, tilePos, num3 * item.Key.lootValue, !flag || Random.value > 0.25f);
                        }
                    }
                }

                spawned = true;
            }

            if (!spawned)
            {
                float num4 = Mathf.Clamp(Mathf.Round(Mathf.Lerp(Manager<KingdomManager>.Instance.playerKingdom.home.present.Count, Manager<KingdomManager>.Instance.playerKingdom.members.Count, 0.5f) * Manager<MomentManager>.Instance.difficulty * Manager<SettingsManager>.Instance.GetGameFloatValue(extraMoment.difficultyID) * OctoberMath.DistributedRandom(0.5f, 1f)), 1f, extraMoment.maxBudget);
                float num5 = num4;
                while (num5 >= 1f)
                {
                    CharacterType characterType = null;
                    float num6 = 1f;
                    if (extraMomentType.monsters != null)
                    {
                        float num7 = 0f;
                        foreach (MonsterMomentMonsterCost monster2 in extraMomentType.monsters)
                        {
                            if (monster2.cost > 0f && monster2.cost <= num5)
                            {
                                float num8 = 1f / monster2.cost;
                                num7 += num8;
                                if (Random.value * num7 <= num8)
                                {
                                    characterType = monster2.monsterType;
                                    num6 = monster2.cost;
                                }
                            }
                        }
                    }
                    else
                    {
                        characterType = extraMomentType.monsterType;
                    }

                    if (characterType == null)
                    {
                        break;
                    }

                    underground = Random.value < extraMoment.undergroundChance;
                    onRoof = Random.value < extraMoment.onRoofChance;
                    int targetDist2 = Mathf.RoundToInt(Mathf.Pow(Random.value, 3f) * 5f);
                    TilePos tilePos2 = nearHomeQuery.Spot(targetDist2);
                    if (!(tilePos2 == TilePos.Invalid))
                    {
                        float num9 = OctoberMath.DistributedRandom(0f, 1f);
                        float f2 = OctoberMath.DistributedRandom(0f, 1f);
                        float num10 = Mathf.Pow(f2, 3f) * ((num4 - num6) / num6) * 0.125f;
                        float num11 = 0.25f + num9 * 0.5f + num10;
                        if (TrySpawn(characterType, tilePos2, num11 * characterType.lootValue, portalIn: true))
                        {
                            num5 -= num6;
                        }
                    }
                }
            }

            spawned = true;
        }

        if (!spawned)
        {
            return;
        }

        if (notification != null)
        {
            if (notification.actor != null)
            {
                Character character;
                if ((character = notification.actor as Character) != null)
                {
                    if (character.dead)
                    {
                        notification.SetActor(null);
                    }
                    else if (character.offMap)
                    {
                        notification.SetActor(null);
                    }
                }
                else
                {
                    notification.SetActor(null);
                }
            }

            if (notification.actor == null)
            {
                CycleParticipants(notification);
            }
        }

        bool flag2 = true;
        foreach (Character participant in participants)
        {
            Character monster = participant;
            if (!monster.dead && (!monster.offMap || monster.portal != null))
            {
                flag2 = false;
                Faction homeFaction;
                if (monster.perception != null && (homeFaction = Manager<FactionManager>.Instance.homeFaction) != null)
                {
                    homeFaction.VisitMembers(PerceiveTarget, FactionMembers.MembersAndSerfs);
                }
            }

            void PerceiveTarget(Character potTarget)
            {
                if (potTarget.onCurrentMap && !potTarget.dead)
                {
                    monster.perception.PerceiveCharacter(potTarget);
                }
            }
        }

        if (flag2)
        {
            if (notification != null)
            {
                Manager<ToastManager>.Instance.QueueToast(extraMoment.winNotif, ToastStinger.Positive, 1f);
                Manager<FactionManager>.Instance.playerFaction.Celebrate();
            }

            End();
        }

        bool QueryPred(TilePos potTile)
        {
            if (!onRoof && Manager<PathingManager>.Instance.GetFloorProp(null, potTile, BlockingQuery.Movement) is RoofPropActor)
            {
                return false;
            }

            if (!underground && potTile.y < Manager<TerrainManager>.Instance.GetHeight(potTile.x, potTile.z))
            {
                return false;
            }

            return true;
        }

        bool TrySpawn(CharacterType type, TilePos tile, float loot, bool portalIn)
        {
            Character character2 = Manager<CharacterManager>.Instance.GenerateCharacter(type);
            character2.SetLootValue(loot);
            if (portalIn)
            {
                float num12 = 7.5f * (0.5f + Random.value);
                float num13 = num12 + 60f * (1f + 0.125f * (0.5f + Random.value));
                character2.PortalIn(tile, num12, num13);
            }
            else if (!character2.TryPlaceOnMap(tile, 3f))
            {
                character2.Destroy();
                return false;
            }

            AddParticipant(character2);
            return true;
        }
    }
}