﻿using System.Collections.Generic;

public class ExtraMomentType : MonsterMomentType
{
    public string toastNotif { get; private set; } = "Monsters are attacking!";
    public string mainNotif { get; private set; } = "Monsters are attacking!";
    public string winNotif { get; private set; } = "Monsters defeated!";
    public string enemy { get; private set; } = "Monsters";
    public float undergroundChance { get; private set; } = 0.125f;
    public float onRoofChance { get; private set; } = 0.125f;

    public ExtraMomentType(OctDatGlobalInitializer initializer)
        : base(initializer)
    {
    }

    public override void CastingTooltip(UIWindowBehavior popup, OctScriptContext context)
    {
        Dictionary<CharacterType, int> value = context.GetValue<Dictionary<CharacterType, int>>(castingKey);
        if (true)
        {
            HostilesTooltipWindowContents.InstanceTooltip(popup, enemy, value);
        }
    }
}