﻿using System.Collections.Generic;

public class CreatureMomentType : MomentType
{
    public CharacterType animalType;
    public List<CreatureList> creatures;

    public CreatureMomentType(OctDatGlobalInitializer initializer)
        : base(initializer)
    {
    }
}