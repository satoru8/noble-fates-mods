﻿using UnityEngine;

public class CreatureMoment : Moment
{
    public Notification notification { get; private set; }
    //public CreatureMomentType creatureType { get; private set; }
    public CreatureMomentType creatureType => base.type as CreatureMomentType;

    public TimeStamp until { get; private set; }

    public void Leave()
    {
        until = Manager<TimeManager>.Instance.seconds;
    }

    public CreatureMoment(MomentType type, OctScriptContext context)
        : base(type, context)
    {
        float budget = (float)Manager<KingdomManager>.Instance.playerKingdom.home.present.Count * Manager<MomentManager>.Instance.difficulty * Manager<SettingsManager>.Instance.GetGameFloatValue("Oct.Settings.Game.DangerousAnimalsDifficulty") * OctoberMath.DistributedRandom(0.25f, 0.5f);
        float totalChance = 0f;
        Manager<CharacterManager>.Instance.VisitCharacterTypes(RollType);

        if (creatureType.creatures == null)
        {
            End();
            return;
        }

        budget = Mathf.Max(budget, creatureType.animalType.dangerousCost);
        TilePos tilePos = TilePos.Invalid;
        for (int i = 0; i < 10; i++)
        {
            TilePos tilePos2 = new TilePos(Random.Range(-Manager<TerrainManager>.Instance.mapRadius + 1, Manager<TerrainManager>.Instance.mapRadius - 1), 0, Random.Range(-Manager<TerrainManager>.Instance.mapRadius + 1, Manager<TerrainManager>.Instance.mapRadius - 1));
            tilePos2.y = Manager<TerrainManager>.Instance.GetHeight(tilePos2.x, tilePos2.z) + 1;
            if (Manager<PathingManager>.Instance.IsWalkableAndReachable(null, tilePos2) && !Manager<ZoneManager>.Instance.IsHome(tilePos2))
            {
                Vector3 vector = CameraController.Instance.transform.parent.position - (Vector3)tilePos2;
                vector.y = 0f;
                if (!(vector.magnitude < 10f))
                {
                    tilePos = tilePos2;
                    break;
                }
            }
        }

        if (tilePos == TilePos.Invalid)
        {
            End();
            return;
        }

        if (creatureType.animalType != null)
        {
            for (; budget > 1f; budget -= creatureType.animalType.dangerousCost)
            {
                Character character = Manager<CharacterManager>.Instance.GenerateCharacter(creatureType.animalType);
                if (!character.TryPlaceOnMap(tilePos, 5f))
                {
                    character.Destroy();
                }
                AddParticipant(character);
            }
        }
        else
        {
            End();
            return;
        }

        if (participants.Count == 0)
        {
            End();
        }

        string text = GetNotification();
        Manager<ToastManager>.Instance.QueueToast(text, ToastStinger.Neutral, 1f);
        notification = new Notification(text, map);
        until = Manager<TimeManager>.Instance.seconds + TimeManager.SecondsPerYear * OctoberMath.DistributedRandom(1f, 6f);
        void RollType(CharacterType potType)
        {
            if (potType.dangerousCost > 0f)
            {
                if (potType.dangerousCost <= budget)
                {
                    float num = 1f / potType.dangerousCost;
                    totalChance += num;
                    if (Random.value * totalChance <= num)
                    {
                        creatureType.animalType = potType;
                    }
                }
                else if (totalChance == 0f)
                {
                    if (creatureType.animalType == null)
                    {
                        creatureType.animalType = potType;
                    }
                    else if (potType.dangerousCost < creatureType.animalType.dangerousCost)
                    {
                        creatureType.animalType = potType;
                    }
                }
            }
        }
    }

    public CreatureMoment(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    protected override void Ended()
    {
        if (notification != null)
        {
            notification.Destroy();
            notification = null;
        }

        base.Ended();
    }

    public override void Tick(float dt)
    {
        Tick(dt);
        Character character = null;
        bool flag = true;
        for (int num = participants.Count - 1; num >= 0; num--)
        {
            if (!participants[num].destroyed && !participants[num].dead && !participants[num].rabid && participants[num].perception != null && participants[num].faction == null)
            {
                flag = false;
                if (participants[num].discovered && character == null)
                {
                    character = participants[num];
                }

                if (Manager<TimeManager>.Instance.seconds >= until && participants[num].migrationCommand == null)
                {
                    participants[num].Migrate();
                }
            }
        }

        if (flag)
        {
            End();
        }
        else if (notification != null)
        {
            notification.SetActor(character);
            notification.SetNotification(GetNotification());
        }
    }

    public string GetNotification()
    {
        int num = 0;
        foreach (Character participant in participants)
        {
            if (!participant.destroyed && !participant.dead && !participant.rabid && participant.perception != null && participant.faction == null)
            {
                num++;
            }
        }

        if (discovered)
        {
            if (num > 1)
            {
                return creatureType.animalType.plural + " spotted nearby!";
            }

            if (num > 0)
            {
                return "A " + creatureType.animalType.name + " has been spotted nearby!";
            }
        }
        else
        {
            if (num > 1)
            {
                return "Reports of " + creatureType.animalType.plural + " in the area";
            }

            if (num > 0)
            {
                return "A " + creatureType.animalType.name + " has been reported in the area";
            }
        }

        return null;
    }
}