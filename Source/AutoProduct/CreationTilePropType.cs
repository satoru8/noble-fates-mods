﻿using System.Collections.Generic;
using UnityEngine;
public class CreationTilePropType : TilePropType
{
    public CreationTilePropType(OctDatGlobalInitializer initializer)
        : base(initializer)
    {
    }

    public float creationTime { get; private set; } = 5760f;

    public List<FacetType> propSettings;
}