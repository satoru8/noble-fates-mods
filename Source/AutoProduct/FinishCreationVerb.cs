﻿using UnityEngine;
public class FinishCreationVerb : Verb
{
    private string skill;
    private TimeStamp timeToFinish;
    public CreationPropActor cProp { get; private set; }
    public CreationCommandType createCommand { get; private set; }

    public FinishCreationVerb(CreationPropActor cProp, string skill)
    {
        this.cProp = cProp;
        this.skill = skill;
    }

    public FinishCreationVerb(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    public string GetSkill()
    {
        if (skill != null)
        {
            return skill;
        }

        return createCommand.skillUsed;
    }

    protected override void Assigned(Verb previousVerb)
    {
        base.Assigned(previousVerb);
        executor.character.SetForward((cProp.pos - executor.actor.pos).normalized);
        executor.character.inventory.StowAll();
        executor.character.animations.PlayAnimation("Armature|Pick");
        timeToFinish = RollRate(GetSkill(), 10f);
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);
        if (cProp.state != CreationPropActor.State.Complete)
        {
            Complete();
        }
        else if (Manager<TimeManager>.Instance.seconds >= timeToFinish)
        {
            timeToFinish = RollRate(GetSkill(), 10f);
            if (executor.character.SuccessRoll(GetSkill()))
            {
                cProp.FinishCreation(executor.character, executor.character.GetSkill(GetSkill()) * Mathf.Min(executor.character.GetDexterity(), 1f));
                executor.character.AwardExperience(GetSkill(), 17.5f);
                Complete();
            }
            else
            {
                executor.character.AwardExperience(GetSkill(), 25f);
            }
        }
    }
}