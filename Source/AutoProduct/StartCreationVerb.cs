﻿public class StartCreationVerb : Verb
{
    private string skill;

    private TimeStamp timeToStart;

    public CreationPropActor cProp { get; private set; }

    public CreationCommand createCommand { get; private set; }

    public CreationCommandType createCommandType => createCommandType;

    public StartCreationVerb(CreationPropActor cProp, CreationCommand createCommand, string skill)
    {
        this.cProp = cProp;
        this.createCommand = createCommand;
        this.skill = skill;
    }

    public StartCreationVerb(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    public string GetSkill()
    {
        if (skill != null)
        {
            return skill;
        }

        return createCommandType.skillUsed;
    }

    protected override void Assigned(Verb previousVerb)
    {
        base.Assigned(previousVerb);
        executor.character.SetForward((cProp.pos - executor.actor.pos).normalized);
        executor.character.inventory.StowAll();
        executor.character.animations.PlayAnimation("Armature|Pick");
        timeToStart = RollRate(GetSkill(), 10f);
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);
        if (cProp.state != 0)
        {
            Complete();
        }
        else if (Manager<TimeManager>.Instance.seconds >= timeToStart)
        {
            timeToStart = RollRate(GetSkill(), 10f);
            if (executor.character.SuccessRoll(GetSkill()))
            {
                cProp.StartCreation(createCommand, executor.character.GetSkill(GetSkill()));
                executor.character.AwardExperience(GetSkill(), 20f);
                Complete();
            }
            else
            {
                executor.character.AwardExperience(GetSkill(), 33.3333321f);
            }
        }
    }
}