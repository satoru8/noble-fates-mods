﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CreationPropActor : ProductionPropActor, IProcessingProp
{
    public enum State
    {
        Empty,
        InProgress,
        Complete
    }

    public int slotCount => 1;

    public int maxQueueLength => 5;

    public ItemRecipe recipe { get; private set; }

    public State state { get; private set; }

    public float progress { get; private set; }

    public float precision { get; private set; }

    public CreationCommand command { get; private set; }

    public CreationTilePropType typeNew
    {
        get
        {
            return type as CreationTilePropType;
        }
    }

    public CreationPropActor(PropType type, int prefab, Vector3 pos, int rot, Faction faction)
        : base(type, prefab, pos, rot, faction)
    {
    }

    public CreationPropActor(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    public override void PostLoad()
    {
        base.PostLoad();
        if (command != null)
        {
            if (commands.Count == 0)
            {
                commands.Add(command);
            }

            if (state == State.Empty)
            {
                command = null;
            }
        }
    }

    protected override void Destroyed()
    {
        for (int num = commands.Count - 1; num >= 0; num--)
        {
            commands[num].Cancel();
        }

        OctoberUtils.Assert(commands.Count == 0);
        base.Destroyed();
    }

    public int BatchSize()
    {
        int num = 0;
        List<IItemType> list = new List<IItemType>();
        for (num = recipe.batch; num > 0; num--)
        {
            bool flag = true;
            foreach (RecipeIngredient ingredient in recipe.ingredients)
            {
                if (ingredient is ItemRecipeIngredient itemRecipeIngredient && base.inventory.CountByFilter(itemRecipeIngredient.filter) < itemRecipeIngredient.count * num)
                {
                    flag = false;
                    break;
                }
            }

            if (flag)
            {
                break;
            }
        }

        return num;
    }

    public void FinishCreation(Character character, float precision)
    {
        if (state != State.Complete)
        {
            return;
        }

        this.precision *= precision;
        int num = 0;
        List<IItemType> consumed = new List<IItemType>();
        for (int i = 0; i < recipe.batch; i++)
        {
            bool flag = true;
            foreach (RecipeIngredient ingredient in recipe.ingredients)
            {
                if (ingredient is ItemRecipeIngredient itemRecipeIngredient && base.inventory.CountByFilter(itemRecipeIngredient.filter) < itemRecipeIngredient.count)
                {
                    flag = false;
                    break;
                }
            }

            if (!flag)
            {
                break;
            }

            foreach (RecipeIngredient ingredient2 in recipe.ingredients)
            {
                if (ingredient2 is ItemRecipeIngredient itemRecipeIngredient2 && base.inventory.ConsumeByFilter(ingredient2.filter, itemRecipeIngredient2.count, consumed, LocalizationManager.ItemLog.used) != itemRecipeIngredient2.count)
                {
                    Debug.LogError("Failed to consume based on assumption!");
                }
            }

            num++;
        }

        Action<ItemActor> callback = null;
        if (command != null)
        {
            callback = command.rule.Crafted;
        }

        float num2 = ((recipe.researchEntry != null) ? recipe.researchEntry.requiredSkill : recipe.minSkill);
        float normalizedSkill = Mathf.Max(precision - num2, 0f) / 20f;
        int count = Mathf.Max(Mathf.RoundToInt((float)(recipe.yield * num) * Character.DistributedYieldResult(UnityEngine.Random.value, normalizedSkill)), 1);
        float durabilityNorm = Mathf.Clamp(0.5f + Character.DistributedRoll(normalizedSkill, recipe.durabilityDifficulty) * 0.5f, 0.5f, 1f);
        Manager<ItemManager>.Instance.DropStack(base.pos, recipe.type.RecipeItemType(), count, durabilityNorm, Ignored.No, Log.Added, LocalizationManager.ItemLog.produced, callback);
        command = null;
        recipe = null;
        state = State.Empty;
        progress = 0f;
        precision = 0f;
    }

    public void StartCreation(CreationCommand command, float precision)
    {
        if (state == State.Empty)
        {
            this.command = command;
            recipe = command.recipe;
            state = State.InProgress;
            progress = 0f;
            this.precision = precision;
        }
    }

    public override int InProgress(ProductionCommand command)
    {
        if (state != 0)
        {
            if (command is CreationCommand)
            {
                return recipe.yield * recipe.batch;
            }

            return 0;
        }

        return 0;
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);
        bool flag = false;
        if (command == null)
        {
            flag = true;
        }
        else if (command.destroyed)
        {
            command = null;
            flag = true;
        }

        if (flag)
        {
            recipe = null;
            state = State.Empty;
            progress = 0f;
            precision = 0f;
            command = null;
        }

        if (state == State.InProgress)
        {
            progress += dt / typeNew.creationTime;
            if (progress > 1f)
            {
                progress = 1f;
                state = State.Complete;
            }
        }
    }

    public override bool UseCustomPropertiesWindow()
    {
        if (Manager<SelectionManager>.Instance.selectedActors.Count == 1 && belonging.owner != null && Manager<FactionManager>.Instance.IsPlayerFaction(belonging.owner.actor as Faction))
        {
            return true;
        }

        return false;
    }

    public override UIWindowBehavior ShowCustomPropertiesWindow(UIElementBaseBehavior parent)
    {
        UIWindowBehavior uIWindowBehavior = Manager<UIManager>.Instance.AddWindow(parent);
        UIProcessingPropWindowContents uIProcessingPropWindowContents = uIWindowBehavior.InstanceContents<UIProcessingPropWindowContents>();
        uIProcessingPropWindowContents.SetProp(this);
        return uIWindowBehavior;
    }

    public bool SlotFull(int slot)
    {
        return state != State.Empty;
    }

    public string SlotName(int slot)
    {
        int baseSkill = ((recipe.researchEntry != null) ? recipe.researchEntry.requiredSkill : recipe.minSkill);
        string text = " (~" + Mathf.RoundToInt((float)(BatchSize() * recipe.yield) * Manager<FactionManager>.Instance.GetAverageYield("Cooking", 0.75f, baseSkill)) + ")";
        if (recipe != null)
        {
            return recipe.GetName() + text;
        }

        if (command != null)
        {
            return command.GetCommandNoun((ResolvedString)recipe.GetName()) + text;
        }

        return null;
    }

    public float SlotProgress(int slot)
    {
        return progress;
    }

    public int QueueLength()
    {
        return commands.Count;
    }

    public ProductionCommand QueuedCommand(int index)
    {
        return commands[index];
    }

    public override ProductionCommand QueueRecipe(ItemRecipe recipe)
    {
        CreationCommand result = Activator.CreateInstance(recipe.commandType, base.faction, this, recipe) as CreationCommand;
        QueueCommand(result);
        return result;
    }
}