﻿using System;
using UnityEngine;

public class CreationCommand : ProductionCommand
{
    public CreationCommandType createCommandType
    {
        get
        {
            return type as CreationCommandType;
        }
    }

    private CreationPropActor cProp;

    public ItemRecipe creationRecipe;

    public override ItemRecipe recipe => creationRecipe;

    public override bool canFilterIngredients => true;

    public override string RelevantSkill()
    {
        if (createCommandType != null)
        {
            return createCommandType.skillUsed;
        }
        else
        {
            return "Cooking";
        }
    }

    public override ResolvedString GetCommandNoun(ResolvedString defaultNoun)
    {
        if (creationRecipe != null)
        {
            return creationRecipe.ToString();
        }
        else
        {
            return defaultNoun;
        }
    }

    public CreationCommand(OctSaveInitializer initializer)
        : base(initializer)
    {
    }

    public CreationCommand(Actor owner, CreationPropActor cProp, ItemRecipe creationRecipe)
        : base(owner)
    {
        this.cProp = cProp;
        this.creationRecipe = creationRecipe;
    }

    public override void VisitFuturePathTargets(CommandCharacterFacet forExecutor, Action<Vector3> visitor)
    {
        visitor(cProp.pos);
    }


    public override ResolvedString Details()
    {
        return new LocalizedString("Oct.Code.Commands.Creation.Status", "Making {recipe}").TemporaryContext().WithParam("recipe", creationRecipe).resolve;
    }

    public override bool Includes(IItemType type)
    {
        return false;
    }

    public override Actor Target()
    {
        return cProp;
    }

    public override bool Valid()
    {
        if (!base.Valid())
        {
            return false;
        }

        if (cProp == null)
        {
            return false;
        }

        if (cProp.destroyed)
        {
            return false;
        }

        return true;
    }

    public override bool CanExecute(CommandCharacterFacet forExecutor, OctScriptContext castingContext = null, Command parent = null, bool micro = false)
    {
        if (!base.CanExecute(forExecutor, castingContext, parent, micro))
        {
            return false;
        }

        if (!cProp.CanPathTo(forExecutor.actor, out var interactPos))
        {
            BlockedFor(forExecutor, 10000);
            return false;
        }

        if (!micro && Manager<CommandManager>.Instance.IsUnsafe(interactPos))
        {
            BlockedFor(forExecutor, 2);
            return false;
        }

        Character character = null;
        if (executor != null)
        {
            character = executor.character;
        }

        if (cProp.reservedBy != character)
        {
            BlockedFor(forExecutor, 1007, cProp.type.name.resolve + " Reserved By " + cProp.reservedBy.GetName());
            return false;
        }

        if (cProp.state == CreationPropActor.State.InProgress)
        {
            BlockedFor(forExecutor, 101);
            return false;
        }

        if (cProp.state == CreationPropActor.State.Empty)
        {
            if (rule.IsSatisfied())
            {
                BlockedFor(forExecutor, 100);
                return false;
            }

            foreach (RecipeIngredient ingredient in recipe.ingredients)
            {
                if (ingredient is ItemRecipeIngredient itemRecipeIngredient)
                {
                    int count = itemRecipeIngredient.count;
                    int remaining = count - cProp.inventory.CountByFilter(ingredient.filter);
                    if (BlockOnItems(forExecutor, ingredient.filter, remaining))
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public override bool IngredientRelevant(IItemType type)
    {
        foreach (RecipeIngredient ingredient in recipe.ingredients)
        {
            if (ingredient.filter.Includes(type))
            {
                return true;
            }
        }

        return false;
    }

    protected override bool Assigned(OctScriptContext castingContext)
    {
        if (!base.Assigned(castingContext))
        {
            return false;
        }

        if (!executor.actor.TryReserve(cProp, BlockToCancelReservation))
        {
            return false;
        }

        return true;
    }

    protected override void Unassigned(CommandCharacterFacet fromExecutor)
    {
        fromExecutor.actor.ReleaseIfReserved(cProp, BlockToCancelReservation);
        base.Unassigned(fromExecutor);
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);
        if (rule.IsComplete())
        {
            Complete();
            return;
        }

        Character actor = (Character)executor.actor;
        if (bodyVerb != null)
        {
            if (bodyVerb.complete)
            {
                bodyVerb = null;
            }
            else if (bodyVerb.interrupted)
            {
                bodyVerb = null;
            }
            else if (bodyVerb.failed)
            {
                if (bodyVerb is GotoVerb)
                {
                    Blocked(4);
                    return;
                }

                bodyVerb = null;
            }
        }

        if (bodyVerb != null)
        {
            return;
        }

        if (cProp.state == CreationPropActor.State.Complete)
        {
            if (cProp.GetPathingTargetDistance(actor) > 0.25f)
            {
                AttemptTransitionToVerb(new GotoVerb(cProp, cProp.pos, forWork: true, RelevantSkill()));
            }
            else
            {
                AttemptTransitionToVerb(new FinishCreationVerb(cProp, RelevantSkill()));
            }
        }
        else if (cProp.state == CreationPropActor.State.Empty)
        {
            if (rule.IsSatisfied())
            {
                Blocked(100);
                return;
            }

            foreach (RecipeIngredient ingredient in recipe.ingredients)
            {
                ItemRecipeIngredient itemRecipeIngredient;
                if ((itemRecipeIngredient = ingredient as ItemRecipeIngredient) == null)
                {
                    continue;
                }

                int count = itemRecipeIngredient.count;
                int num = count - cProp.inventory.CountByFilter(ingredient.filter);
                int num2 = num - executor.character.inventory.CountByFilter(ingredient.filter);
                if (num2 > 0)
                {
                    if (!AttemptExecuteChild(new WithdrawItemsCommand(executor.character, ingredient.filter, num, itemRecipeIngredient.count * recipe.batch, RelevantSkill())))
                    {
                        Blocked();
                    }

                    return;
                }
            }

            foreach (RecipeIngredient ingredient2 in recipe.ingredients)
            {
                ItemRecipeIngredient itemRecipeIngredient2;
                if ((itemRecipeIngredient2 = ingredient2 as ItemRecipeIngredient) == null)
                {
                    continue;
                }

                int count2 = itemRecipeIngredient2.count;
                int num3 = cProp.inventory.CountByFilter(ingredient2.filter);
                int num4 = count2 - num3;
                if (num4 > 0)
                {
                    if (!AttemptExecuteChild(new DepositItemsCommand(executor.character, cProp, ingredient2.filter, num4, itemRecipeIngredient2.count * recipe.batch - num3)))
                    {
                        Blocked();
                    }

                    return;
                }
            }

            if (cProp.GetPathingTargetDistance(actor) > 0.25f)
            {
                AttemptTransitionToVerb(new GotoVerb(cProp, cProp.pos, forWork: true, RelevantSkill()));
            }
            else
            {
                AttemptTransitionToVerb(new StartCreationVerb(cProp, this, RelevantSkill()));
            }
        }
        else
        {
            Blocked();
        }
    }
}