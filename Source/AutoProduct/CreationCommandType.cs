﻿public class CreationCommandType : CommandType
{
    public CreationCommandType(OctDatGlobalInitializer initializer)
        : base(initializer)
    {
    }

    public string skillUsed { get; private set; } = "Cooking";
    public string verb { get; private set; } = "Making ";
}
