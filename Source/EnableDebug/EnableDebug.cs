﻿using HarmonyLib;
using System.Reflection;
using UnityEngine;

[HarmonyPatch(typeof(FlowManager))]
[HarmonyPatch("IsGateOpen")]
public class FlowManager_Patch
{
    [HarmonyPostfix]
    public static void Postfix(ref bool __result, string gateName)
    {
        if (gateName == "Oct.Flow.Debug")
        {
            __result = true;
        }
    }
}

[HarmonyPatch(typeof(ManagerBehavior))]
[HarmonyPatch("Awake")]
class EnableDebug
{
    static void Postfix()
    {
        var isDebugEnabled = typeof(ManagerBehavior).GetField("IsDebug", BindingFlags.Instance | BindingFlags.NonPublic);

        if (isDebugEnabled != null)
        {
            isDebugEnabled.SetValue(ManagerBehavior.Instance, true);
        }
    }
}