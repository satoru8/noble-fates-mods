﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectProductsVerb : Verb
{
    private ProductionCharacterFacet target;
    private TimeStamp timeToCollect;

    public CollectProductsVerb(OctSaveInitializer initializer) : base(initializer)
    { }

    public CollectProductsVerb(ProductionCharacterFacet target) : base()
    {
        this.target = target;
    }

    private string Attribute()
    {
        return target.productionType.skillUsed;
    }

    protected override void Assigned(Verb previousVerb)
    {
        base.Assigned(previousVerb);
        executor.character.SetForward((target.actor.pos - executor.actor.pos).normalized);
        Character character = (Character)executor.actor;
        character.inventory.StowAll();
        character.animations.PlayAnimation("Armature|Pick");
        timeToCollect = RollRate(Attribute(), 2);
    }

    protected override void Unassigned(VerbCharacterFacet from, Verb nextVerb)
    {
        from.character.audio.StopVoice("Verb");
        base.Unassigned(from, nextVerb);
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);

        if (target == null || target.destroyed || target.character.dead || target.products == 0 || target.ripe < target.productionType.growth)
        {
            Complete();
            return;
        }
        else
        {
            if (TimeManager.Instance.seconds >= timeToCollect)
            {
                timeToCollect = RollRate(Attribute(), 2);

                Character character = (Character)executor.actor;

                if (character.SuccessRoll(Attribute()))
                {
                    Vector3 pos = target.actor.pos;

                    int count = Mathf.Min(10, target.products);
                    target.CollectProduct(count);

                    if (target.productionType.itemType != null)
                    {
                        BelongingsFacet owner = FactionManager.Instance.playerFaction.belongings;
                        if (command != null)
                        {
                            if (command.faction != null)
                            {
                                owner = command.faction.belongings;
                            }
                            else
                            {
                                owner = executor.character.faction.belongings;
                            }
                        }

                        int yield = Mathf.RoundToInt(count * character.DistributedYieldRoll(Attribute(), target.productionType.collectionDifficulty));
                        if (yield > 0f)
                        {
                            ItemManager.Instance.DropStack(pos, target.productionType.itemType, yield, 1, Ignored.No, Log.Added, "collected", null, owner);
                        }

                        if (target.productionType.itemType.dropItemChance > 0)
                        {
                            if (ItemManager.Instance.GetItemType(target.productionType.itemType.dropItemType) is StackedItemType itemType)
                            {
                                float yieldChance = count * yield * target.productionType.itemType.dropItemChance * 2 * character.DistributedYieldRoll(Attribute(), target.productionType.collectionDifficulty);
                                int itemYield = Mathf.FloorToInt(yieldChance);

                                yieldChance -= itemYield;

                                if (Random.value <= yieldChance)
                                {
                                    ++yield;
                                }
                                if (yield > 0f)
                                {
                                    ItemManager.Instance.DropStack(pos, itemType, itemYield, 1, Ignored.No, Log.Added, "collected", null, owner);
                                }
                            }
                        }
                    }

                    character.AwardExperience(Attribute(), Character.AttributeSuccessXP);
                }
                else
                {
                    character.AwardExperience(Attribute(), Character.AttributeFailureXP);
                }
            }
        }
    }
}