﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionCharacterFacetType : FacetType
{
    public string name;
    public StackedItemType itemType { get; private set; }
    public float collectionDifficulty { get; private set; } = .675f;
    public float yield { get; private set; } = 1f;
    public float growthPerDay { get; private set; } = .5f;
    public float minAge { get; private set; } = 1f;
    public float maxAge { get; private set; } = 60f;
    public float growth { get; private set; } = 1f;
    public string skillUsed { get; private set; } = "Ranching";
    public CommandType commandType { get; private set; }
    public ResearchEntry researchEntry;

    public ProductionCharacterFacetType(OctDatGlobalInitializer initializer) : base(initializer)
    { }
}

public class ProductionCharacterFacet : CharacterFacet
{
    public ProductionCharacterFacetType productionType
    {
        get
        {
            return type as ProductionCharacterFacetType;
        }
    }

    public int products { get; private set; }
    public float modifier { get; private set; }
    public CollectProductsCommand collectCommand { get; private set; }
    public float ripe { get; private set; } = 1f;

    public CollectProductsCommand GetOrIssueCollectCommand()
    {
        if (collectCommand == null)
        {
            if (character != null)
            {
                collectCommand = IssueCollectCommand(FactionManager.Instance.playerFaction);
                collectCommand.onDestroy += ClearCollectCommand;
            }
        }
        return collectCommand;
    }

    public void ClearCollectCommand(Command command)
    {
        collectCommand = null;
    }

    public ProductionCharacterFacet(OctSaveInitializer initializer) : base(initializer)
    { }

    public ProductionCharacterFacet(Character character, ProductionCharacterFacetType type) : base(character, type)
    {
        if (ManagerBehavior.Instance.state == ManagerBehavior.GameState.CreatingMap)
        {
            if (character.age > type.minAge && character.age < type.maxAge)
            {
                Bloom(OctoberMath.DistributedRandom(0f, 1f));
            }
        }
    }

    public CollectProductsCommand IssueCollectCommand(Faction faction)
    {
        if (character != null)
        {
            return new CollectProductsCommand(faction, this);
        }
        return null;
    }

    private void RemoveProduct(int count)
    {
        products = Mathf.Max(products - count, 0);
    }

    public override void Destroyed()
    {
        if (collectCommand != null)
        {
            collectCommand.Cancel();
        }

        base.Destroyed();
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);

        float growRate = 1f - (character.age / productionType.maxAge);

        if (character.dead == false && character.age < productionType.maxAge)
        {
            if (growRate > 0f)
            {
                if (character.age > productionType.minAge)
                {
                    if (products == 0)
                    {
                        Bloom();
                    }

                    float growDelta = dt / TimeManager.SecondsPerDay;
                    growDelta *= productionType.growthPerDay;
                    growDelta *= growRate;
                    growDelta *= modifier;
                    Grow(growDelta);
                }
            }
        }
        else
        {
            growRate = 0f;
        }

    }

    public void Grow(float delta)
    {
        ripe += delta;
        if (ripe >= productionType.growth)
        {
            ripe = productionType.growth;
        }
    }

    public void Bloom(float growth = 0f)
    {
        ripe = growth;
        modifier = .01f + Random.value;
        products = OctoberMath.FloorToInt(productionType.yield * modifier);
    }

    public void CollectProduct(int products)
    {
        RemoveProduct(products);
    }

    public override void VisitProperties(IActorPropertyVisitor visitor)
    {
        base.VisitProperties(visitor);

        if (FactionManager.Instance.IsPlayerFaction(character.faction) || character.orderableAlly || character.controllableAlly)
        {
            if (character.dead == false && character.age >= productionType.minAge && character.age < productionType.maxAge)
            {
                if (productionType.researchEntry == null || productionType.researchEntry.AvailableFor(character.faction.kingdom))
                {
                    if (productionType.itemType != null)
                    {
                        if (ripe >= productionType.growth)
                        {
                            visitor.VisitSummedIntProperty(productionType.itemType.name, Mathf.RoundToInt(products * FactionManager.Instance.GetAverageYield(productionType.skillUsed, productionType.collectionDifficulty)), true);
                        }
                        else
                        {
                            visitor.VisitSingleTextProperty(productionType.name + " Growth", OctoberMath.FloorToInt((ripe / productionType.growth) * 100) + "%");
                        }

                        if (!SelectionManager.Instance.IsTactical())
                        {
                            if (productionType.commandType != null)
                            {
                                visitor.VisitSharedCommandProperty("Collect " + productionType.itemType.name, GetOrIssueCollectCommand, collectCommand, products > 0, productionType.commandType, -10);
                            }
                        }
                    }
                }
            }
        }
    }

    public override void VisitThirdPersonInteractions(Character forCharacter, System.Action<IThirdPersonInteraction> visitor)
    {
        base.VisitThirdPersonInteractions(forCharacter, visitor);

        if (FactionManager.Instance.IsPlayerFaction(character.faction) || character.orderableAlly || character.controllableAlly)
        {
            if (character.dead == false && character.age < productionType.maxAge)
            {
                if (ripe >= productionType.growth)
                {
                    if (products > 0)
                    {
                        if (productionType.researchEntry == null || forCharacter.pawn.ResearchUnlocked(productionType.researchEntry))
                        {
                            if (productionType.itemType != null)
                            {
                                visitor(new VerbThirdPersonInteraction(ThirdPersonInteractionType.Interact, character, "Collect " + productionType.itemType.name, delegate { return new CollectProductsVerb(this); }, 100));
                            }
                        }
                    }
                }
            }
        }
    }
}
