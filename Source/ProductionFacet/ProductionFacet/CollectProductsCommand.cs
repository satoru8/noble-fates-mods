﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectProductsCommand : Command
{
    public const int COLLECT_COMMAND_BLOCKED_NO_PRODUCT = COMMAND_BLOCKED_CUSTOM_HARD;

    public ProductionCharacterFacet target { get; private set; }

    public CollectProductsCommand(OctSaveInitializer initializer) : base(initializer)
    { }

    public CollectProductsCommand(Actor owner, ProductionCharacterFacet target) : base(owner)
    {
        this.target = target;

        if (target.productionType.researchEntry != null)
        {
            if (FactionManager.Instance.IsPlayerFaction(faction))
            {
                if (!faction.kingdom.ResearchUnlocked(target.productionType.researchEntry))
                {
                    ResearchManager.Instance.ResearchDirty();
                }
            }
        }
    }

    public override float GetHeuristic(CommandCharacterFacet forExecutor)
    {
        return (target.actor.pos - forExecutor.character.pos).magnitude;
    }

    public override void VisitFuturePathTargets(CommandCharacterFacet forExecutor, System.Action<Vector3> visitor)
    {
        visitor(target.actor.pos);
    }

    public override ResearchEntry ResearchEntry()
    {
        return target.productionType.researchEntry;
    }

    protected override Actor ResearchAt()
    {
        return target.character;
    }

    public override bool Valid()
    {
        if (!base.Valid())
        {
            return false;
        }
        else if (target == null)
        {
            return false;
        }
        else if (target.actor == null)
        {
            return false;
        }
        else if (target.character.dead)
        {
            return false;
        }
        else if (target.character.faction != faction)
        {
            return false;
        }
        else if (target.character.age >= target.productionType.maxAge)
        {
            return false;
        }

        return true;
    }

    public override string ToString()
    {
        if (target != null)
        {
            return "Collecting " + target.productionType.itemType.name;
        }
        else
        {
            return "Collecting";
        }
    }

    public override string GetBlockedReason(int code)
    {
        switch (code)
        {
            case COLLECT_COMMAND_BLOCKED_NO_PRODUCT:
                return "No " + target.productionType.itemType.name;
        }

        return base.GetBlockedReason(code);
    }

    public override bool CanExecute(CommandCharacterFacet forExecutor, OctScriptContext castingContext = null, Command parent = null, bool micro = false)
    {
        if (!base.CanExecute(forExecutor, castingContext, parent, micro))
        {
            return false;
        }

        if (!target.actor.CanPathTo(forExecutor.actor))
        {
            BlockedFor(forExecutor, COMMAND_BLOCKED_UNREACHABLE);
            return false;
        }

        if (target.products == 0)
        {
            BlockedFor(forExecutor, COLLECT_COMMAND_BLOCKED_NO_PRODUCT);
            return false;
        }
        else if (target.ripe < target.productionType.growth)
        {
            BlockedFor(forExecutor, COLLECT_COMMAND_BLOCKED_NO_PRODUCT);
            return false;
        }

        return true;
    }

    public override void Cancel()
    {
        base.Cancel();
    }

    public override void Complete()
    {
        base.Complete();
    }

    public override void Tick(float dt)
    {
        base.Tick(dt);

        if (target.products == 0)
        {
            Blocked(COLLECT_COMMAND_BLOCKED_NO_PRODUCT);
            return;
        }
        else if (target.ripe < target.productionType.growth)
        {
            Blocked( COLLECT_COMMAND_BLOCKED_NO_PRODUCT);
            return;
        }

        if (bodyVerb != null)
        {
            if (bodyVerb.complete)
            {
                if (bodyVerb is GotoVerb)
                {
                    bodyVerb = null;
                }
                else if (bodyVerb is CollectProductsVerb)
                {
                    Blocked(COLLECT_COMMAND_BLOCKED_NO_PRODUCT);
                    return;
                }
            }
            else if (bodyVerb.interrupted)
            {
                Blocked();
                return;
            }
            else if (bodyVerb.failed)
            {
                Blocked(bodyVerb is GotoVerb ? COMMAND_BLOCKED_PATH_FAILED : COMMAND_BLOCKED_SOFT);
                return;
            }
        }

        if (bodyVerb == null)
        {
            if (target.actor.GetPathingTargetDistance(executor.actor) <= 1.0f)
            {
                AttemptTransitionToVerb(new CollectProductsVerb(target));
            }
            else if (!AttemptExecuteChild(new GotoCommand(executor.character, target.character, target.character.pos, true, target.productionType.skillUsed, .75f)))
            {
                Blocked();
                return;
            }
        }
    }
}
