﻿using TMPro;
using UnityEngine;

public class SecretMenuWindowContents : UIWindowContents
{
    private SecretMenuManager smManager => SecretMenuManager.Instance;
    private UIManager uiManager = UIManager.Instance;
    private UIToggleBehavior debugToggle;

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (window != null)
        {
            window.Close();
            ToolsManager.Instance.PopToolbarInFront();
            ToolsManager.Instance.DeactivateAll();
        }
    }

    protected override void Start()
    {
        base.Start();
        window.SetChildAlignment(TextAnchor.MiddleCenter).SetAlphaScale(0);

        UILayoutBehavior combinedLayout = uiManager.AddHorizontalGroup(window);

        UILayoutBehavior leftLayout = uiManager.AddVerticalGroup(combinedLayout);
        leftLayout.SetChildAlignment(TextAnchor.LowerRight).SetHeight(490).SizeWidthToContent();

        UILayoutBehavior leftLayout2 = uiManager.AddVerticalGroup(combinedLayout);
        leftLayout2.SetChildAlignment(TextAnchor.LowerRight).SetHeight(490).SizeWidthToContent();

        UILayoutBehavior mainLayout = uiManager.AddVerticalGroup(combinedLayout);
        mainLayout.SetChildAlignment(TextAnchor.MiddleCenter).SetSpacing(10);

        UITextBehavior title = uiManager.AddLabel(mainLayout, smManager.menuTitle);
        title
            .SetFontSize(36)
            .SetFontColor(ManagerBehavior.Instance.textColor)
            .SetFont(ManagerBehavior.Instance.texturina)
            .SetMaterial(ManagerBehavior.Instance.texturinaSemiBold)
            .SetAlignment(TextAlignmentOptions.Center)
            .SetWidth(500)
            .SetHeight(45);

        UIWindowBehavior mainWindow = uiManager.AddWindow(mainLayout);
        mainWindow.SetSprite(ToolsUIAssets.Instance.window)
            .SetPadding(new RectOffset(28, 28, 28, 28))
            .SetSpacing(10).SetHeight(500).SetWidth(500);

        UILayoutBehavior firstRow = uiManager.AddHorizontalGroup(mainWindow);
        firstRow.SetSpacing(14).SetHeight(80).SizeWidthToContent();

        UILayoutBehavior secondRow = uiManager.AddHorizontalGroup(mainWindow);
        secondRow.SetSpacing(14).SetHeight(80).SizeWidthToContent();

        UILayoutBehavior thirdRow = uiManager.AddHorizontalGroup(mainWindow);
        thirdRow.SetSpacing(14).SetHeight(80).SizeWidthToContent();

        UILayoutBehavior fourthRow = uiManager.AddHorizontalGroup(mainWindow);
        fourthRow.SetSpacing(14).SetHeight(80).SizeWidthToContent();

        UILayoutBehavior fifthRow = uiManager.AddHorizontalGroup(mainWindow);
        fifthRow.SetSpacing(14).SetHeight(80).SizeWidthToContent();

        smManager.NewButton(firstRow, "Character Debug Menu", smManager.CharacterDebug);
        smManager.NewButton(firstRow, "Character Full Control", smManager.CharacterFullControl);
        smManager.NewButton(firstRow, "No Building Costs", smManager.NoBuildingCosts);
        smManager.NewButton(firstRow, "Unlock All Research", smManager.UnlockAllResearch);
        smManager.NewButton(secondRow, "Instant Dig", smManager.InstantDig);
        smManager.NewButton(secondRow, "Props Debug Menu", smManager.PropDebug);
        smManager.NewButton(thirdRow, "Effects Editor", smManager.EffectsEditor);

        UITextBehavior debugText = uiManager.AddLabel(leftLayout, "Full Debug");
        debugText.SetFontSize(14).SizeWidthToContent().SetHeight(30);

        debugToggle = uiManager.AddToggle(leftLayout2, smManager.ToggleDebugButton);

        smManager.debugButton = uiManager.AddButton(fifthRow, "Full Debug Tool", smManager.ToggleDebugMenu);
        smManager.debugButton.SetHeight(80).SetWidth(100);

        uiManager.AddButton(mainLayout, "Close", smManager.CloseButton).SetHeight(30).SetWidth(150);

        if (smManager.debugButtonVisible == false)
        {
            smManager.debugButton.SetActive(false);
        }
        else
        {
            debugToggle.SetValue(UIToggleBehavior.Value.On);
            smManager.debugButton.SetActive(true);
        }
    }
}


//public class ExtendedUITrayWindowContents : UITrayWindowContents
//{
//    public UIIconButtonBehavior toggleSecretMenu;
//    private UILayoutBehavior parent;
//    public void SecretMenuTooltip(UIWindowBehavior popup)
//    {
//        TextTooltipWindowContents.InstanceTooltip(popup, "Toggle Secret Menu");
//    }
//    private bool isToggleSecretMenuAdded = false;
//    protected override void Awake()
//    {
//        base.Awake();

//        if (!isToggleSecretMenuAdded)
//        {
//            parent = Manager<UIManager>.Instance.AddHorizontalGroup(base.window);
//            toggleSecretMenu = (UIIconButtonBehavior)Manager<UIManager>.Instance.AddIconButton(parent, CustomTabTool.ToggleSecretMenu)
//                .SetIcon(PropManager.Instance.IsVisibleManual("Roofs") ? (Texture)TrayUIAssets.Instance.roofsIcon : (Texture)TrayUIAssets.Instance.noRoofsIcon)
//                .SetButton(SharedUIAssets.Instance.thickButton)
//                .SetWidth(52f)
//                .SetHeight(34f)
//                .SetTooltipProvider(SecretMenuTooltip);

//            isToggleSecretMenuAdded = true;
//        }

//        Manager<FlowManager>.Instance.AddGate("Oct.Flow.SecretMenu", delegate (bool visible)
//        {
//            toggleSecretMenu.SetActive(visible);
//        });

//        FlowManager.Instance.AddGate("Oct.Flow.SecretMenuToggle", visible => toggleSecretMenu.SetActive(toggleSecretMenu));
//        FlowManager.Instance.OpenGate("Oct.Flow.SecretMenuToggle");
//    }

//    public override void Refresh()
//    {
//        base.Refresh();

//    }
//}