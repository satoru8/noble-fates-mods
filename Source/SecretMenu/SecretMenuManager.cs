﻿using System;
using System.Reflection;
using UnityEngine;

public class SecretMenuManager : Manager<SecretMenuManager>
{
    SecretMenuWindowContents secretMenu;
    private UIManager uiManager = UIManager.Instance;

    public bool debugButtonVisible = false;
    public UIButtonBehavior debugButton;

    public string tabToolTitle = "Secret Menu";
    public string menuTitle = "Secret Menu";
    public string menuGate = "Oct.Flow.Tools.Secret";
    public string menuHotkey = "Oct.Settings.Global.Controls.Tools.SecretMenu";

    [OctSaveIgnoreField]
    Texture path;

    public override void BeginInitialization()
    {
        base.BeginInitialization();
        try
        {
            new TabTool(path, tabToolTitle, menuGate, sort: 99).AddActivatedCallback(ShowSecretMenu).AddDeactivatedCallback(HideSecretMenu);
            InputManager.Instance.AddHotKey(menuHotkey, ToggleSecretMenu);
            FlowManager.Instance.MarkTransientGate(menuGate);
            FlowManager.Instance.OpenGate(menuGate);
            //TabToolEntry buttonsParentEntry = new TabToolEntry(customTabTool, null, null, "ButtonsParent", null).SetSort(0);
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to add Secret Menu. Error: {e.Message}");
        }
    }

    //public override void Tick(float dt)
    //{
    //    base.Tick(dt);
    //    if (Input.GetKey(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.M))
    //    {
    //        ToggleSecretMenu();
    //    }
    //}

    public void ShowSecretMenu()
    {
        if (secretMenu == null)
        {
            UIWindowScene uIWindowScene = Manager<UIManager>.Instance.PushScene<UIWindowScene>();
            UIWindowBehavior window = uIWindowScene.window;
            secretMenu = window.InstanceContents<SecretMenuWindowContents>();
            ToolsManager.Instance.PushToolbarInFront();
        }
    }

    public void HideSecretMenu()
    {
        if (secretMenu != null)
        {
            secretMenu.window.Close();
            ToolsManager.Instance.PopToolbarInFront();
            ToolsManager.Instance.DeactivateAll();
        }
    }

    public void ToggleDebugButton(UIToggleBehavior.Value value)
    {
        debugButtonVisible = value == UIToggleBehavior.Value.On;
        debugButton?.SetActive(debugButtonVisible);
    }

    public void NewButton(UILayoutBehavior parent, string buttonText, Action onClick)
    {
        try
        {
            UIButtonBehavior button = uiManager.AddButton(parent, buttonText, onClick);
            button.SetWrapping(true).SetHeight(80).SetWidth(100);
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to add button. Error: {e.Message}");
        }
    }

    public void ToggleSecretMenu()
    {
        if (FlowManager.Instance.IsGateOpen(menuGate))
            FlowManager.Instance.CloseGate(menuGate);
        else
            FlowManager.Instance.OpenGate(menuGate);
    }

    public void ToggleDebugMenu()
    {
        if (FlowManager.Instance.IsGateOpen("Oct.Flow.Debug"))
            FlowManager.Instance.CloseGate("Oct.Flow.Debug");
        else
            FlowManager.Instance.OpenGate("Oct.Flow.Debug");
    }

    public void CharacterDebug()
    {
        ToggleProperty(CharacterManager.Instance, "debugEnabled");
    }

    public void CharacterFullControl()
    {
        ToggleProperty(CharacterManager.Instance, "fullControlEnabled");
    }

    public void NoBuildingCosts()
    {
        PropManager.PlaceCompletedStructures = !PropManager.PlaceCompletedStructures;
    }

    public void UnlockAllResearch()
    {
        ToggleProperty(ResearchManager.Instance, "allUnlocked");
        ManagerBehavior.Instance.ResearchChanged();
    }

    public void InstantDig()
    {
        TerrainManager.Instance.digImmediate = !TerrainManager.Instance.digImmediate;
    }

    public void EffectsEditor()
    {
        CloseButton();
        EffectsManager.Instance.ShowEditor();
    }
    public void PropDebug()
    {
        ToggleProperty(PropManager.Instance, "debugEnabled");
    }

    private void ToggleProperty(object targetObject, string propertyName)
    {
        if (targetObject == null || string.IsNullOrEmpty(propertyName))
        {
            Debug.LogError("Target object or property name is null.");
            return;
        }

        PropertyInfo property = targetObject.GetType().GetProperty(propertyName);
        if (property == null)
        {
            Debug.LogError($"Property '{propertyName}' not found on type '{targetObject.GetType()}'.");
            return;
        }

        if (property.PropertyType != typeof(bool))
        {
            Debug.LogError($"Property '{propertyName}' is not of type bool.");
            return;
        }

        bool currentValue = (bool)property.GetValue(targetObject);
        property.SetValue(targetObject, !currentValue);
    }

    private void ChangePropertyInt(object targetObject, string propertyName, int value)
    {
        if (targetObject == null || string.IsNullOrEmpty(propertyName))
        {
            Debug.LogError("Target object or property name is null.");
            return;
        }

        PropertyInfo property = targetObject.GetType().GetProperty(propertyName);
        if (property == null)
        {
            Debug.LogError($"Property '{propertyName}' not found on type '{targetObject.GetType()}'.");
            return;
        }

        if (property.PropertyType != typeof(int))
        {
            Debug.LogError($"Property '{propertyName}' is not of type int.");
            return;
        }

        property.SetValue(targetObject, value);
    }

    public void CloseButton()
    {
        if (secretMenu.window != null)
        {
            secretMenu.window.Close();
            ToolsManager.Instance.DeactivateAll();
        }
    }
}