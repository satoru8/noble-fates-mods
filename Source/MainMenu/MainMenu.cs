﻿using HarmonyLib;
using System;

namespace MainMenuReturnOption
{
    class MainMenuReturnOptionBus : OctModBus
    {
        public override void Loaded()
        {
            var harmony = new Harmony("MainMenuReturnOption");

            harmony.Patch(
                original: AccessTools.Method(typeof(UIMenuScene), "AddMenuEntry"),
                postfix: new HarmonyMethod(typeof(MainMenuReturnOptionBus), nameof(AddReturnButton))
            );
        }

        static void AddReturnButton(UIMenuScene __instance, string caption, Action clicked, int total, bool animate = true)
        {
            if (!ManagerBehavior.Instance.isMainMenu && caption == "Resume")
            {
                AddMenuEntry(__instance, "Return to Main Menu", ReturnToMainMenu, total + 1, animate);
            }
        }

        public static void ReturnToMainMenu()
        {
            switch (ManagerBehavior.Instance.state)
            {
                case ManagerBehavior.GameState.Initializing:
                case ManagerBehavior.GameState.Loading:
                    break;
                case ManagerBehavior.GameState.Playing:
                    if (ManagerBehavior.Instance.SaveGame(out string filename))
                    {
                        UIMenuScene menuScene = FindMenuScene();
                        if (menuScene != null)
                        {
                            UIManager.Instance.sceneStack.Clear();
                            //ManagerBehavior.Instance.ClearPointerTarget();
                            ManagerBehavior.Instance.ResetGame();
                            //ManagerBehavior.Instance.Awake();
                            ManagerBehavior.Instance.SetMainMenu(true);
                            UIManager.Instance.PushScene<UIMenuScene>();
                            ManagerBehavior.Instance.LoadGame("MainMenu");
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private static UIMenuScene FindMenuScene()
        {
            foreach (var scene in UIManager.Instance.sceneStack)
            {
                if (scene is UIMenuScene menuScene)
                {
                    return menuScene;
                }
            }
            return null;
        }

        private static void AddMenuEntry(UIMenuScene menuScene, string caption, Action clicked, int total, bool animate = true)
        {
            var addMenuEntryMethod = typeof(UIMenuScene).GetMethod("AddMenuEntry", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

            addMenuEntryMethod.Invoke(menuScene, new object[] { caption, clicked, total, animate });
        }
    }
}
