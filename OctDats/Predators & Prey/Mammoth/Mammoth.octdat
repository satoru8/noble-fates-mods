{
	id Oct.Characters.Animals.Mammoth
	type CharacterType
	inherit Oct.Characters.Animals.Animal
	
	animalLine = Honk honk
	name = Mammoth	
	plural = Mammoths
    scale = 1.4

	runRate = .25
    attackRange = 2.1
    herd = true
    herdThreshold = 4
    perceptionRange = 8
    perKilometer = 2
    combatIdle = Armature|CombatIdle
	corpseType = <Oct.Items.Corpse.Animals.Mammoth>

    weight = 0
    rabidCost = 7
    dangerousCost = 7

	tamingResearchEntry = 
    {
        id Oct.Research.Ranching.MammothTaming
        type ResearchEntry
        inherit Oct.Research.Ranching.Base

        name = Mammoth Taming
        description = Ability to Tame Mammoths
        prerequisite = <Oct.Research.Ranching>
        tier = 11
    }

    meleeDamage = 7
	attackRate = .3
    aggression = .6
    value = 65
    
    isHerbavoreAnimal = true
    isCarnivoreAnimal = false
    eatAnimation = Armature|Eat
    
    hideName = Mammoth Hide
    leatherName = Mammoth Leather
    hideColor = color(.625, .625, .625)
	
	prefab = 
	{
		type Prefab
		path = /Assets/Mammoth/Mammoth.fbx
		name = Mammoth
        behavior = CharacterBehavior
	}
	
	variants = 
	[
		{
			id Oct.Characters.Animals.Mammoth.Brown
			type CharacterTypeVariant

			name = Mammoth

			names = 
			[
				Mammoth
			]

            materialSlots = 
            [
                {
                    id Oct.Characters.Animals.Mammoth.Brown.Fur
                    type CharacterTypeMaterialSlot
                    name = Fur
                    materials = 
                    [
                        Fur
                    ]
                    definitions = 
                    [
                        {
                            id Oct.Characters.Animals.Mammoth.Brown.Fur.Color
                            type ColorCharacterTypeMaterialDefinition
                            colors = 
                            [
                                {
                                    type CharacterTypeMaterialColor
                                    hueFrom = .04
                                    hueTo = .09
                                    saturationFrom = .15
                                    saturationTo = .9
                                    valueFrom = .2
                                    valueTo = .5
                                }
                            ]
                        }
                    ]
                }
            ]
		}
        {
			id Oct.Characters.Animals.Mammoth.Black
			type CharacterTypeVariant

			name = Elder Mammoth

			names = 
			[
				Elder Mammoth
			]

            materialSlots = 
            [
                {
                    id Oct.Characters.Animals.Mammoth.Black.Fur
                    type CharacterTypeMaterialSlot
                    name = Fur
                    materials = 
                    [
                        Fur
                    ]
                    definitions = 
                    [
                        {
                            id Oct.Characters.Animals.Mammoth.Black.Fur.Color
                            type ColorCharacterTypeMaterialDefinition
                            colors = 
                            [
                                {
                                    type CharacterTypeMaterialColor
                                    hueFrom = 0
                                    hueTo = 0
                                    saturationFrom = 0
                                    saturationTo = 0
                                    valueFrom = .011
                                    valueTo = .25
                                }
                            ]
                        }
                    ]
                }
            ]
		}
        {
			id Oct.Characters.Animals.Mammoth.White
			type CharacterTypeVariant

			name = Artic Mammoth

			names = 
			[
				Artic Mammoth
			]

            materialSlots = 
            [
                {
                    id Oct.Characters.Animals.Mammoth.White.Fur
                    type CharacterTypeMaterialSlot
                    name = Fur
                    materials = 
                    [
                        Fur
                    ]
                    definitions = 
                    [
                        {
                            id Oct.Characters.Animals.Mammoth.White.Fur.Color
                            type ColorCharacterTypeMaterialDefinition
                            colors = 
                            [
                                {
                                    type CharacterTypeMaterialColor
                                    hueFrom = 0
                                    hueTo = 0
                                    saturationFrom = 0
                                    saturationTo = 0
                                    valueFrom = .4
                                    valueTo = .6
                                }
                            ]
                        }
                    ]
                }
            ]
		}
	]

	stats = 
	[
		{
			id Oct.Characters.Animals.Mammoth.Stats.MoveSpeed
			type StatType
			name = MoveSpeed
			min = 0
			max = 1000000
			initial = 0.7
			hidden = true
		}
		{
			id Oct.Characters.Animals.Mammoth.Stats.Health
			type StatType	
			name = Health
			min = 0
			max = 90
			initial = 90
		}
	]

	audio = 
	[
		{
			type AudioBankHandle			
			name = Death
			handle = 
			{
				type AudioClipHandle				
				path = /SFX/Wolf/Death.wav
			}
		}
		{
			type AudioBankHandle			
			name = Hurt
			handle = 
			{
				type AudioHandles
                audioChances = 
                [
                    {
                        type AudioHandleChance
                        chance = 1
                        audio =
                        {
                            type AudioClipHandle				
                            path = /SFX/Wolf/Hurt1.wav
                            globalCooldown = 3
                        }
                    }
                    {
                        type AudioHandleChance
                        chance = 1
                        audio = 
                        {
                            type AudioClipHandle				
                            path = /SFX/Wolf/Hurt2.wav
                            globalCooldown = 3
                        }
                    }
                    {
                        type AudioHandleChance
                        chance = 10
                    }
                ]
            }
		}
		{
			type AudioBankHandle			
			name = Aware
			handle = 
			{
				type AudioHandles
                audioChances = 
                [
                    {
                        type AudioHandleChance
                        chance = 1
                        audio =
                        {
                            type AudioClipHandle				
                            path = /Assets/Mammoth/Mammoth1.wav
                            globalCooldown = 1
                        }
                    }
                ]
            }
		}
	]

	animations = 
	[
		{
			type AnimationTrack			
			name = Armature|Eat
			loop = true
		}
		{
			type AnimationTrack			
			name = Armature|CombatIdle
			loop = true
		}
		?(name == Armature|CombatStrafeForward) {
			type AnimationTrack			
			loop = false
		}
		?(name == Armature|Attack) {
			type AnimationTrack
            notifies = 
            [
                {
                    type AudioNotify
                    time = 0
                    audio = 
                    {
                        type AudioHandles			
                        audioChances = 
                        [
                            {
                                type AudioHandleChance
                                chance = 1
                                audio =
                                {
                                    type AudioClipHandle				
                                    path = /Assets/Mammoth/Mammoth1.wav
                                    globalCooldown = 1
                                }
                            }
                            {
                                type AudioHandleChance
                                chance = 1
                                audio = 
                                {
                                    type AudioClipHandle				
                                    path = /Assets/Mammoth/Mammoth2.wav
                                    globalCooldown = 1
                                }
                            }
                        ]
                    }
                }
            ]                
		}
	]
	
	instincts = 
	[
		{
			type RelaxInstinct
			timeFrom = 1
			timeTo = 6
            distFrom = 1
            distTo = 10
		}
	]
}