{
	abstract
	id Fantasy.Items.ShieldBase
	type InstancedItemType
	inherit Oct.Items.Buffable
	
	itemClass = ArmorInstancedItem
	category = <Oct.Items.Categories.Gear.Shields>
	indoorDecayAfter = 0
	outdoorDecayAfter = 0
	equipmentSlot = Secondary
	cosmeticPrefab = true
	salvageAt = <Fantasy.Props.FantasyCraftingTable>
	
	staticBuffs =
	[
		{
			type StatModifierInstancedItemStaticBuff
			statName = Mobility
			modifier = -.005
			perc = true
			good = false
		}
		{
			type StatModifierInstancedItemStaticBuff
			statName = Dexterity
			modifier = -.01
			perc = true
			good = false
		}
	]
}
{
	id Fantasy.Items.Shield.Dragon
	type InstancedItemType
	inherit Fantasy.Items.ShieldBase
	
	name = Dragon Shield
	pluralName = Dragon Shields
	
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
			modifier = true
			min = 7
			max = 11
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 54
			max = 242
		}
	]
	
	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Fantasy.Items.DragonScales>
			count = 5
		}
	]
	
	prefab =
	{
		type Prefab
		path = /Assets/Items/DragonShield.fbx
		name = DragonShield
		behavior = InstancedItemBehavior
		
        // sharedMaterials = 
        // [
        //     {
        //         type OctDatSharedMaterial
        //         names = 
        //         [
        //             Face
        //         ]
        //         material = <Oct.Items.Materials.Dragon.Face>
        //     }
        // ]
	}
	
	equipmentCosmetic =
	{
		type StaticCharacterCosmetic
		
		boneTransforms =
		[
			{
				type StaticCharacterCosmeticTransform
				bone = Back1
				rotation = rotation(0, -62.5, 180)
				translation = vector(0, -.15625, .0625)
			}
			{
				type StaticCharacterCosmeticTransform
				bone = LeftLowerArm
				rotation = rotation(0, 0, 90)
				translation = vector(.1, 0, 0.035)
			}
		]
		
		prefab =
		{
			type Prefab
			path = /Assets/Items/DragonShield.fbx
			name = DragonShield
			behavior = StaticCharacterCosmeticBehavior
			
            // sharedMaterials = 
            // [
            //     {
            //         type OctDatSharedMaterial
            //         names = 
            //         [
            //             Face
            //         ]
            //         material = <Oct.Items.Materials.Dragon.Face>
            //     }
            // ]
		}
	}
}
{
	id Fantasy.Items.Recipes.Shield.Dragon
	type ItemRecipe
	
	researchEntry = <Fantasy.Research.Shield.Dragon>
	type = <Fantasy.Items.Shield.Dragon>
	commandType = CraftItemCommand
	craftingLoop = WoodCraftingLoop
	progressMultiplier = .325
	qualityDifficulty = 1.2
	skillCurve = .8
	
	craftAt =
	[
		<Fantasy.Props.FantasyCraftingTable>
	]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Fantasy.Items.DragonScales>
			count = 10
		}
	]
}