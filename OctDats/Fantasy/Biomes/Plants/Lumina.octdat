{
	id Fantasy.Plants.Lumina
	type FoliageType
	inherit Oct.Foliage
	
	name = Lumina
	minScale = .25
	maxScale = 1
	maturationYears = 1
	lifetimeYears = 8
	healthPerCutMultiplier = 10
	farmName = Lumina
	farmingSpacing = .5
	plantItemFilter = <Fantasy.Items.LuminaSeed>
	noCollision = true
	cutAudio = CutFoliage
	plantSoilClump = true
	immatureEmission = 2.5
	matureEmission = 5
	
	plantResearchEntry =
	{
		id Fantasy.Research.Farming.Lumina
		type ResearchEntry
		inherit Oct.Research.Farming.Base
		
		name = Lumina Farm
		prerequisite = <Fantasy.Research.Farming>
		tooltipProvider = <Fantasy.Plants.Lumina>
		tierOffset = 8
	}
	
	stages =
	[
		{
			id Fantasy.Plants.Lumina.Stage
			type FoliageStage
		}
		{
			id Fantasy.Plants.Lumina.Stage1
			type FoliageStage
			maturation = .01
			prefab =
			{
				type Prefab
				path = /Assets/Plants/LuminaPlant.fbx
				name = Lumina Early
				behavior = FoliageBehavior
				scale = 0.25
				sharedMaterials =
				[
					<Fantasy.Materials.LumaBerry>
				]
			}
		}
		{
			id Fantasy.Plants.Lumina.Stage2
			type FoliageStage
			maturation = .5
			prefab =
			{
				type Prefab
				path = /Assets/Plants/LuminaPlant.fbx
				name = Lumina Not Ripe
				behavior = FoliageBehavior
				scale = 0.5
				sharedMaterials =
				[
					<Fantasy.Materials.LumaBerry>
				]
			}
		}
		{
			id Fantasy.Plants.Lumina.Stage3
			type FoliageStage
			ripeness = .5
			prefab =
			{
				type Prefab
				path = /Assets/Plants/LuminaPlant.fbx
				name = Lumina Near Ripe
				behavior = FoliageBehavior
				scale = 0.75
				sharedMaterials =
				[
					<Fantasy.Materials.LumaBerry>
				]
			}
		}
		{
			id Fantasy.Plants.Lumina.Ripe
			type FoliageStage
			ripeness = 1
			prefab =
			{
				type Prefab
				path = /Assets/Plants/LuminaPlant.fbx
				name = Lumina Ripe
				behavior = FoliageBehavior
				sharedMaterials =
				[
					<Fantasy.Materials.LumaBerry>
				]
			}
		}
	]
	
	seasonalMaterials =
	[
		Stem
	]
	
	seasons =
	[
		{
			type FoliageSeason
			
			growRate = 1
			
			seasons =
			[
				Spring
				Summer
			]
			
			colors =
			{
				type FoliageSeasonalColor
				
				colorFrom = color(#053d11)
				colorTo = color(#053d11)
			}
		}
		{
			type FoliageSeason
			
			growRate = .75
			
			seasons =
			[
				Fall
			]
			
			colors =
			{
				type FoliageSeasonalColor
				
				colorFrom = color(#053d11)
				colorTo = color(#053d11)
			}
		}
		{
			type FoliageSeason
			
			growRate = .5
			
			seasons =
			[
				Winter
			]
			
			colors =
			{
				type FoliageSeasonalColor
				
				colorFrom = color(#053d11)
				colorTo = color(#053d11)
			}
		}
	]
	
	fruitType =
	{
		id Fantasy.Luminaplant.Fruit
		type FruitType
		
		stages =
		[
			{
				type FruitStage
				name = Unripe
				ripeness = .5
				yield = 0
			}
			{
				type FruitStage
				name = Ripe
				ripeness = 1
				yield = 5
				itemType = <Fantasy.Items.LumaBerrySlice>
			}
		]
		
		transform = Berry
		yield = 6
		ripenessPerDay = .2
		dieAfterYield = false
	}
}