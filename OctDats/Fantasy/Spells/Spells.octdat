// Shadow Step
{
	id Fantasy.Abilities.ShadowStep
	type AbilityType
	
	name = Shadow Step
	description = User untargetable for 20m<br>2h Cooldown After Use
	icon = tex(/Assets/Icons/A_Shadowstep.png)
	modifierIcon = tex(/Assets/Icons/A_Shadowstep.png)
	weight = 0.2
	limit = 1
	
	attributes =
	[
		Melee
		Ranged
		Spell Casting
	]
	
	activation =
	{
		type ActiveAbilityActivationType
		lengthMinutes = 2
		cooldownMinutes = 120
	}
	
	targetType =
	{
		type SelfAbilityTargetType
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Shadow Step
			statusEffectMinutes = 20
		}
		{
			type EffectAbilityActionType
			effect = <Fantasy.Effects.ShadowStep>
			self = true
		}
	]
}
{
	id Fantasy.StatusEffects.ShadowStep
	type StatusEffectType
	
	name = Shadow Step
	effectName = Shadow Step
	icon = tex(/Assets/Icons/A_Shadowstep.png)
	iconColor = Neutral
	effectType = IgnoredStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitSort = 25
}

// Healing Spells
{
	id Fantasy.Abilities.LesserHealing
	type AbilityType
	
	name = Lesser Healing
	description = Heals target for 0.3 Health per minute over 10minutes<br>4 tile range
	icon = tex(/Assets/Icons/A_Healing.png)
	weight = 0.2
	limit = 1
	
	attributes =
	[
		Spell Casting
	]
	
	activation =
	{
		type SpellActiveAbilityActivationType
		lengthMinutes = 2
		cooldownMinutes = 60
	}
	
	targetType =
	{
		type SelectedCharacterAbilityTargetType
		range = 4
		selfIfNoTarget = true
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Lesser Healing
			statusEffectMinutes = 10
		}
		{
			type EffectAbilityActionType
			effect = <Oct.Effects.HealingAura>
		}
		{
			type SoundAbilityActionType
			audio =
			{
				type AudioClipHandle
				path = /SFX/Magic/healing_magic_spell_02.wav
				volume = .25
			}
		}
	]
}
{
	id Fantasy.StatusEffects.LesserHealing
	type StatModifierStatusEffectType
	
	name = Lesser Healing
	statName = HealthPerMinute
	statDelta = 0.3
	effectName = Lesser Healing
	icon = tex(/Assets/Icons/A_Healing.png)
	iconColor = Good
	effectType = StatModifierStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitSort = 0
}

{
	id Fantasy.Abilities.Healing
	type AbilityType
	
	name = Healing
	description = Heals target for 0.6 Health per minute over 10 minutes<br>5 tile range
	icon = tex(/Assets/Icons/A_Healing.png)
	weight = 0.15
	limit = 1
	
	attributes =
	[
		Spell Casting
	]
	
	activation =
	{
		type SpellActiveAbilityActivationType
		lengthMinutes = 3
		cooldownMinutes = 60
	}
	
	targetType =
	{
		type SelectedCharacterAbilityTargetType
		range = 5
		selfIfNoTarget = true
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Healing
			statusEffectMinutes = 10
		}
		{
			type EffectAbilityActionType
			effect = <Oct.Effects.HealingAura>
		}
		{
			type SoundAbilityActionType
			audio =
			{
				type AudioClipHandle
				path = /SFX/Magic/healing_magic_spell_02.wav
				volume = .25
			}
		}
	]
}
{
	id Fantasy.StatusEffects.Healing
	type StatModifierStatusEffectType
	
	name = Healing
	statName = HealthPerMinute
	statDelta = 0.6
	effectName = Healing
	icon = tex(/Assets/Icons/A_Healing.png)
	iconColor = Good
	effectType = StatModifierStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitSort = 0
}

{
	id Fantasy.Abilities.SoothingRenewal
	type AbilityType
	
	name = Soothing Renewal
	description = Heals team for 0.25 Health per minute over 30 minutes<br>4 tile range
	icon = tex(/Assets/Icons/A_Healing.png)
	weight = 0.1
	limit = 1
	
	attributes =
	[
		Spell Casting
	]
	
	activation =
	{
		type SpellActiveAbilityActivationType
		lengthMinutes = 3
		cooldownMinutes = 120
	}
	
	targetType =
	{
		type SelectedCharacterAbilityTargetType
		range = 4
		selfIfNoTarget = false
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Soothing Renewal
			statusEffectMinutes = 30
		}
		{
			type EffectAbilityActionType
			effect = <Oct.Effects.HealingAura>
		}
		{
			type SoundAbilityActionType
			audio =
			{
				type AudioClipHandle
				path = /SFX/Magic/healing_magic_spell_02.wav
				volume = .25
			}
		}
	]
}
{
	id Fantasy.StatusEffects.SoothingRenewal
	type StatModifierStatusEffectType
	
	name = Soothin Renewal
	statName = HealthPerMinute
	statDelta = 0.25
	effectName = Soothing Renewal
	icon = tex(/Assets/Icons/A_Healing.png)
	iconColor = Good
	effectType = StatModifierStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitSort = 0
}

// Attack Spells
{
	id Fantasy.Abilities.FireBlast
	type AbilityType
	
	name = Fire Blast
	description = Burns all targets for 30 minutes<br>2 tile radius
	icon = tex(/Assets/Icons/A_Fireblast.png)
	weight = 0.25
	limit = 1
	
	attributes =
	[
		Spell Casting
	]
	
	activation =
	{
		type SpellActiveAbilityActivationType
		lengthMinutes = 5
		cooldownMinutes = 60
	}
	
	targetType =
	{
		type EnemiesWithinRangeAbilityTargetType
		range = 2
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Fire Blast
			statusEffectMinutes = 20
		}
		{
			type EffectAbilityActionType
			effect = <Fantasy.Effects.FireBombs>
			self = true
		}
	]
}
{
	id Fantasy.StatusEffects.FireBlast
	type StatModifierStatusEffectType
	
	name = Fire Blast
	effectName = Fire Blast
	statName = HealthPerMinute
	statDelta = -0.2
	icon = tex(/Assets/Icons/A_Fireblast.png)
	effectType = StatModifierStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitThrobStrength = 2
	portraitSort = 0
}

{
	id Fantasy.Abilities.Enflame
	type AbilityType
	
	name = Enflame
	description = Burns a single target for 0.4 damage per minute over 20 minutes<br>4 tile range
	icon = tex(/Assets/Icons/A_Enflame.png)
	weight = 0.2
	limit = 1
	
	attributes =
	[
		Spell Casting
	]
	
	activation =
	{
		type SpellActiveAbilityActivationType
		lengthMinutes = 3
		cooldownMinutes = 90
	}
	
	targetType =
	{
		type SelectedCharacterAbilityTargetType
		range = 4
		selfIfNoTarget = false
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Enflame
			statusEffectMinutes = 20
		}
		{
			type EffectAbilityActionType
			effect = <Fantasy.Effects.Enflame>
			self = false
		}
		{
			type SoundAbilityActionType
			audio =
			{
				type AudioClipHandle
				path = /SFX/Magic/healing_magic_spell_02.wav
				volume = .25
			}
		}
	]
}
{
	id Fantasy.StatusEffects.Enflame
	type StatModifierStatusEffectType
	
	name = Enflame
	statName = HealthPerMinute
	statDelta = -0.4
	effectName = Enflame
	icon = tex(/Assets/Icons/A_Enflame.png, point)
	iconColor = Bad
	effectType = StatModifierStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitSort = 0
}

{
	id Fantasy.Abilities.PoisonousSmite
	type AbilityType
	
	name = Poisonous Smite
	description = Poisons a single target for 0.175 damage per minute over 60 minutes<br>4 tile range
	icon = tex(/Assets/Icons/A_PoisonSmite.png)
	weight = 0.15
	limit = 1
	
	attributes =
	[
		Spell Casting
	]
	
	activation =
	{
		type SpellActiveAbilityActivationType
		lengthMinutes = 4
		cooldownMinutes = 90
	}
	
	targetType =
	{
		type SelectedCharacterAbilityTargetType
		range = 4
		selfIfNoTarget = false
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Poisonous Smite
			statusEffectMinutes = 60
		}
		// {
		// 	type EffectAbilityActionType
		// 	effect = <Fantasy.Effects.Slash>
		// }
		// {
		// 	type SoundAbilityActionType
		// 	audio =
		// 	{
		// 		type AudioClipHandle
		// 		path = /SFX/Magic/healing_magic_spell_02.wav
		// 		volume = .25
		// 	}
		// }
	]
}
{
	id Fantasy.StatusEffects.PoisonousSmite
	type StatModifierStatusEffectType
	
	name = Poisonous Smite
	statName = HealthPerMinute
	statDelta = -0.175
	effectName = Poisonous Smite
	icon = tex(/Assets/Icons/A_PoisonSmite.png, point)
	iconColor = Bad
	effectType = StatModifierStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitSort = 0
}

{
	id Fantasy.Abilities.Stasis
	type AbilityType
	
	name = Stasis
	description = Stops enemies in their tracks for 20 minutes<br><br>Single target<br>6 tile range
	icon = tex(/Assets/Icons/A_Stasis.png)
	weight = 0.3
	limit = 1
	
	attributes =
	[
		Spell Casting
	]
	
	activation =
	{
		type SpellActiveAbilityActivationType
		lengthMinutes = 3
		cooldownMinutes = 180
	}
	
	targetType =
	{
		type SelectedCharacterAbilityTargetType
		range = 6
		selfIfNoTarget = false
	}
	
	actions =
	[
		{
			type StatusEffectAbilityActionType
			statusEffectName = Stasis
			statusEffectMinutes = 20
		}
		{
			type EffectAbilityActionType
			effect = <Fantasy.Effects.Stasis>
		}
		{
			type SoundAbilityActionType
			audio =
			{
				type AudioClipHandle
				path = /SFX/Magic/healing_magic_spell_02.wav
				volume = .25
			}
		}
	]
}
{
	id Fantasy.StatusEffects.Stasis
	type StatModifierStatusEffectType
	
	name = Stasis
	statName = Mobility
	statDelta = -200
	effectName = Stasis
	icon = tex(/Assets/Icons/A_Stasis.png, point)
	iconColor = Bad
	effectType = StatModifierStatusEffect
	lifetime = -1
	portraitIcon = true
	portraitSort = 0
}

// Passives
{
	id Fantasy.Abilities.Passive.Gifted
	type AbilityType
	inherit Oct.Abilities.AttributePassive
	
	name = Gifted
	description = +1 to Melee/Ranged/Building/Cooking/Crafting
	weight = 0.01
	limit = 1
	
	icon = tex(/Assets/Icons/Gifted.png)
	
	attributes =
	[
		Melee
		Ranged
		Building
		Cooking
		Crafting
	]
	
	actions =
	[
		{
			type StatModifierAbilityActionType
			stat = Melee
			delta = 1
		}
		{
			type StatModifierAbilityActionType
			stat = Ranged
			delta = 1
		}
		{
			type StatModifierAbilityActionType
			stat = Building
			delta = 1
		}
		{
			type StatModifierAbilityActionType
			stat = Cooking
			delta = 1
		}
		{
			type StatModifierAbilityActionType
			stat = Crafting
			delta = 1
		}
	]
}

{
	id Fantasy.Abilities.Passive.ToughSkin
	type AbilityType
	inherit Oct.Abilities.AttributePassive
	
	name = Tough Skin
	description = +10 HP & +5 Armor
	icon = tex(/Assets/Icons/ToughSkin.png)
	weight = 0.1
	limit = 1
	
	attributes =
	[
		MaxHealth
		Armor
	]
	
	actions =
	[
		{
			type StatModifierAbilityActionType
			stat = MaxHealth
			delta = 10
		}
		{
			type StatModifierAbilityActionType
			stat = Armor
			delta = 5
		}
	]
}

{
	id Fantasy.Abilities.Passive.ArcherSigil
	type AbilityType
	inherit Oct.Abilities.AttributePassive
	
	name = Archers Sigil
	description = +4 Ranged -4 Melee
	icon = tex(/Assets/Icons/ArchersSigil.png)
	weight = 0.05
	limit = 1
	
	attributes =
	[
		Melee
		Ranged
	]
	
	actions =
	[
		{
			type StatModifierAbilityActionType
			stat = Melee
			delta = -4
		}
		{
			type StatModifierAbilityActionType
			stat = Ranged
			delta = 4
		}
	]
}

{
	id Fantasy.Abilities.Passive.WarriorsCrest
	type AbilityType
	inherit Oct.Abilities.AttributePassive
	
	name = Warriors Crest
	description = +4 Melee -4 Ranged
	icon = tex(/Assets/Icons/WarriorsCrest.png)
	weight = 0.05
	limit = 1
	
	attributes =
	[
		Melee
		Ranged
	]
	
	actions =
	[
		{
			type StatModifierAbilityActionType
			stat = Melee
			delta = 4
		}
		{
			type StatModifierAbilityActionType
			stat = Ranged
			delta = -4
		}
	]
}