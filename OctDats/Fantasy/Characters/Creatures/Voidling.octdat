{
	id Fantasy.Creatures.Voidling
	type CharacterType
	inherit Oct.Characters.Animals.Animal
	
	scale = .7
	animalLine = ????
	name = Voidling
	plural = Voidlings
	runRate = 1
	strafeSpeed = 1
	strafeLength = 1
	attackRange = 1
	canGotoStrafe = false
	herd = true
	perceptionRange = 4
	combatIdle = Armature|CombatIdle
	corpseType = <Fantasy.Corpse.Voidling>
	lootValue = 30
	
	rabidCost = 0
	dangerousCost = 0
	
	meleeDamage = 3
	attackRate = .2
	attackSpeed = 1
	aggression = 2
	value = 1000
	
	isCarnivoreAnimal = true
	eatAnimation = Armature|Eat
	
	playingFacets =
	[
		<Fantasy.Profiles.Voidling>
	]
	
	tamingResearchEntry = <Fantasy.Research.CreatureTaming.Voidling>
	
	prefab =
	{
		type Prefab
		path = /Assets/Creatures/Voidling.fbx
		name = Voidling
		behavior = CharacterBehavior
	}
	
	rightAttacks =
	[
		$clear
		Armature|Attack
		Armature|Attack2
	]
	
	variants =
	[
		{
			id Fantasy.Creatures.Voidling.black
			type CharacterTypeVariant
			
			name = Voidling
			
			names =
			[
				Voidling
			]
		}
	]
	
	stats =
	[
		{
			id Fantasy.Creatures.Voidling.MoveSpeed
			type StatType
			name = MoveSpeed
			min = 0
			max = 1000000
			initial = .7
			hidden = true
		}
		{
			id Fantasy.Creatures.Voidling.Health
			type StatType
			name = Health
			min = 0
			max = 32
			initial = 32
		}
		-(name == FoodPerDay)
		{
			id Fantasy.Creatures.Voidling.FoodPerDay
			type StatType
			inherit Oct.Characters.Stats.FoodPerDay
			initial = -10
		}
		-(name == HealthPerMinute)
		{
			id Fantasy.Creatures.Voidling.HealthPerMinute
			type StatType
			inherit Oct.Characters.Stats.HealthPerMinute
			initial = .1
		}
		
	]
	
	audio =
	[
		{
			type AudioBankHandle
			name = Death
			handle =
			{
				type AudioClipHandle
				// path = /Assets/Voidlings/VoidlingDeath.wav
				path = /SFX/Wolf/Aware1.wav
			}
		}
		{
			type AudioBankHandle
			name = Armature|React
			handle =
			{
				type AudioHandles
				audioChances =
				[
					{
						type AudioHandleChance
						chance = 1
						audio =
						{
							type AudioClipHandle
							// path = /Assets/Voidlings/VoidlingHurt.wav
							path = /SFX/Wolf/Aware1.wav
							globalCooldown = 3
						}
					}
				]
			}
		}
		{
			type AudioBankHandle
			name = Aware
			handle =
			{
				type AudioHandles
				audioChances =
				[
					{
						type AudioHandleChance
						chance = 1
						audio =
						{
							type AudioClipHandle
							// path = /Assets/Voidlings/VoidlingReact.wav
							path = /SFX/Wolf/Aware1.wav
							globalCooldown = 1
						}
					}
					{
						type AudioHandleChance
						chance = 1
						audio =
						{
							type AudioClipHandle
							// path = /Assets/Voidlings/VoidlingReact2.wav
							path = /SFX/Wolf/Aware1.wav
							globalCooldown = 1
						}
					}
				]
			}
		}
	]
	
	animations =
	[
		{
			type AnimationTrack
			name = Armature|Eat
			loop = true
			notifies =
			[
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioClipHandle
						// path = /Assets/Voidlings/VoidlingEat.wav
						path = /SFX/Wolf/Aware1.wav
						globalCooldown = 1
						volume = 0.8
					}
				}
			]
		}
		?(name == Armature|Attack) {
			type AnimationTrack
			name = Armature|Attack
			notifies =
			[
				
				?(message == Damage) {
					type VerbNotify
					time = 1.17
				}
				
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									// path = /Assets/Voidlings/VoidlingHit.wav
									path = /SFX/Wolf/Aware1.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									// path = /Assets/Voidlings/VoidlingHit2.wav
									path = /SFX/Wolf/Aware1.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
		{
			type AnimationTrack
			name = Armature|Attack2
			notifies =
			[
				?(message == Damage) {
					type VerbNotify
					time = 1
				}
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									// path = /Assets/Voidlings/VoidlingHit.wav
									path = /SFX/Wolf/Aware1.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									// path = /Assets/Voidlings/VoidlingHit2.wav
									path = /SFX/Wolf/Aware1.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
	]
	
	instincts =
	[
		{
			type HuntInstinct
		}
		{
			type RelaxInstinct
			timeFrom = 1
			timeTo = 10
			distFrom = 1
			distTo = 15
		}
	]
}