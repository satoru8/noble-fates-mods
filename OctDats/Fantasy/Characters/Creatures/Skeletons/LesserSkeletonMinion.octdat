{
	id Fantasy.Creatures.LesserSkeletonMinion
	type CharacterType
	inherit Fantasy.Creatures.Skeleton.Base
	
	name = Lesser Skeleton
	plural = Lesser Skeletons
	
	corpseType = <Fantasy.Corpse.LesserSkeletonMinion>
	lootValue = 10
	meleeDamage = 1.5
	value = 800
	
	dangerousCost = 10
	herd = true
	herdThreshold = 6
	
	playingFacets =
	[
		<Fantasy.Profiles.LesserSkeleton>
	]
	
	prefab =
	{
		type Prefab
		path = /Assets/Creatures/Skeletons/LesserSkeletonMinion.fbx
		name = LesserSkeletonMinion
		behavior = CharacterBehavior
	}
	
	rightAttacks =
	[
		$clear
		Armature|Attack
		Armature|Attack2
		Armature|Attack3
	]
	
	variants =
	[
		{
			id Oct.Characters.LesserSkeletonMinion
			type CharacterTypeVariant
			
			name = Lesser Skeleton
			names =
			[
				Lesser Skeleton
			]
			
			materialSlots =
			[
				{
					id Fantasy.Creatures.LesserSkeletonMinion.Bones.Skin
					type CharacterTypeMaterialSlot
					name = Bones
					materials =
					[
						Bones
					]
					definitions =
					[
						{
							id Fantasy.Creatures.LesserSkeletonMinion.Bones.Skin.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .112
									hueTo = .12
									saturationFrom = .3
									saturationTo = .54
									valueFrom = .2
									valueTo = .4
								}
							]
						}
					]
				}
			]
		}
	]
	
	stats =
	[
		{
			id Fantasy.Creatures.LesserSkeletonMinion.MoveSpeed
			type StatType
			name = MoveSpeed
			min = 0
			max = 1000000
			initial = .7
			hidden = true
		}
		{
			id Fantasy.Creatures.LesserSkeletonMinion.Health
			type StatType
			name = Health
			min = 0
			max = 30
			initial = 30
		}
		-(name == FoodPerDay)
		{
			id Fantasy.Creatures.LesserSkeletonMinion.FoodPerDay
			inherit Oct.Characters.Stats.FoodPerDay
			type StatType
			initial = -25
		}
		-(name == HealthPerMinute)
		{
			id Fantasy.Creatures.LesserSkeletonMinion.HealthPerMinute
			inherit Oct.Characters.Stats.HealthPerMinute
			type StatType
			initial = .1
		}
	]
	
	animations =
	[
		{
			type AnimationTrack
			name = Armature|Eat
			loop = true
			
		}
		?(name == Armature|Attack) {
			type AnimationTrack
			name = Armature|Attack
			notifies =
			[
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Skeletons/SkeletonAttack.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Skeletons/SkeletonAttack2.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
		{
			type AnimationTrack
			name = Armature|Attack2
			notifies =
			[
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Skeletons/SkeletonAttack.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Skeletons/SkeletonAttack2.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
	]
	
	instincts =
	[
		{
			type RelaxInstinct
			timeFrom = 1
			timeTo = 90
			distFrom = 1
			distTo = 15
		}
	]
}