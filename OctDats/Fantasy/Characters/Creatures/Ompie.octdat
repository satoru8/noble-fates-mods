{
	id Fantasy.Creatures.Ompie
	type CharacterType
	inherit Oct.Characters.Animals.Animal
	
	scale = .75
	animalLine = Rwoaar
	name = Ompie
	plural = Ompies
	runRate = .9
	strafeSpeed = 1
	strafeLength = 1.5
	attackRange = 1.5
	canGotoStrafe = false
	herd = false
	perceptionRange = 8
	combatIdle = Armature|CombatIdle
	corpseType = <Fantasy.Corpse.Ompie>
	lootValue = 25
	rangedFirePos = vector(.2, .55, 1)
	
	rabidCost = 0
	dangerousCost = 0
	
	meleeDamage = 2
	attackRate = .05
	attackSpeed = .8
	aggression = 1
	value = 1000
	
	isCarnivoreAnimal = true
	eatAnimation = Armature|Eat
	
	playingFacets =
	[
		<Fantasy.Profiles.Ompie>
	]
	
	tamingResearchEntry = <Fantasy.Research.CreatureTaming.Ompie>
	
	prefab =
	{
		type Prefab
		path = /Assets/Creatures/Ompie/Ompie.fbx
		name = Ompie
		behavior = CharacterBehavior
	}
	
	rightAttacks =
	[
		$clear
		Armature|Attack
		Armature|Attack2
		
	]
	
	rangedAttackTalents =
	[
		{
			id Fantasy.Creatures.Ompie.RangedAttack
			type TalentCharacterRangedAttackType
			animation = Armature|RangedAttack
			animationSpeed = .7
			damage = 3
			speed = 7
			fullbody = true
			lead = false
			fireAfter = 1.35
			completeAfter = 5
			cameraShake = .15
			
			projectileType =
			{
				id Fantasy.Creatures.Ompie.RangedAttack.Projectile
				type ProjectileType
				
				gravity = .1
				radius = .3
				hitCameraShake = 0
				hitCameraShakeRadius = 4
				foliageDamageScale = .2
				terrainDamageScale = .1
				propDamageScale = .2
				throughUntilDecay = true
				blockable = true
				major = true
				trailLength = 0
				
				hit =
				{
					type AudioClipHandle
					path = /Assets/SFX/Ompie/SlimeHit.wav
					globalCooldown = 0.15
					volume = .25
				}
				
				prefab =
				{
					type Prefab
					path = /Assets/Creatures/Ompie/SlimeBall.fbx
					name = Ompie Magic
					behavior = ProjectileBehavior
				}
				
				landEffectType =
				{
					id Fantasy.Creatures.Ompie.RangedAttack.Land
					type EffectType
					name = MagicMissileLand
					unityPrefab = MagicMissileLand
				}
			}
		}
	]
	
	variants =
	[
		{
			id Fantasy.Creatures.Ompie.Brown
			type CharacterTypeVariant
			
			name = Ompie
			
			names =
			[
				Ompie
			]
			
			
			materialSlots =
			[
				{
					id Fantasy.Creatures.Ompie.Brown.Skin
					type CharacterTypeMaterialSlot
					name = Skin
					materials =
					[
						Skin
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.Brown.Skin.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .13
									hueTo = .26
									saturationFrom = .25
									saturationTo = .6
									valueFrom = .1
									valueTo = .45
								}
							]
						}
					]
				}
				{
					id Fantasy.Creatures.Ompie.Brown.Hair
					type CharacterTypeMaterialSlot
					name = Chest
					materials =
					[
						Chest
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.Brown.Hair.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .13
									hueTo = .26
									saturationFrom = .25
									saturationTo = .45
									valueFrom = .04
									valueTo = .8
								}
							]
						}
					]
				}
				{
					id Fantasy.Creatures.Ompie.Brown.eyes
					type CharacterTypeMaterialSlot
					name = Eyes
					materials =
					[
						Eyes
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.Brown.eyes.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .2
									hueTo = .25
									saturationFrom = .65
									saturationTo = 1
									valueFrom = .65
									valueTo = 1
								}
							]
						}
					]
				}
				
				
				
				
			]
			
		}
		{
			id Fantasy.Creatures.Ompie.Brown2
			type CharacterTypeVariant
			
			name = Ompie
			
			names =
			[
				Ompie
			]
			
			
			materialSlots =
			[
				{
					id Fantasy.Creatures.Ompie.Brown2.Skin
					type CharacterTypeMaterialSlot
					name = Skin
					materials =
					[
						Skin
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.Brown2.Skin.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .08
									hueTo = .1
									saturationFrom = .5
									saturationTo = .8
									valueFrom = .1
									valueTo = .45
								}
							]
						}
					]
				}
				{
					id Fantasy.Creatures.Ompie.Brown2.Hair
					type CharacterTypeMaterialSlot
					name = Chest
					materials =
					[
						Chest
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.Brown2.Hair.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .112
									hueTo = .112
									saturationFrom = .5
									saturationTo = .8
									valueFrom = .02
									valueTo = .035
								}
							]
						}
					]
				}
				{
					id Fantasy.Creatures.Ompie.Brown2.eyes
					type CharacterTypeMaterialSlot
					name = Eyes
					materials =
					[
						Eyes
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.Brown2.eyes.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .2
									hueTo = .25
									saturationFrom = .65
									saturationTo = 1
									valueFrom = .65
									valueTo = 1
								}
							]
						}
					]
				}
				
			]
			
		}
		{
			id Fantasy.Creatures.Ompie.White
			type CharacterTypeVariant
			
			name = Ompie
			
			names =
			[
				Ompie
			]
			
			materialSlots =
			[
				{
					id Fantasy.Creatures.Ompie.White.Skin
					type CharacterTypeMaterialSlot
					name = Skin
					materials =
					[
						Skin
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.White.Skin.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .7
									hueTo = .8
									saturationFrom = .4
									saturationTo = .6
									valueFrom = .1
									valueTo = .45
								}
							]
						}
					]
				}
				{
					id Fantasy.Creatures.Ompie.White.Hair
					type CharacterTypeMaterialSlot
					name = Chest
					materials =
					[
						Chest
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.White.Hair.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .7
									hueTo = .8
									saturationFrom = .4
									saturationTo = .6
									valueFrom = .045
									valueTo = .06
								}
							]
						}
					]
				}
				{
					id Fantasy.Creatures.Ompie.White.eyes
					type CharacterTypeMaterialSlot
					name = Eyes
					materials =
					[
						Eyes
					]
					definitions =
					[
						{
							id Fantasy.Creatures.Ompie.White.eyes.Color
							type ColorCharacterTypeMaterialDefinition
							colors =
							[
								{
									type CharacterTypeMaterialColor
									hueFrom = .1
									hueTo = .1
									saturationFrom = .8
									saturationTo = 1
									valueFrom = .65
									valueTo = 1
								}
							]
						}
					]
				}
				
			]
		}
	]
	
	stats =
	[
		{
			id Fantasy.Creatures.Ompie.Stats.MoveSpeed
			type StatType
			name = MoveSpeed
			min = 0
			max = 1000000
			initial = .4
			hidden = true
		}
		{
			id Fantasy.Creatures.Ompie.Stats.Health
			type StatType
			name = Health
			min = 0
			max = 35
			initial = 35
		}
		-(name == FoodPerDay)
		{
			id Fantasy.Creatures.Ompie.FoodPerDay
			type StatType
			inherit Oct.Characters.Stats.FoodPerDay
			initial = -10
		}
		-(name == HealthPerMinute)
		{
			id Fantasy.Creatures.Ompie.HealthPerMinute
			type StatType
			inherit Oct.Characters.Stats.HealthPerMinute
			initial = .1
		}
	]
	
	audio =
	[
		{
			type AudioBankHandle
			name = Death
			handle =
			{
				type AudioClipHandle
				path = /Assets/SFX/Ompie/OmpieDeath.wav
			}
		}
		{
			type AudioBankHandle
			name = Armature|React
			handle =
			{
				type AudioHandles
				audioChances =
				[
					{
						type AudioHandleChance
						chance = 1
						audio =
						{
							type AudioClipHandle
							path = /Assets/SFX/Ompie/OmpieHurt.wav
							globalCooldown = 3
						}
					}
				]
			}
		}
		{
			type AudioBankHandle
			name = Aware
			handle =
			{
				type AudioHandles
				audioChances =
				[
					{
						type AudioHandleChance
						chance = 1
						audio =
						{
							type AudioClipHandle
							path = /Assets/SFX/Ompie/OmpieReact.wav
							globalCooldown = 1
						}
					}
					{
						type AudioHandleChance
						chance = 1
						audio =
						{
							type AudioClipHandle
							path = /Assets/SFX/Ompie/OmpieReact2.wav
							globalCooldown = 1
						}
					}
				]
			}
		}
	]
	
	
	animations =
	[
		{
			type AnimationTrack
			name = Armature|Eat
			loop = true
			notifies =
			[
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioClipHandle
						path = /Assets/SFX/Ompie/OmpieEat.wav
						globalCooldown = 1
						volume = 1
					}
				}
			]
		}
		?(name == Armature|Attack) {
			type AnimationTrack
			name = Armature|Attack
			notifies =
			[
				
				?(message == Damage) {
					type VerbNotify
					time = 1.17
				}
				
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit2.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
		{
			type AnimationTrack
			name = Armature|RangedAttack
			
			notifies =
			[
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit2.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
		{
			type AnimationTrack
			name = Armature|Attack2
			notifies =
			[
				?(message == Damage) {
					type VerbNotify
					time = 1
				}
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit2.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
		{
			type AnimationTrack
			name = Armature|Attack3
			notifies =
			[
				?(message == Damage) {
					type VerbNotify
					time = 1.08
				}
				{
					type AudioNotify
					time = 0
					audio =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
							{
								type AudioHandleChance
								chance = .5
								audio =
								{
									type AudioClipHandle
									path = /Assets/SFX/Ompie/OmpieHit2.wav
									globalCooldown = 1
									volume = 0.5
								}
							}
						]
					}
				}
			]
		}
	]
	
	instincts =
	[
		{
			type HuntInstinct
		}
		{
			type RelaxInstinct
			timeFrom = 1
			timeTo = 10
			distFrom = 1
			distTo = 5
		}
	]
}