{
	id Fantasy.Characters.Revenant.Variants.Shared
	type CharacterTypeVariant
	abstract
	
	names =
	[
		Amaris
		Bless
		Blue
		Boneapart
		Boneharb
		Boneharvest
		Bonehead
		Bonejangles
		Bones
		Caelum
		Casper
		Celestriax
		Charles
		Charlie
		Corbin
		Crack
		Curse
		Decay
		Demon
		Dinklebottom
		Doofus
		Eclipse
		Edward
		Eternal
		Fragile
		Ghost
		Healthy
		Hex
		Hollow
		Jangle
		Kael
		Knight
		Lichy
		McRibbs
		Narumi
		Nova
		Osteonox
		Phantasm
		Poison
		Pox
		Rotten
		Ruin
		Sleepy
		Sol
		Solitaire
		Sombre
		Spider
		Sunburn
		White
		Wren
	]
	
	lexicon = <Oct.Humanoid.Lexicon>
	
	cosmeticSlots =
	[
		{
			type CharacterCosmeticSlot
			
			name = Body
			cosmetics =
			[
				{
					id Fantasy.Characters.RevenantBody
					type SkinnedCharacterCosmetic
					
					name = Body
					babies = false
					prefab =
					{
						type Prefab
						path = /Assets/Characters/Revenant/Body.fbx
						name = Body
						physics = false
						autoGradient = false
						behavior = SkinnedCharacterCosmeticBehavior
						scale = 1
						
						material =
						{
							type Material
							_AOGradientOffset = 0
							_AOGradientHeight = .5
							_AOGradientExponent = .75
							_SkinDetail = 1
						}
					}
					
					instanceColors =
					[
						<Fantasy.Characters.Revenant.Colors.Skin>
					]
				}
				<Oct.Characters.Humanoids.Wrap.Baby>
				<Oct.Characters.Humanoids.Wrap.Baby.Variant>
			]
		}
		{
			type CharacterCosmeticSlot
			
			name = Head
			
			cosmetics =
			[
				<Fantasy.Characters.Revenant.Head>
				<Fantasy.Characters.Revenant.Head2>
				<Fantasy.Characters.Revenant.Head3>
				<Fantasy.Characters.Revenant.Head4>
			]
		}
		{
			type CharacterCosmeticSlot
			
			name = Eyes
			
			cosmetics =
			[
				<Fantasy.Characters.Revenant.Empty>
				<Fantasy.Characters.Revenant.Eye01>
				<Fantasy.Characters.Revenant.Eye02>
				<Fantasy.Characters.Revenant.Eye03>
				<Fantasy.Characters.Revenant.Eye04>
				<Fantasy.Characters.Revenant.Eye05>
				<Fantasy.Characters.Revenant.Eye06>
				<Fantasy.Characters.Revenant.Eye07>
				<Fantasy.Characters.Revenant.Eye08>
				<Fantasy.Characters.Revenant.Eye09>
				<Fantasy.Characters.Revenant.Eye10>
				<Fantasy.Characters.Revenant.Eye11>
			]
		}
		{
			type CharacterCosmeticSlot
			
			name = Mouth
			
			cosmetics =
			[
				<Fantasy.Characters.Revenant.Mouth01>
				<Fantasy.Characters.Revenant.Mouth02>
				<Fantasy.Characters.Revenant.Mouth03>
				<Fantasy.Characters.Revenant.Mouth04>
				<Fantasy.Characters.Revenant.Mouth05>
				<Fantasy.Characters.Revenant.Mouth06>
				<Fantasy.Characters.Revenant.Mouth07>
				<Fantasy.Characters.Revenant.Mouth08>
				<Fantasy.Characters.Revenant.Mouth09>
				<Fantasy.Characters.Revenant.Mouth10>
				<Fantasy.Characters.Revenant.Mouth11>
				<Fantasy.Characters.Revenant.Mouth12>
				<Fantasy.Characters.Revenant.Mouth13>
			]
		}
		{
			type CharacterCosmeticSlot
			
			name = Hair
			cosmetics =
			[
				<Fantasy.Characters.Revenant.Hair.None>
				<Fantasy.Characters.Revenant.Hair01>
				<Fantasy.Characters.Revenant.Hair02>
				<Fantasy.Characters.Revenant.Hair03>
				<Fantasy.Characters.Revenant.Hair04>
				<Fantasy.Characters.Revenant.Hair05>
				
				<Fantasy.Characters.Revenant.Hair08>
				<Fantasy.Characters.Revenant.Hair09>
				<Fantasy.Characters.Revenant.Hair10>
			]
		}
	]
	
	audio =
	[
		{
			type AudioBankHandle
			name = Grunt
			handle =
			{
				type AudioHandles
			}
		}
		{
			type AudioBankHandle
			name = Hurt
			handle =
			{
				type AudioHandles
			}
		}
		{
			type AudioBankHandle
			name = Death
			handle =
			{
				type AudioHandles
			}
		}
		{
			type AudioBankHandle
			name = Cheer
			handle =
			{
				type AudioHandles
			}
		}
	]
}
{
	id Fantasy.Characters.Revenant
	type CharacterType
	inherit Oct.Characters.Humanoids.Humanoid
	
	name = Revenant
	plural = Revenants
	corpseType = <Fantasy.Items.Corpse.Revenant>
	
	comfortRange = 10
	survivalRange = 15
	bleeds = false
	blood = null
	bloodFountain = null
	
	skinLinearColor = true
	skinColorFrom = color(#bdbdbd)
	skinColorTo = color(#424242)
	
	vertColorPowerFrom = 0.25
	vertColorPowerTo = .75
	
	hideName = Revenant Skin
	leatherName = Revenant Leather
	hideColor = color(#606060)
	
	attraction =
	{
		id Fantasy.Characters.Revenant.Attraction
		type CharacterTypePawnAttractionType
		importance = 2
		type = <Fantasy.Characters.Revenant>
	}
	
	prefab =
	{
		type Prefab
		path = /Art/characters/Human/Human.fbx
		name = Revenant
		behavior = CharacterBehavior
		
		animations =
		[
			{
				type Prefab
				path = /Art/characters/Humanoid/Animations/HumanoidAnims.fbx
			}
			{
				type Prefab
				path = /Art/characters/Humanoid/Animations/HumanoidAnims_Blender2_93.fbx
			}
		]
	}
	
	hairColors =
	[
		// warm colors
		{
			type CharacterTypeMaterialColor
			hueFrom = 0
			hueTo = 0.35
			hueExponent = 1.5
			saturationFrom = 0.45
			saturationTo = 1.1
			saturationExponent = 1.4
			valueFrom = 0.4
			valueTo = 1.2
			valueExponent = 1.5
			chance = 0.5
		}
		// heavy dark and bright colors
		{
			type CharacterTypeMaterialColor
			hueFrom = 0
			hueTo = 0.35
			hueExponent = 1.5
			saturationFrom = -0.2
			saturationTo = 1.1
			saturationExponent = 0.5
			valueFrom = 0.1
			valueTo = 0.95
			valueExponent = 0.1
			chance = 0.33
		}
		// all colors
		{
			type CharacterTypeMaterialColor
			hueFrom = 0
			hueTo = 1
			hueExponent = 1
			saturationFrom = 0.2
			saturationTo = 1.0
			saturationExponent = 1.4
			valueFrom = 0
			valueTo = 1.2
			valueExponent = 1.2
			chance = 0.5
		}
	]
	
	boneScales =
	[
		{
			type CharacterTypeBoneScale
			boneName = Head
			range = vector(.125, .125, .125)
			center = vector(1.1375, 1.0625, 1.0625)
		}
	]
	
	familyNames =
	{
		id Fantasy.Characters.Revenant.Family.Names
		type PickGenerator
		generators =
		[
			{
				type CombineGenerator
				space = false
				left =
				{
					id Fantasy.Characters.Revenant.Family.Names.Prefix
					type PickStringGenerator
					strings =
					[
						Ash
						Blight
						Bone
						Carrion
						Chill
						Corpse
						Crypt
						Doom
						Dread
						Dust
						Fell
						Ghoul
						Gloom
						Grav
						Grim
						Haunt
						Husk
						Lich
						Mist
						Mort
						Mourn
						Necro
						Pale
						Phantom
						Rot
						Ruin
						Shade
						Shadow
						Shroud
						Skul
						Sorrow
						Spectral
						Tomb
						Wither
						Wraith
					]
				}
				right =
				{
					id Fantasy.Characters.Revenant.Family.Names.Suffix
					type PickStringGenerator
					strings =
					[
						bane
						blight
						born
						breath
						claw
						dusk
						fall
						fang
						frost
						gaze
						geist
						gloom
						grave
						howl
						mark
						maw
						mist
						mor
						plague
						rattle
						reap
						rise
						shade
						shard
						shriek
						sinew
						soul
						spire
						thorn
						veil
						walker
						whisper
						wight
						wrack
						wyrm
					]
				}
			}
		]
	}
	
	variants =
	[
		{
			id Fantasy.Characters.Revenant.Variants.Male
			type CharacterTypeVariant
			inherit Fantasy.Characters.Revenant.Variants.Shared
			
			weight = 1
			name = Male
			gender = Male
			names =
			[
				Achey
				Acidux
				Aether
				Ainz Ooal Gown
				Aridius
				Ashen
				Blip
				Boneblade
				Bonequill
				Breaky
				Brock
				Calypso
				Cheeky
				Chompa
				Creaky
				Creepa
				Crumbles
				Cryptus
				Damian
				Diablo
				Doom
				Drake
				Drakolith
				Drakon
				Drakonix
				Draven
				Dread
				Dreadshade
				Drools
				Foggy
				Fredrick
				Frog
				Gerald
				Ghost
				Gravemourn
				Gregory
				Grim
				Grimmace
				Grimscar
				Grimthorn
				Grumble
				Henry
				Hunter
				IamStillAlive
				Indigo
				ItsCold
				Jack Marrow
				Javier
				Jawz
				Jester
				Junior
				Keiran
				Lazarus
				Lich
				Lucifer
				Lucius
				Ludacrai
				Luthor
				Marrowgrim
				Marshall
				Mist
				Morbius
				Mordacai
				Morghast
				Morrigan
				Mortalis
				Mortis
				Mr House
				Murk
				Muscle
				Necron
				Nihilus
				Nocture
				Nocturnai
				Noxious
				Oni
				Peter
				Punk
				Rathokar
				Rattle
				Requiem
				Rotten
				Rupert
				Ruthark
				Ruthius
				Sable
				Scraps
				Shade
				Shade
				Shadow
				Shivers
				Skelethor
				Skeletor
				Skellington
				Skelvis
				Skulldor
				Skullrend
				Skullvester
				Skully
				Snap
				Sneako
				Soulreaper
				Soulweaver
				Spinestein
				Spook
				Sunscreen
				Thanarok
				Thornsoul
				Thoruk
				Vulk
				Vulkan
				Vultan
				Whisper
				Wobblebone
				XRay
				Xaxier
			]
			
			voices =
			[
				HumanMaleVoice1
			]
			
			audio =
			[
				?(name == Grunt) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Grunt/Grunt1.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Grunt/Grunt2.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Grunt/Grunt3.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Grunt/Grunt4.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Grunt/Grunt5.wav
									globalCooldown = 3
								}
							}
						]
					}
				}
				?(name == Hurt) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Hurt/Hurt1.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Hurt/Hurt2.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Hurt/Hurt3.wav
									globalCooldown = 3
								}
							}
						]
					}
				}
				?(name == Death) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Death/Death1.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Death/Death2.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Death/Death3.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Death/Death4.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Death/Death5.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Death/Death6.wav
									globalCooldown = 3
								}
							}
						]
					}
				}
				?(name == Cheer) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_1_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_1_normal.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_1_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_2_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_2_normal.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_2_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_3_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_3_normal.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Woo_3_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_1_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_1_normal.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_1_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_2_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_2_normal.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_2_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_3_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_3_normal.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_3_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_4_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_4_normal.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Male/Cheer/Yeah_4_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 19
								audio = null
							}
						]
					}
				}
			]
		}
		{
			id Fantasy.Characters.Revenant.Variants.Female
			type CharacterTypeVariant
			inherit Fantasy.Characters.Revenant.Variants.Shared
			
			weight = 1
			name = Female
			gender = Female
			
			names =
			[
				Ambrose
				Anete
				Aria
				Ariandra
				Ash
				Ashelly
				Ashen
				Ashina
				Astra
				Astridra
				Azura
				Bellatrix
				Bloody mary
				Bonelly
				Bonnelopy
				Bonnie
				Bonnie
				Brienne
				Calystria
				Calystria
				Cassandra
				Cofinnete
				Cryptessa
				Desdemona
				Dooklebottom
				Echograce
				Elara
				Elizabeth
				Eloise
				Emberlyn
				Ephemera
				Hairlessine
				Helga
				Hellger
				Jawlene
				Katherine
				Lazarus
				Lazura
				Lexura
				Lichara
				Lyra
				Lyricantha
				Marrowene
				Marvine
				Mary
				Morrigan
				Nazura
				Necrolina
				Neferatu
				Nocturella
				Nocturla
				Nocturna
				Noxioula
				Nyx
				Ophelia
				Penolopy
				Phantaselle
				Phantomyst
				Poppy
				Ribsy
				Rose
				Seren
				Skelenopy
				Skellie
				Skulette
				Skullary
				Skullene
				Undia
				Valia
				Vespera
				Vex
				Vexene
				Victoria
				Vixen
				Wraitha
			]
			
			voices =
			[
				HumanFemaleVoice1
			]
			
			cosmeticSlots =
			[
				?(name == Hair) {
					type CharacterCosmeticSlot
					
					cosmetics =
					[
						<Fantasy.Characters.Revenant.Hair06>
						<Fantasy.Characters.Revenant.Hair07>
					]
				}
			]
			
			audio =
			[
				?(name == Grunt) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Grunt/Grunt1.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Grunt/Grunt2.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Grunt/Grunt3.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Grunt/Grunt4.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Grunt/Grunt5.wav
									globalCooldown = 3
								}
							}
						]
					}
				}
				?(name == Hurt) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Hurt/Hurt_1.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Hurt/Hurt_2.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Hurt/Hurt_3.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Hurt/Hurt_4.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Hurt/Hurt_5.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Hurt/Hurt_6.wav
									globalCooldown = 5
								}
							}
						]
					}
				}
				?(name == Death) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Death/Death1.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Death/Death2.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Death/Death3.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Death/Death4.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Death/Death5.wav
									globalCooldown = 3
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Death/Death6.wav
									globalCooldown = 3
								}
							}
						]
					}
				}
				?(name == Cheer) {
					type AudioBankHandle
					handle =
					{
						type AudioHandles
						audioChances =
						[
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_1_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_1.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_1_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_2_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_2.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_2_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_3_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_3.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Yeah_3_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_1_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_1.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_1_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_2_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_2.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_2_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_3_low.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_3.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 1
								audio =
								{
									type AudioClipHandle
									path = /SFX/Human/Female/Cheer/Woo_3_high.wav
									globalCooldown = 5
								}
							}
							{
								type AudioHandleChance
								chance = 19
								audio = null
							}
						]
					}
				}
			]
		}
		{
			id Fantasy.Characters.Revenant.Variants.Nonbinary
			type CharacterTypeVariant
			inherit Fantasy.Characters.Revenant.Variants.Male
			inherit Fantasy.Characters.Revenant.Variants.Female
			
			weight = .25
			name = Nonbinary
			gender = Nonbinary
		}
	]
	
	stats =
	[
		-(name == Mobility)
		{
			id Fantasy.Characters.Humanoids.Stats.MoveSpeed
			type StatType
			name = Mobility
			min = 0
			max = 5
			initial = .9
		}
		-(name == AttackRate)
		{
			id Fantasy.Characters.Humanoids.Stats.AttackRate
			type StatType
			name = AttackRate
			min = 0
			max = 1000000
			initial = .9
		}
		-(name == Pain)
		{
			id Fantasy.Characters.Stats.Pain
			type StatType
			name = Pain
			min = 0
			max = 100
			initial = 0
		}
	]
	
	attributePotentialDeltas =
	[
		{
			type CharacterTypeAttributePotentialDelta
			attribute = Melee
			delta = 2
		}
		{
			type CharacterTypeAttributePotentialDelta
			attribute = Ranged
			delta = 2
		}
		{
			type CharacterTypeAttributePotentialDelta
			attribute = Farming
			delta = -3
		}
		{
			type CharacterTypeAttributePotentialDelta
			attribute = Cooking
			delta = -3
		}
		{
			type CharacterTypeAttributePotentialDelta
			attribute = Nursing
			delta = -1
		}
	]
	
	innateAbilities =
	[
		{
			id Fantasy.Characters.Revenant.Potential
			type AbilityType
			
			name = Revenant Potential
			description = +1 Melee & Ranged Potential<br>-3 Farming & Cooking Potential<br>-1 Nursing Potential
			
			icon = tex(/Assets/Icons/Revenant.png)
			modifierIcon = tex(/Art/Icons/Abilities/Innate.png)
			
			activation =
			{
				type PassiveAbilityActivationType
			}
		}
		{
			id Fantasy.Characters.Revenant.Innate
			type AbilityType
			inherit Oct.Abilities.AttributePassive
			
			name = Revenant Innate
			description = +1 Melee & Ranged<br>-10 Armor, Mobility & AttackRate<br>Does not require food, hygeine or light
			innate = true
			icon = tex(/Assets/Icons/Revenant2.png)
			modifierIcon = tex(/Art/Icons/Abilities/Innate.png)
			
			attributes =
			[
				Melee
				Ranged
				Armor
			]
			
			actions =
			[
				{
					type StatModifierAbilityActionType
					stat = Melee
					delta = 1
				}
				{
					type StatModifierAbilityActionType
					stat = Ranged
					delta = 1
				}
				{
					type StatModifierAbilityActionType
					stat = Armor
					delta = -10
				}
			]
		}
	]
	
	needs =
	[
		-(name == Food)
		{
			id Fantasy.Characters.Needs.Food
			type CharacterStatNeedType
			inherit Oct.Characters.Needs.Food
			unmetOpinion = 0
			decreasePerHour = 0
			subceededOpinion = 0
			survival = false
		}
		-(name == Light)
		{
			id Fantasy.Characters.Needs.Light
			type CharacterStatNeedType
			inherit Oct.Characters.Needs.Light
			unmetOpinion = 0
			decreasePerHour = 0
			subceededOpinion = 0
		}
		-(name == Hygiene)
		{
			id Fantasy.Characters.Needs.Hygiene
			type CharacterStatNeedType
			inherit Oct.Characters.Needs.Hygiene
			unmetOpinion = 0
			decreasePerHour = 0
			subceededOpinion = 0
		}
	]
	
	instincts =
	[
		$clear
		{
			type RestInstinct
		}
		{
			type DownedInstinct
		}
		{
			type WorkInstinct
			babies = false
		}
		{
			type RelaxInstinct
			babies = false
		}
		{
			type LightInstinct
			babies = false
		}
		{
			type EmoteInstinct
			babies = false
		}
		{
			type SocializeInstinct
			babies = false
		}
		{
			type FightInstinct
			babies = false
		}
		{
			type FleeInstinct
			babies = false
		}
		{
			type PanicInstinct
			babies = false
		}
	]
	
	opinions =
	[
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Fantasy.Characters.Felindae>
			center = -.25
			range = 1.25
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Fantasy.Characters.Ratfolk>
			center = -.25
			range = 1.25
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Fantasy.Characters.Revenant>
			center = .5
			range = 1
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Characters.Humanoids.Human>
			center = -.5
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Characters.Humanoids.Dwarf>
			center = -.5
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Characters.Humanoids.Elf>
			center = -.3
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Characters.Humanoids.Orc>
			center = .2
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.RawMeat.Boar>
			center = .1
			range = 1.125
			exp = 3
			minSimilarity = -.875
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.RawMeat.Cow>
			center = .1
			range = 1.125
			exp = 3
			minSimilarity = -.875
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.RawMeat.Human>
			center = -.75
			range = 1.75
			exp = 5
			minSimilarity = -.25
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.RawMeat.Orc>
			center = -.5
			range = 1.5
			exp = 4
			minSimilarity = -.5
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.RawMeat.Dwarf>
			center = -.5
			range = 1.5
			exp = 4
			minSimilarity = -.5
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.RawMeat.Elf>
			center = -.5
			range = 1.5
			exp = 4
			minSimilarity = -.5
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.RawMeat.Imp>
			center = -.25
			range = 1.25
			exp = 3
			minSimilarity = -.75
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.CookedMeat.Boar>
			center = .25
			range = 1.25
			exp = 2
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.CookedMeat.Cow>
			center = .25
			range = 1.25
			exp = 2
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.CookedMeat.Orc>
			center = -.25
			range = 1.25
			exp = 3
			minSimilarity = -.75
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.CookedMeat.Dwarf>
			center = -.25
			range = 1.25
			exp = 3
			minSimilarity = -.75
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.CookedMeat.Elf>
			center = -.25
			range = 1.25
			exp = 3
			minSimilarity = -.75
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.CookedMeat.Human>
			center = -.5
			range = 1.5
			exp = 4
			minSimilarity = -.5
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.CookedMeat.Imp>
			center = 0
			range = 1
			exp = 2
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Octoberberries>
			center = .125
			range = 1.125
			exp = 3
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Moonshine.Corn>
			center = .0625
			range = 1.0625
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Moonshine.Pumpkin>
			center = .0625
			range = 1.0625
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Moonshine.Octo>
			center = .0625
			range = 1.0625
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Moonshine.MealBean>
			center = .0625
			range = 1.0625
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Moonshine.Straw>
			center = .0625
			range = 1.0625
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Moonshine.Wood>
			center = .0625
			range = 1.0625
			exp = 4
		}
		{
			type ExplicitCharacterTypeOpinionRange
			subject = <Oct.Items.Stackable.Wine>
			center = .0625
			range = 1.0625
			exp = 4
		}
		{
			type CorpseProductCharacterTypeOpinionRange
			characterType = <Oct.Characters.Animals.Boar>
			center = .25
			range = 1.25
			exp = 2
		}
		{
			type CorpseProductCharacterTypeOpinionRange
			characterType = <Oct.Characters.Animals.Cow>
			center = .25
			range = 1.25
			exp = 2
		}
		{
			type CorpseProductCharacterTypeOpinionRange
			characterType = <Oct.Characters.Monsters.LesserImp>
			center = .25
			range = 1.25
			exp = 3
			minSimilarity = -.75
		}
	]
	
	animations =
	[
		{
			type AnimationTrack
			name = Armature|React
			notifies =
			[
				{
					type AudioHandleNotify
					time = 0
					name = Hurt
				}
			]
		}
	]
}