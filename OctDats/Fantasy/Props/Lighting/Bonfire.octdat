{
	id Fantasy.Props.Bonfire
	type FirePitPropType
	inherit Oct.Props.Workbench
	inherit Oct.Props.Audio.Stone
	
	actorType = FirePitPropActor
	name = Bonfire
	pluralName = Bonfires
	toolSort = 0
	
	category = <Fantasy.Categories.Props.Lighting>
	researchEntry = <Fantasy.Research.Bonfire>
	
	buildItemFilter = <Oct.Items.Stackable.Wood>
	buildItemCount = 22
	repairItemFilter = <Oct.Items.Stackable.Wood>
	
	maxHealth = 60
	progressMultiplier = .333
	difficulty = .5
	
	minValue = 3
	maxValue = 9
	
	interactionPoint = vector(0, 0, .51)
	
	facets =
	[
		{
			id Fantasy.Props.Bonfire.Storage
			type StoragePropFacetType
			facetClass = StoragePropFacet
			slots =
			[
				vector(0, .4, 0)
			]
		}
		{
			id Fantasy.Props.Bonfire.FuelBurner
			type FuelBurnerPropFacetType
			
			facetClass = FuelBurnerPropFacet
			fuelType = <Oct.Items.Stackable.Wood>
			
			fuelPerHour = 20
			flameTransform = Flame
			scaleStrength = .5
			rotationStrength = .25
			lightIntensity = 25
			
			temperatureDelta = 18
			temperatureRange = 4
			forTemperature = true
			exhausts = true
			smokeEffect = <Oct.Props.FuelBurner.Smoke>
			
			fuelMaterials =
			[
				Wood2
				Wood3
			]
			
			flameMaterials =
			[
				Flame
			]
		}
	]
	
	ignoreDestruction =
	[
		Wood
		Wood2
		Wood3
	]
	
	buildPos =
	[
		vector(0, 0, .51)
		vector(0, 0, -.51)
		vector(.51, 0, 0)
		vector(-.51, 0, 0)
	]
	
	prefabs =
	[
		{
			type Prefab
			path = /Assets/Props/Lighting/Bonfire.fbx
			name = Bonfire
			behavior = WorkbenchPropBehavior
			
			sharedMaterials =
			[
				{
					type OctDatSharedMaterial
					names =
					[
						Wood
						Wood2
						Wood3
					]
					material = <Oct.Props.Materials.Wood>
				}
				{
					type OctDatSharedMaterial
					names =
					[
						Ash
					]
					material = <Fantasy.Materials.Ash>
				}
			]
		}
	]
	
	lights =
	[
		{
			type PropLight
			caster = Flame
			unityPrefab = FirePitLight
		}
	]
	
	colorSlots =
	[
		
		{
			type PropColorSlot
			name = Wood
			materials =
			[
				Wood
			]
			palette = <Oct.Props.Palettes.Wood>
		}
		{
			type PropColorSlot
			name = Wood 2
			materials =
			[
				Wood2
			]
			palette = <Oct.Props.Palettes.Wood>
		}
		{
			type PropColorSlot
			name = Wood 3
			materials =
			[
				Wood3
			]
			palette = <Oct.Props.Palettes.Wood>
		}
	]
}