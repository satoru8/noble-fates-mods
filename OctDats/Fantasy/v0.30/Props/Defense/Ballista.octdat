{
	id Fantasy.Props.Ballista
	type CatapultPropType
	
	name = Carroballista
	pluralName = Carroballistas
	toolName = Carroballista
	description = A formidable Carroballista. It stands as a powerful guardian, ready to unleash destruction upon any approaching threat.
	iconSort = 0
	
	category = <Fantasy.Categories.Props.Defense>
	researchEntry = <Fantasy.Research.Defense.Ballista>
	
	buildItemFilter = <Fantasy.Items.LukanPlank>
	buildItemCount = 42
	
	maxHealth = 200
	progressMultiplier = .00625
	difficulty = 1
	skillCurve = .75
	
	minValue = 15
	maxValue = 50
	
	clipScale = 3
	clipOffset = -2
	clipSpace = vector(0, 0, 1)
	clipSpaceLocal = true
	
	actorType = CatapultPropActor
	selectionMode = Tile
	buildingMode = Props
	rotation = choose
	
	combatBlueprint = true
	crateWeight = 8
	
	facets =
	[
		{
			id .Launcher
			type ProjectileLauncherPropFacetType
			
			facetClass = ProjectileLauncherPropFacet
			
			speed = 10
			rangeRadius = 37.5
			aimRadius = 5
			damage = 60
			fireProgress = .1
			
			projectile =
			{
				id Fantasy.Props.Ballista.Projectile
				type PropProjectileType
				
				name = Arrow
				
				hitItems = true
				throughUntilDecay = true
				radius = .2
				trailLength = 1
				
				itemDamageScale = 5
				foliageDamageScale = 10
				terrainDamageScale = .0625
				
				hitCameraShake = .3
				hitCameraShakeRadius = 3
				
				blockable = false
				major = true
				
				hit =
				{
					type AudioClipHandle
					path = /SFX/Catapult/catapultImpactSound.wav
					globalCooldown = 0.25
				}
				
				aoes =
				[
					{
						type ProjectileTypeAOE
						range = .5
						damage = 60
					}
					{
						type ProjectileTypeAOE
						range = 1.5
						damage = 35
					}
					{
						type ProjectileTypeAOE
						range = 2.5
						damage = 25
					}
				]
				
				prefab =
				{
					type Prefab
					path = \Assets\Siege\Ballista_Arrow.fbx
					name = Ballista_Arrow
					behavior = ProjectileBehavior
					
					sharedMaterials =
					[
						{
							type OctDatSharedMaterial
							names =
							[
								Frames
							]
							material = <Oct.Props.Materials.Wood>
						}
					]
				}
			}
		}
	]
	
	buildPos =
	[
		vector(-1, 0, 1)
		vector(0, 0, 1)
		vector(1, 0, 1)
		vector(-1, 0, -1)
		vector(0, 0, -1)
		vector(1, 0, -1)
	]
	
	prefabs =
	[
		{
			type Prefab
			path = \Assets\Siege\Ballista.fbx
			name = Ballista
			behavior = CatapultPropBehavior
			physicsPath = \Assets\Siege\BallistaMesh.fbx
			
			
			sharedMaterials =
			[
				{
					type OctDatSharedMaterial
					names =
					[
						Wood
					]
					material = <Oct.Props.Materials.Wood>
				}
				{
					type OctDatSharedMaterial
					names =
					[
						Frames
					]
					material = <Oct.Props.Materials.Iron>
				}
				{
					type OctDatSharedMaterial
					names =
					[
						Rope
					]
					material = <Oct.Props.Materials.Leather>
				}
			]
		}
	]
	
	colorSlots =
	[
		{
			type PropColorSlot
			name = Frames
			materials =
			[
				Frames
			]
			palette = <Oct.Props.Palettes.Iron>
		}
		{
			type PropColorSlot
			name = Wood
			materials =
			[
				Wood
			]
			palette = <Oct.Props.Palettes.WoodFurniture>
		}
		{
			type PropColorSlot
			name = Rope
			materials =
			[
				Rope
			]
			palette = <Oct.Props.Palettes.Leather>
		}
	]
	
	idleAnimation = Armature|CatapultIdle
	
	animations =
	[
		{
			type AnimationTrack
			name = Armature|CatapultFire
			blendTime = 0
			
			notifies =
			[
				{
					type ActorNotify
					time = .75
					message = Fire
				}
			]
		}
		{
			type AnimationTrack
			name = Armature|CatapultIdle
			blendTime = 0.25
		}
	]
}