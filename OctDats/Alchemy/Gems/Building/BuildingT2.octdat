{
    id Alchemy.Items.Gems.Building.T2
    type InstancedItemType
	inherit Alchemy.Items.Gems.Base
	alias Gem.Items.BuildingGem.T2

    name = Building Gem II
    pluralName = Building Gems II
    category = <Gem.Items.Categories.Gems.Building>
    equipmentSlot = GemOne

	stats =
	[
		{
			type InstancedItemStat
			name = Value
			formula = QualityHalfDurability
			min = 40
			max = 60
		}
	]

	staticBuffs = 
    [
		{
			type StatModifierInstancedItemStaticBuff
			statName = Building
			modifier = 2
        }
    ]
    
	prefab = 
	{
		type Prefab
		path = /Assets/Gems/T2/BlueT2.fbx
        name = Building Gem
        behavior = InstancedItemBehavior
		scale = 2
        translation = vector(0, .03, .0)
		sharedMaterials = 
		[
			<Alchemy.Materials.Gems.Saphire>
		]
	}

    salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Alchemy.Items.Gems.Building.T1>
			count = 1
		}
	]
}
{
	id Gem.Recipes.Gems.BuildingGem.T2
	type ItemRecipe
	
	name = Building II
	type = <Alchemy.Items.Gems.Building.T2>
	commandType = Alchemy.TransmuteItemCommand
	progressMultiplier = .65
	researchEntry = <Gem.Research.Gems.BuildingGem.T2>
	craftingLoop = ClothCraftingLoop
	qualityDifficulty = 1
    skillCurve = .6
    
    craftAt = 
    [
        <Alchemy.GemTable>
    ]
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Alchemy.Items.Gems.Building.T1>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Alchemy.Items.Crystal.Dust.Blue>
			count = 20
		}
		{
			type ItemRecipeIngredient
			filter = <Gem.Items.RawBlueCrystal>
			count = 60
		}
	]
}
{
	id Gem.Research.Gems.BuildingGem.T2
	type ResearchEntry
    inherit Alchemy.Research
	
	name = Building Gem II
	prerequisite = <Gem.Research.Gems.BuildingGem.T1>
    tooltipProvider = <Gem.Recipes.Gems.BuildingGem.T2>
	tierOffset = 1
}