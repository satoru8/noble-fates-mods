{
	id Alchemy.AlchemyTable
	type WorkbenchPropType
	inherit Oct.Props.Workbench
	inherit Oct.Props.Audio.Wooden
	
	actorType = CraftingTablePropActor
	name = Alchemy Table
    pluralName = Alchemy Tables
    toolName = Alchemy Table
	toolSort = -99
	
    buildItemFilter = <Oct.Items.Stackable.Wood>
	buildItemCount = 24
	repairItemFilter = <Oct.Items.Stackable.Wood>
    interactionPoint = vector(0, 0, .51)

    maxHealth = 150
    progressMultiplier = .675
    difficulty = .475
	
	researchEntry = 
    {
	    id Alchemy.Research.AlchemyTable
	    type ResearchEntry
        inherit Alchemy.Research
	
	    name = Alchemy Table
        tierOffset = 2
	    prerequisite = <Alchemy.Research.Workbench>
        tooltipProvider = <Alchemy.AlchemyTable>
    }
	
    facets = 
    [
        {
            id Alchemy.AlchemyTable.Storage
            type StoragePropFacetType
            facetClass = StoragePropFacet
            slots = 
            [
                vector(0, .325, .1625)
            ]
        }
    ]
    
	prefabs = 
	[
		{
			type Prefab
			path = /Assets/Table/AlchemyTable.fbx
			name = Alchemy Table
            behavior = WorkbenchPropBehavior
            collapseMeshes = false
            collapseMaterials = false

            sharedMaterials = 
            [
                {
                    type OctDatSharedMaterial
                    names = 
                    [
                        Counter
                        Legs
                        Cork
                    ]
                    material = <Oct.Props.Materials.Wood>
                }
                {
                    type OctDatSharedMaterial
                    names = 
                    [
                        Cloth
                        ClothPattern
                    ]
                    material = <Oct.Props.Materials.Cloth>
                }
                {
                    type OctDatSharedMaterial
                    names = 
                    [
                        Rim
                    ]
                    material = <Oct.Props.Materials.Iron>
                }
            ]
		}
	]

	colorSlots = 
	[
		{
			type PropColorSlot
			name = Table Top
            materials =
            [
                Counter
            ]
			palette = <Oct.Props.Palettes.WoodFurniture>
		}
		{
			type PropColorSlot
			name = Table Legs
            materials =
            [
                Legs
            ]
			palette = <Oct.Props.Palettes.WoodFurniture>
		}
        {
			type PropColorSlot
			name = Cloth
            materials =
            [
                Cloth
            ]
			palette = <Oct.Props.Palettes.Cloth>
		}
        {
			type PropColorSlot
			name = Cloth Accent
            materials =
            [
                ClothPattern
            ]
			palette = <Oct.Props.Palettes.Cloth>
		}
        {
			type PropColorSlot
			name = Corks
            materials =
            [
                Cork
            ]
			palette = <Oct.Props.Palettes.Wood>
		}
        {
			type PropColorSlot
			name = Potion Holder
            materials =
            [
                Rim
            ]
			palette = <Oct.Props.Palettes.Iron>
		}
	]
}