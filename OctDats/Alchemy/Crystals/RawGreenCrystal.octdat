{
	id Gem.Items.RawGreenCrystal
	type StackedItemType
	inherit Alchemy.Items.Crystal.Base
	
	name = Emerald

    sources = 
    [
        <Gem.Terrain.Gatherables.GreenCrystalNode>
    ]

	prefab = 
	{
		type Prefab
		path = /Assets/Crystals/MiniNode.fbx
		name = Emerald
        behavior = StackedItemBehavior

		material =
		{
			type Material
			_Smoothness = 0
        	_Metallic = 0
			_AlbedoMap = tex(/Assets/Crystals/GreenNode.png, bilinear, mips)
			_AOGradientWorld = 1
		}
	}
	
	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Gem.Items.RawCrystal>
			count = 2
		}
	]
}
{
	id Gem.Recipes.Crystals.GreenCrystal
	type ItemRecipe
	inherit Alchemy.Recipes.Crystals.Base
	
	name = Emerald - 10
	type = <Gem.Items.RawGreenCrystal>
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Gem.Items.RawCrystal>
			count = 20
		}
	]

	researchEntry = 
	{
		id Gem.Research.Crystals.Green
		type ResearchEntry
    	inherit Alchemy.Research
	
		name = Emerald - 10
		prerequisite = <Gem.Research.Crystals>
		tooltipProvider = <Gem.Recipes.Crystals.GreenCrystal>
		tierOffset = 0
	}
}
{
	id Alchemy.Recipes.Crystals.Green.20
	type ItemRecipe
	inherit Alchemy.Recipes.Crystals.Base
	
	name = Emerald - 20
	type = <Gem.Items.RawGreenCrystal>
	yield = 20
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Gem.Items.RawCrystal>
			count = 20
		}
	]

	researchEntry = 
	{
		id Alchemy.Research.Crystals.Green.20
		type ResearchEntry
    	inherit Alchemy.Research
	
		name = Emerald - 20
		prerequisite = <Gem.Research.Crystals.Green>
		tooltipProvider = <Alchemy.Recipes.Crystals.Green.20>
		tierOffset = 1
	}
}
{
	id Alchemy.Recipes.Crystals.Green.40
	type ItemRecipe
	inherit Alchemy.Recipes.Crystals.Base
	
	name = Emerald - 40
	type = <Gem.Items.RawGreenCrystal>
	yield = 40
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Gem.Items.RawCrystal>
			count = 80
		}
	]

	researchEntry = 
	{
		id Alchemy.Research.Crystals.Green.40
		type ResearchEntry
    	inherit Alchemy.Research
	
		name = Emerald - 40
		prerequisite = <Alchemy.Research.Crystals.Green.20>
		tooltipProvider = <Alchemy.Recipes.Crystals.Green.40>
		tierOffset = 1
	}
}
{
	id Alchemy.Recipes.Crystals.Green.80
	type ItemRecipe
	inherit Alchemy.Recipes.Crystals.Base
	
	name = Emerald - 80
	type = <Gem.Items.RawGreenCrystal>
	yield = 80
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Gem.Items.RawCrystal>
			count = 160
		}
	]

	researchEntry = 
	{
		id Alchemy.Research.Crystals.Green.80
		type ResearchEntry
    	inherit Alchemy.Research
	
		name = Emerald - 80
		prerequisite = <Alchemy.Research.Crystals.Green.40>
		tooltipProvider = <Alchemy.Recipes.Crystals.Green.80>
		tierOffset = 1
	}
}