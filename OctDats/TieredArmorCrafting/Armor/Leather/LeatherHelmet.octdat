//////////// Tier II ////////////
{
	id Oct.Items.Armor.Skins.Helmet.Leather.II
	type InstancedItemTypeGenerator
	inherit Oct.Items.Armor.Skins.Helmet
    inherit Oct.Items.Armor.Leather
	inherit Oct.Items.Restrictions2
	
	name = Leather Helmet II
    pluralName = Leather Helmets II
	buffScale = 3
	
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
            modifier = true
			min = 1
			max = 3
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 14
			max = 98
		}
	]

	salvage =
	[
		?(count == 3) {
			type IngredientItemSalvageComponent
			count = 10
		}
	]
}
{
	id Oct.Items.Recipes.Skins.Helmet.Leather.II
	type ItemRecipe
	
	type = <Oct.Items.Armor.Skins.Helmet.Leather.II>
	researchEntry = <Oct.Research.Skins.Helmet.Leather.II>
	commandType = CraftItemCommand
	craftingLoop = ClothCraftingLoop
	canFilterIngredients = true
	progressMultiplier = .4
	qualityDifficulty = 1.275
    skillCurve = .74
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Skins.Helmet.Leather>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Skins.Leather>
			count = 10
		}
	]
}
//////////// Tier III ////////////
{
	id Oct.Items.Armor.Skins.Helmet.Leather.III
	type InstancedItemTypeGenerator
	inherit Oct.Items.Armor.Skins.Helmet
    inherit Oct.Items.Armor.Leather
	inherit Oct.Items.Restrictions2

	name = Leather Helmet III
    pluralName = Leather Helmets III
	buffScale = 3
	
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
            modifier = true
			min = 2
			max = 3
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 16
			max = 112
		}
	]

	salvage =
	[
		?(count == 3) {
			type IngredientItemSalvageComponent
			count = 15
		}
	]
}
{
	id Oct.Items.Recipes.Skins.Helmet.Leather.III
	type ItemRecipe
	
	type = <Oct.Items.Armor.Skins.Helmet.Leather.III>
	researchEntry = <Oct.Research.Skins.Helmet.Leather.III>
	commandType = CraftItemCommand
	craftingLoop = ClothCraftingLoop
	canFilterIngredients = true
	progressMultiplier = .4
	qualityDifficulty = 1.3
    skillCurve = .76
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Skins.Helmet.Leather.II>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Skins.Leather>
			count = 15
		}
	]
}
//////////// Tier IV ////////////
{
	id Oct.Items.Armor.Skins.Helmet.Leather.IV
	type InstancedItemTypeGenerator
	inherit Oct.Items.Armor.Skins.Helmet
    inherit Oct.Items.Armor.Leather
	inherit Oct.Items.Restrictions2
	
	name = Leather Helmet IV
    pluralName = Leather Helmets IV
	buffScale = 4
	
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
            modifier = true
			min = 2
			max = 4
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 18
			max = 136
		}
	]

	salvage =
	[
		?(count == 3) {
			type IngredientItemSalvageComponent
			count = 20
		}
	]
}
{
	id Oct.Items.Recipes.Skins.Helmet.Leather.IV
	type ItemRecipe
	
	type = <Oct.Items.Armor.Skins.Helmet.Leather.IV>
	researchEntry = <Oct.Research.Skins.Helmet.Leather.IV>
	commandType = CraftItemCommand
	craftingLoop = ClothCraftingLoop
	canFilterIngredients = true
	progressMultiplier = .4
	qualityDifficulty = 1.325
    skillCurve = .78
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Skins.Helmet.Leather.III>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Skins.Leather>
			count = 20
		}
	]
}
//////////// Tier V ////////////
{
	id Oct.Items.Armor.Skins.Helmet.Leather.V
	type InstancedItemTypeGenerator
	inherit Oct.Items.Armor.Skins.Helmet
    inherit Oct.Items.Armor.Leather
	inherit Oct.Items.Restrictions2
	
	name = Leather Helmet V
    pluralName = Leather Helmets V
	buffScale = 4
	
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
			modifier = true
			min = 3
			max = 4
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 20
			max = 150
		}
	]

	salvage =
	[
		?(count == 3) {
			type IngredientItemSalvageComponent
			count = 25
		}
	]
}
{
	id Oct.Items.Recipes.Skins.Helmet.Leather.V
	type ItemRecipe
	
	type = <Oct.Items.Armor.Skins.Helmet.Leather.V>
	researchEntry = <Oct.Research.Skins.Helmet.Leather.V>
	commandType = CraftItemCommand
	craftingLoop = ClothCraftingLoop
	canFilterIngredients = true
	progressMultiplier = .4
	qualityDifficulty = 1.35
    skillCurve = .8
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
    
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Skins.Helmet.Leather.IV>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Skins.Leather>
			count = 25
		}
	]
}