//////////// Tier II ////////////
{
	id Oct.Items.Armor.Plate.Pauldrons.Steel.II
	type InstancedItemType
	inherit Oct.Items.Armor.Plate.Pauldrons	
	inherit Oct.Items.Armor.Steel
    
	name = Steel Plate Pauldrons II
	buffScale = 5
    
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
			modifier = true
			min = 5
			max = 12
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 34
			max = 206
		}
	]
    
	salvageAt = <Oct.Props.Foundry>
	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.IronRods>
			count = 30
		}
	]
}
{
	id Oct.Items.Recipes.Plate.Pauldrons.Steel.II
	type ItemRecipe
	
	type = <Oct.Items.Armor.Plate.Pauldrons.Steel.II>
	researchEntry = <Oct.Research.Plate.Pauldrons.Steel.II>
	craftingLoop = MetalCraftingLoop
	commandType = CraftItemCommand
	progressMultiplier = .25
	qualityDifficulty = 1.63
    skillCurve = .83
    
    craftAt = 
    [
        <Oct.Props.Forge>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Plate.Pauldrons.Steel>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.IronRods>
			count = 30
		}
	]
}
//////////// Tier III ////////////
{
	id Oct.Items.Armor.Plate.Pauldrons.Steel.III
	type InstancedItemType
	inherit Oct.Items.Armor.Plate.Pauldrons	
	inherit Oct.Items.Armor.Steel
    
	name = Steel Plate Pauldrons III
	buffScale = 5
    
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
			modifier = true
			min = 6
			max = 12
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 36
			max = 220
		}
	]
    
	salvageAt = <Oct.Props.Foundry>
	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.IronRods>
			count = 35
		}
	]
}
{
	id Oct.Items.Recipes.Plate.Pauldrons.Steel.III
	type ItemRecipe
	
	type = <Oct.Items.Armor.Plate.Pauldrons.Steel.III>
	researchEntry = <Oct.Research.Plate.Pauldrons.Steel.III>
	craftingLoop = MetalCraftingLoop
	commandType = CraftItemCommand
	progressMultiplier = .25
	qualityDifficulty = 1.635
    skillCurve = .835
    
    craftAt = 
    [
        <Oct.Props.Forge>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Plate.Pauldrons.Steel.II>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.IronRods>
			count = 35
		}
	]
}
//////////// Tier IV ////////////
{
	id Oct.Items.Armor.Plate.Pauldrons.Steel.IV
	type InstancedItemType
	inherit Oct.Items.Armor.Plate.Pauldrons	
	inherit Oct.Items.Armor.Steel
    
	name = Steel Plate Pauldrons IV
	buffScale = 6
    
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
			modifier = true
			min = 7
			max = 13
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 38
			max = 234
		}
	]
    
	salvageAt = <Oct.Props.Foundry>
	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.IronRods>
			count = 40
		}
	]
}
{
	id Oct.Items.Recipes.Plate.Pauldrons.Steel.IV
	type ItemRecipe
	
	type = <Oct.Items.Armor.Plate.Pauldrons.Steel.IV>
	researchEntry = <Oct.Research.Plate.Pauldrons.Steel.IV>
	craftingLoop = MetalCraftingLoop
	commandType = CraftItemCommand
	progressMultiplier = .25
	qualityDifficulty = 1.64
    skillCurve = .84
    
    craftAt = 
    [
        <Oct.Props.Forge>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Plate.Pauldrons.Steel.III>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.IronRods>
			count = 40
		}
	]
}
//////////// Tier V ////////////
{
	id Oct.Items.Armor.Plate.Pauldrons.Steel.V
	type InstancedItemType
	inherit Oct.Items.Armor.Plate.Pauldrons	
	inherit Oct.Items.Armor.Steel
    
	name = Steel Plate Pauldrons V
	buffScale = 6
    
	stats =
	[
		{
			type InstancedItemStat
			name = Armor
			formula = QualityHalfDurability
			modifier = true
			min = 8
			max = 14
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 40
			max = 248
		}
	]
    
	salvageAt = <Oct.Props.Foundry>
	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.IronRods>
			count = 45
		}
	]
}
{
	id Oct.Items.Recipes.Plate.Pauldrons.Steel.V
	type ItemRecipe
	
	type = <Oct.Items.Armor.Plate.Pauldrons.Steel.V>
	researchEntry = <Oct.Research.Plate.Pauldrons.Steel.V>
	craftingLoop = MetalCraftingLoop
	commandType = CraftItemCommand
	progressMultiplier = .25
	qualityDifficulty = 1.645
    skillCurve = .845
    
    craftAt = 
    [
        <Oct.Props.Forge>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Armor.Plate.Pauldrons.Steel.IV>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.IronRods>
			count = 45
		}
	]
}