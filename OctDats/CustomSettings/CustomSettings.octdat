// Author: Satoru
// Version: 1.8
// Description: Changes settings to allow for different values

{
    id Oct.Settings.Game.DaysPerYear
    type DaysPerYearSliderSettingDefinition
    appendonly
    max = 16
}
{
    id Oct.Settings.Game.DifficultyRamp
    type SliderSettingDefinition
    appendonly
    max = 24
}
{
    id Oct.Settings.Game.DifficultMoment.Frequency
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.DifficultMoment.Variance
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.DifficultMoment.Scale
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.Petition.Difficulty
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.Difficulty.Needs.Scale
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.Desertion.Warning
    type SliderSettingDefinition
    appendonly
    min = 0
    max = 10
    step = .1
}
{
    id Oct.Settings.Game.AI.Wealth
    type SliderSettingDefinition
    appendonly
    min = .2
    max = 10
}
{
    id Oct.Settings.Game.TemperatureSensitivity.Comfort
    type SliderSettingDefinition
    appendonly
    max = 10
    step = .1
}
{
    id Oct.Settings.Game.TemperatureSensitivity.Survival
    type SliderSettingDefinition
    appendonly
    max = 10
    step = .1
}
{
    id Oct.Settings.Game.FoodDecayRate
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.RaidChance
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.RaidDifficulty
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.ImpChance
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.ImpDifficulty
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.KinChance
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.KinDifficulty
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.RabidChance
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.RabidDifficulty
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.DangerousAnimalsChance
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.DangerousAnimalsDifficulty
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.BanditChance
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.BanditDifficulty
    type SliderSettingDefinition
    appendonly
    max = 10
}
{
    id Oct.Settings.Game.PreachingRate
    type SliderSettingDefinition
    appendonly
    min = .1
    max = 10
    step = .1
}

{
    id Oct.Settings.Global.Graphics.MaxFramerate
    type MaximumFramerateChoiceSettingDefinition
    appendonly
    choices = 
    [
        $clear
        Unlimited
        30
        45
        59
        60
        72
        90
        120
        144
    ]
}
{
    id Oct.Settings.Global.Audio.Master
    type MixerVolumeSliderSettingDefinition
    step = 1
}
{
    id Oct.Settings.Global.Audio.Music
    type MixerVolumeSliderSettingDefinition
    step = 1
}
{
    id Oct.Settings.Global.Audio.UI
    type MixerVolumeSliderSettingDefinition
    step = 1
}
{
    id Oct.Settings.Global.Audio.InWorld
    type MixerVolumeSliderSettingDefinition
    step = 1
}
{
    id Oct.Settings.Global.Audio.Ambient
    type MixerVolumeSliderSettingDefinition
    step = 1
}

{
    id Oct.Settings.Global.Saves.AutoSave
    type AutoSaveFrequencySliderSettingDefinition
    max = 180
    step = 1
}
{
    id Oct.Settings.Global.AutoSaves.Keep
    type SliderSettingDefinition
    min = 2
    max = 60
    step = 1
}