{
	id Oct.Characters.Monsters.Lich
	type CharacterType
	inherit Oct.Characters.Monsters.Monster
	
	name = Lich
	plural = Liches
    
	runRate = .8
	corpseType = <Oct.Items.Corpse.Monsters.Lich>
	
    blood = SmallBlood
    bloodFountain = None
	reactToDamage = false

	attackRate = 1.5
    attackSpeed = .6
    rangedFirePos = vector(.2, .5, 1)
    meleeDamage = 8
    introAnim = Armature|Attract
    
    scale = 1
    
	rightAttacks = 
	[
		Armature|Attack
	]

	leftAttacks = 
	[
		Armature|Attack2
	]
    
    rangedAttackTalents =
    [
        {
            id Oct.Characters.Monsters.Lich.RangedAttack
            type TalentCharacterRangedAttackType
            animation = Armature|RangedAttack
            animationSpeed = .7
            damage = 15
            speed = 10
            fullbody = true
            lead = false
            fireAfter = 1.35
            completeAfter = 5
            cameraShake = .15
            
            projectileType =             
            {
                id Oct.Characters.Monsters.Lich.RangedAttack.Projectile
                type ProjectileType
                
                gravity = .3
                radius = .2
                hitCameraShake = .3
                hitCameraShakeRadius = 4
                foliageDamageScale = 6
                terrainDamageScale = .12
                propDamageScale = 1.5
                throughUntilDecay = true
                blockable = true
                major = true
				trailLength = 0
            
                hit = 
                {
                    type AudioClipHandle
                    path = /SFX/Magic/MagicArsenal/fireimpact04.wav
                    globalCooldown = 0.15
                    volume = .25
                }
                
                aoes = 
                [
                    {
                        type ProjectileTypeAOE
                        range = .5
                        damage = 15
                    }
                    {
                        type ProjectileTypeAOE
                        range = 1
                        damage = 10
                    }
                ]

				prefab =
				{
					type Prefab
					path = /Assets/LichMagic.fbx
					name = Lich Magic
					behavior = ProjectileBehavior
				}

                // effectType = 
                // {
                //     id Oct.Characters.Monsters.Lich.RangedAttack.Effect
                //     type EffectType
                //     name = BreacherAttack
                //     unityPrefab = BreacherAttack
            
                //     loop =
                //     {
                //         type AudioClipHandle
                //         path = /SFX/Magic/MagicArsenal/fireloop.wav
                //         volume = .25
                //     }
                // }
                
                landEffectType = 
                {
                    id Oct.Characters.Monsters.Lich.RangedAttack.Land
                    type EffectType
                    name = MagicMissileLand
                    unityPrefab = MagicMissileLand
                }
            }
        }
    ]
	
	prefab = 
	{
		type Prefab
		path = /Assets/Lich.fbx
		name = Lich
        behavior = CharacterBehavior
		
	}
	
	variants = 
	[
		{
			id Oct.Characters.Monsters.Lich.Basic
			type CharacterTypeVariant
			
			name = Lich
			names = 
			[
				Lich
			]
		}
	]
	
	stats = 
	[
		{
			id Oct.Characters.Monsters.Lich.Stats.MoveSpeed
			type StatType
			name = MoveSpeed
			min = 0
			max = 1000000
			initial = .75
			hidden = true
		}
		{
			id Oct.Characters.Monsters.Lich.Stats.Health
			type StatType	
			name = Health
			min = 0
			max = 100
			initial = 100
		}
	]
	
	audio = 
	[
		{
			type AudioBankHandle			
			name = Death
			handle = 
			{
				type AudioHandles
                audioChances = 
                [
                    {
                        type AudioHandleChance
                        chance = 1
                        audio =
                        {
                            type AudioClipHandle				
                            path = /SFX/LichDeath.wav
                        }
                    }
                ]
            }
		}
	]
	
	animations = 
	[
		{
			type AnimationTrack			
			name = Armature|Attack
			
			notifies = 
			[
				{
					type AudioNotify
					time = .75
					audio = 
					{
                        type AudioClipHandle
                        path = /SFX/LichAttack.wav
                        globalCooldown = 1
                    }
				}
                // {
                //     type VerbNotify
                //     time = 1.0625
                //     message = Damage
                // }
				// {
				// 	type WeaponTrailNotify					
				// 	time = .75
				// 	enabled = true
				// 	boneName = LeftHandSlot
                //     localRotation = rot(0, 0, 90)
                //     thickness = .25
				// }
				// {
				// 	type WeaponTrailNotify					
				// 	time = 1.25
				// 	enabled = false
				// 	boneName = LeftHandSlot
				// }
			]
		}
		{
			type AnimationTrack			
			name = Armature|RangedAttack
			
			notifies = 
			[
				{
					type AudioNotify
					time = 1
					audio = 
					{
                        type AudioClipHandle
                        path = /SFX/LichAttack2.wav
                        globalCooldown = 1
                    }
				}
			]
		}
		{
			type AnimationTrack			
			name = Armature|Attack2
			
			notifies = 
			[
				{
					type AudioNotify
					time = 0
					audio = 
					{
                        type AudioClipHandle
                        path = /SFX/LichAttack2.wav
                        globalCooldown = 1
                    }
				}
			]
		}
		{
			type AnimationTrack			
			name = Armature|React
			notifies = 
			[
				{
					type AudioNotify
					time = 0
					audio = 
					{
                        type AudioClipHandle
                        path = /SFX/LichHurt.wav
                        globalCooldown = 1
                    }
				}
			]
		}
	]
	
	// feet = 
    // [
    //     {
    //         id Oct.Characters.Monsters.Breacher.Feet.Left
    //         type CharacterTypeFootDef
    //         offset = vector(-.5, 0, 0)
    //         decal = <Oct.Decals.WildHoof.LeftFoot>
    //         scale = 5
    //     }
	// 	{
    //         id Oct.Characters.Monsters.Breacher.Feet.Right
    //         type CharacterTypeFootDef
    //         offset = vector(.5, 0, 0)
    //         decal = <Oct.Decals.WildHoof.LeftFoot>
    //         scale = 5
    //     }
	// ]
	
	instincts = 
	[
		{
			type RelaxInstinct
			timeFrom = 20
			timeTo = 30
		}
	]
}