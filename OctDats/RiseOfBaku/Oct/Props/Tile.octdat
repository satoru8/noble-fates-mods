{
	abstract
	id Oct.Props.BakuTiles
	type FloorPropType
	inherit Oct.Props.Audio.Wooden
	
	actorType = FloorPropActor
	category = <Oct.Props.Categories.Floors>
	selectionMode = Tile
	buildingMode = Props
    area = true
    layer = 9
	toolSort = 0

	buildItemFilter = <Oct.Items.Stackable.Stone>
	buildItemCount = 6
	repairItemFilter = <Oct.Items.Stackable.Stone>
    
    maxHealth = 100
    progressMultiplier = .75
    difficulty = .65
    skillCurve = .325
    
    minValue = 1
    maxValue = 5
    
    insulation = 15
    
    ignoreVisibility = true
    crateable = false
	
	buildPos = 
	[
		vector(0, 0, 0)
		vector(0, -1, 0)
		vector(0, -2, 0)
		vector(1, 0, 0)
		vector(-1, 0, 0)
		vector(0, 0, 1)
		vector(0, 0, -1)
		vector(1, -1, 0)
		vector(-1, -1, 0)
		vector(0, -1, 1)
		vector(0, -1, -1)
	]
    
    pathingLinks =
    [
        {
            type PropPathingLink
            pos = vector(1, 0, 0)
            linkClass = ClimbUpPropPathingLinkInstance
        }
        {
            type PropPathingLink
            pos = vector(-1, 0, 0)
            linkClass = ClimbUpPropPathingLinkInstance
        }
        {
            type PropPathingLink
            pos = vector(0, 0, 1)
            linkClass = ClimbUpPropPathingLinkInstance
        }
        {
            type PropPathingLink
            pos = vector(0, 0, -1)
            linkClass = ClimbUpPropPathingLinkInstance
        }
    ]
	
	clipOffset = -.5
	clipSpace = vector(0, 0, 1)
}
{
	id Oct.Props.BakuTiles.StoneX
	type FloorPropType
	inherit Oct.Props.BakuTiles
	
	name = Stone
    pluralName = Stone Floors
    toolName = Stone
	
	researchEntry = <Oct.Research.Building.Decoration.Baku>
	
	prefabs = 
	[
		{
			type Prefab
			path = /Assets/Tile/StoneFloor.fbx
			name = StoneX
            behavior = FloorPropBehavior

			sharedMaterials = 
			[
				{
					type OctDatSharedMaterial
					names = 
					[
						FloorBase
					]
					material = 
					{
						type Material

						_AlbedoMap = tex(\Assets\Tile\StoneFloor.png, bilinear, mips)

						_SMEAOMap = tex(\Assets\Tile\StoneFloorAO.png, bilinear, mips, linear)
						_NormalMap = tex(\Assets\Tile\StoneFloorNRM.png, bilinear, mips, normals)
					}
				}
			]
		}
	]
}
{
	id Oct.Props.BakuTiles.Grate
	type FloorPropType
	inherit Oct.Props.BakuTiles
	
	name = Grate
    pluralName = Grates
    toolName = Grate
	
	researchEntry = <Oct.Research.Building.Decoration.Baku>
	
	prefabs = 
	[
		{
			type Prefab
			path = /Assets/Props/Grate.fbx
			name = Grate
            behavior = FloorPropBehavior

			sharedMaterials = 
			[
				{
					type OctDatSharedMaterial
					names = 
					[
						Metal
					]
					material = <Oct.Props.Materials.Iron>
				}
			]
		}
	]
}
{
	id Oct.Props.BakuTiles.Skull
	type FloorPropType
	inherit Oct.Props.BakuTiles
	
	name = Skull Floor
    pluralName = Skull Floors
    toolName = Skulls
	
	researchEntry = <Oct.Research.Building.Decoration.Baku>
	
	prefabs = 
	[
		{
			type Prefab
			path = /Assets/Tile/SkullTile.fbx
			name = Skulls
            behavior = FloorPropBehavior

			sharedMaterials = 
			[
				{
					type OctDatSharedMaterial
					names = 
					[
						Skulls
					]
					material = 
					{
						type Material


						_NormalMap = tex(\Assets\Tile\SkullNormal.png, bilinear, mips, normals)
					}
				}
			]
		}
	]

	colorSlots = 
	[
		{
			type PropColorSlot
			name = Skulls
			defaultEntry = 1
            materials = 
            [
                Skulls
            ]
			palette = <Oct.Props.Palettes.Stone>
		}		
	]
}