//////////// No Buff ////////////
{
    abstract
	id Oct.Items.Weapons.Axe.NoBuff
	type WeaponItemType
    
	itemClass = InstancedItem
	category = <Oct.Items.Categories.Gear.Weapons.Axes>
	isMelee = true
    
    cosmeticPrefab = true
	
    equipmentSlot = Primary
	equipmentCosmetic = 
	{
		type StaticCharacterCosmetic
		inherit Oct.Items.Weapons.StaticCharacterCosmetic
	}
}
{
	id Oct.Items.Weapons.Hammer.NoBuff
	type WeaponItemType
    abstract
	
	itemClass = InstancedItem
	category = <Oct.Items.Categories.Gear.Weapons.Hammers>
	isMelee = true
	
    equipmentSlot = Primary
	cosmeticPrefab = true
	
	equipmentCosmetic = 
	{
		type StaticCharacterCosmetic
		inherit Oct.Items.Weapons.StaticCharacterCosmetic
	}
}
{
    abstract
	id Oct.Items.Weapons.Sword.NoBuff
	type WeaponItemType
	
	itemClass = InstancedItem
	category = <Oct.Items.Categories.Gear.Weapons.Swords>
	isMelee = true
    
    bitSlots =
    [
        <Oct.Items.Weapons.Sword.Slot.Handle>
        <Oct.Items.Weapons.Sword.Slot.Blade>
        <Oct.Items.Weapons.Sword.Slot.CrossGuard>
        <Oct.Items.Weapons.Sword.Slot.Pommel>
    ]
    
	equipmentSlot = Primary
	equipmentCosmetic = 
	{
		type StaticCharacterCosmetic
		inherit Oct.Items.Weapons.StaticCharacterCosmetic
	}
}
{
    abstract
	id Oct.Items.Weapons.Crossbow.Base.NoBuff
	type RangedWeaponItemType
	
	itemClass = InstancedItem
	category = <Oct.Items.Categories.Gear.Weapons.Crossbows>
	isMelee = false
	ammoType = <Oct.Items.Stackable.WoodenArrows>
	cosmeticPrefab = true
	equipmentSlot = Secondary
    
    ammoFilter = <Oct.Items.Categories.Misc.Ammo.Arrows>   
    attackType = <Oct.Items.Weapons.Crossbow.Attack>

	equipmentCosmetic = <Oct.Items.Weapons.Crossbow.Cosmetic>
    
	cosmeticPrefab = true
    cosmeticPrefabFloorHeight =  .025
    skinnedPrefabRotation = rot(90, 0, 0)
    iconCaptureRotation = rot(0, 180, 0)

	salvageAt = <Oct.Props.CraftingTable>
	
	prefab =
	{
		type Prefab
		path = /Art/Items/Wood/Crossbow/CrossbowIcon.fbx
		name = Crossbow Item
        behavior = InstancedItemBehavior
                
        sharedMaterials = 
        [
            {
                type OctDatSharedMaterial
                names = 
                [
                    Wood
                ]
                material = <Oct.Items.Materials.Wood>
            }
            {
                type OctDatSharedMaterial
                names = 
                [
                    Metal
                ]
                material = <Oct.Items.Materials.Bronze>
            }
            {
                type OctDatSharedMaterial
                names = 
                [
                    Bowstring
                ]
                material = <Oct.Items.Materials.String>
            }
        ]
	}
    
    partialPrefab =
    {
        type Prefab
		path = /Art/Items/Wood/Crossbow/CrossbowBlank.fbx
		name = Crossbow Blank
        behavior = InstancedItemBehavior
                
        sharedMaterials = 
        [
            {
                type OctDatSharedMaterial
                names = 
                [
                    Wood
                ]
                material = <Oct.Items.Materials.Wood>
            }
            {
                type OctDatSharedMaterial
                names = 
                [
                    Bowstring
                ]
                material = <Oct.Items.Materials.String>
            }
        ]
    }
	
	
	substances = 
    [
        <Oct.Items.Substances.Wood>
    ]
    
    bitSlots =
    [
        <Oct.Items.Weapons.Crossbow.Slot.Stock>
        <Oct.Items.Weapons.Crossbow.Slot.Mechanical>
        <Oct.Items.Weapons.Crossbow.Slot.String>
    ]
}
{
    abstract
	id Oct.Items.Weapons.Bow.NoBuff
	type RangedWeaponItemType
	
	itemClass = InstancedItem
	category = <Oct.Items.Categories.Gear.Weapons.Bows>
	isMelee = false
	ammoType = <Oct.Items.Stackable.WoodenArrows>
	cosmeticPrefab = true
	equipmentSlot = Secondary
    
    ammoFilter = <Oct.Items.Categories.Misc.Ammo.Arrows>
    drawnBone = LeftHandSlot 

	equipmentCosmetic = <Oct.Items.Weapons.Bow.Cosmetic>
	
	salvageAt = <Oct.Props.CraftingTable>

	substances = 
    [
        <Oct.Items.Substances.Wood>
    ]
}

//////////// Tier V Buff ////////////
{
	abstract
	id Oct.Items.Buffable.AttributeBuff.II
	type StatModifierInstancedItemBuffDefinition
	modifierMin = 2
	modifierMax = 7
}
{
	abstract
	id Oct.Items.Buffable.II
	type InstancedItemType
	
    dropSound = CommonDrop
    hasRarity = true
    canAutoJunk = true
	
	potentialBuffs =
	[
		{
			id Oct.Items.Buffable.MeleeBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.MeleeBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.RangedBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.RangedBuff
			inherit Oct.Items.Buffable.AttributeBuff.II

		}
		{
			id Oct.Items.Buffable.CookingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.CookingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.NursingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.NursingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.MiningBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.MiningBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.BuildingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.BuildingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.CleaningBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.CleaningBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.LoggingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.LoggingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.ForagingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.ForagingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.CraftingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.CraftingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.FarmingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.FarmingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.RanchingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.RanchingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
		{
			id Oct.Items.Buffable.HaulingBuff.II
			type StatModifierInstancedItemBuffDefinition
            inherit Oct.Items.Buffable.HaulingBuff
			inherit Oct.Items.Buffable.AttributeBuff.II
		}
	]
}