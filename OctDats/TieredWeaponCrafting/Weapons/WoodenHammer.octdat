//////////// Tier II ////////////
{
	id Oct.Items.Weapons.Hammer.Wood.II
	type WeaponItemType
	inherit Oct.Items.Weapons.Hammer.Wood
	
	name = Wooden Hammer II
    pluralName = Wooden Hammers II
	buffScale = 2
    
	stats =
	[
		?(name == Damage) {
			type InstancedItemStat
			min = 4
			max = 12
		}
		?(name == Value) {
			type InstancedItemStat
			min = 8
			max = 40
		}
	]

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 15
		}
	]
}
{
	id Oct.Items.Recipes.Hammer.Wood.II
	type ItemRecipe
	
	type = <Oct.Items.Weapons.Hammer.Wood.II>
	commandType = CraftItemCommand
	progressMultiplier = .65
	researchEntry = <Oct.Research.Smithing.Hammer.Wood.II>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = .925
    skillCurve = .65
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.Hammer.Wood>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 15
		}
	]
}
//////////// Tier III ////////////
{
	id Oct.Items.Weapons.Hammer.Wood.III
	type WeaponItemType
	inherit Oct.Items.Weapons.Hammer.Wood
	inherit Oct.Items.Weapons.Melee.Restrictions
	
	name = Wooden Hammer III
    pluralName = Wooden Hammers III
	buffScale = 3
    
	stats =
	[
		?(name == Damage) {
			type InstancedItemStat
			min = 6
			max = 14
		}
		?(name == Value) {
			type InstancedItemStat
			min = 12
			max = 50
		}
	]

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 20
		}
	]
}
{
	id Oct.Items.Recipes.Hammer.Wood.III
	type ItemRecipe
	
	type = <Oct.Items.Weapons.Hammer.Wood.III>
	commandType = CraftItemCommand
	progressMultiplier = .65
	researchEntry = <Oct.Research.Smithing.Hammer.Wood.III>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = .925
    skillCurve = .65
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.Hammer.Wood.II>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 20
		}
	]
}
//////////// Tier IV ////////////
{
	id Oct.Items.Weapons.Hammer.Wood.IV
	type WeaponItemType
	inherit Oct.Items.Weapons.Hammer.Wood
	inherit Oct.Items.Weapons.Melee.Restrictions
	
	name = Wooden Hammer IV
    pluralName = Wooden Hammers IV
	buffScale = 3
    
	stats =
	[
		?(name == Damage) {
			type InstancedItemStat
			min = 8
			max = 16
		}
		?(name == Value) {
			type InstancedItemStat
			min = 16
			max = 60
		}
	]

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 25
		}
	]
}
{
	id Oct.Items.Recipes.Hammer.Wood.IV
	type ItemRecipe
	
	type = <Oct.Items.Weapons.Hammer.Wood.IV>
	commandType = CraftItemCommand
	progressMultiplier = .65
	researchEntry = <Oct.Research.Smithing.Hammer.Wood.IV>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = .925
    skillCurve = .65
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.Hammer.Wood.III>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 25
		}
	]
}
//////////// Tier V ////////////
{
	id Oct.Items.Weapons.Hammer.Wood.V
	type WeaponItemType
	inherit Oct.Items.Weapons.Hammer.NoBuff
    inherit Oct.Items.Weapons.Wood
	inherit Oct.Items.Buffable.II
	inherit Oct.Items.Weapons.Melee.Restrictions
	
	name = Wooden Hammer V
    pluralName = Wooden Hammers V

	buffScale = 4
    
    bitSlots =
    [
        <Oct.Items.Weapons.Hammer.Wood.Slot.Grip>
        <Oct.Items.Weapons.Hammer.Wood.Slot.Head>
    ]
    
    substances = 
    [
        <Oct.Items.Substances.Wood>
    ]
    
	stats =
	[
		{
			type InstancedItemStat
			name = Damage
			formula = QualityHalfDurability
			min = 10
			max = 18
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 20
			max = 72
		}
	]

	equipmentCosmetic = 
	{
		type StaticCharacterCosmetic
		inherit Oct.Items.Weapons.StaticCharacterCosmetic
	}
    
	prefab =
	{
		type Prefab
		path = /Art/Items/Weapons/Hammer/Wood/WoodenHammer.fbx
		name = Wood Hammer Item
        behavior = InstancedItemBehavior
	}
		
	salvageAt = <Oct.Props.CraftingTable>

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 30
		}
	]
}
{
	id Oct.Items.Recipes.Hammer.Wood.V
	type ItemRecipe
	
	type = <Oct.Items.Weapons.Hammer.Wood.V>
	commandType = CraftItemCommand
	progressMultiplier = .65
	researchEntry = <Oct.Research.Smithing.Hammer.Wood.V>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = .925
    skillCurve = .65
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.Hammer.Wood.IV>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 30
		}
	]
}