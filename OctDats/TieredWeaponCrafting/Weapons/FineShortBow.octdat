//////////// Tier II ////////////
{
	id Oct.Items.Weapons.ShortBow.Fine.II
	type RangedWeaponItemType
	inherit Oct.Items.Weapons.ShortBow.Fine
	
	name = Fine Short Bow II
    pluralName = Fine Short Bows II
	buffScale = 5
    
	stats =
	[
		?(name == Damage) {
			type InstancedItemStat
			min = 10
			max = 37
		}
		?(name == Value) {
			type InstancedItemStat
			min = 28
			max = 276
		}
	]

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 35
		}
	]
}
{
	id Oct.Items.Recipes.ShortBow.Fine.II
	type ItemRecipe
	
	type = <Oct.Items.Weapons.ShortBow.Fine.II>
	commandType = CraftItemCommand
	progressMultiplier = .4
	researchEntry = <Oct.Research.Crafting.ShortBow.Fine.II>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = 1.85
    skillCurve = 1.3
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.ShortBow.Fine>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 35
		}
	]
}
//////////// Tier III ////////////
{
	id Oct.Items.Weapons.ShortBow.Fine.III
	type RangedWeaponItemType
	inherit Oct.Items.Weapons.ShortBow.Fine
	inherit Oct.Items.Weapons.Ranged.Restrictions
	
	name = Fine Short Bow III
    pluralName = Fine Short Bows III
	buffScale = 6
    
	stats =
	[
		?(name == Damage) {
			type InstancedItemStat
			min = 11
			max = 38
		}
		?(name == Value) {
			type InstancedItemStat
			min = 34
			max = 298
		}
	]

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 40
		}
	]
}
{
	id Oct.Items.Recipes.ShortBow.Fine.III
	type ItemRecipe
	
	type = <Oct.Items.Weapons.ShortBow.Fine.III>
	commandType = CraftItemCommand
	progressMultiplier = .4
	researchEntry = <Oct.Research.Crafting.ShortBow.Fine.III>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = 1.7
    skillCurve = 1.275
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.ShortBow.Fine.II>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 40
		}
	]
}
//////////// Tier IV ////////////
{
	id Oct.Items.Weapons.ShortBow.Fine.IV
	type RangedWeaponItemType
	inherit Oct.Items.Weapons.ShortBow.Fine
	inherit Oct.Items.Weapons.Ranged.Restrictions
	
	name = Fine Short Bow IV
    pluralName = Fine Short Bows IV
	buffScale = 7
    
	stats =
	[
		?(name == Damage) {
			type InstancedItemStat
			min = 12
			max = 39
		}
		?(name == Value) {
			type InstancedItemStat
			min = 38
			max = 332
		}
	]

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 45
		}
	]
}
{
	id Oct.Items.Recipes.ShortBow.Fine.IV
	type ItemRecipe
	
	type = <Oct.Items.Weapons.ShortBow.Fine.IV>
	commandType = CraftItemCommand
	progressMultiplier = .4
	researchEntry = <Oct.Research.Crafting.ShortBow.Fine.IV>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = 1.55
    skillCurve = 1.25
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.ShortBow.Fine.III>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 45
		}
	]
}
//////////// Tier V ////////////
{
	id Oct.Items.Weapons.ShortBow.Fine.V
	type RangedWeaponItemType
	inherit Oct.Items.Weapons.Bow.NoBuff
    inherit Oct.Items.Weapons.Wood.Fine
	inherit Oct.Items.Buffable.II
	inherit Oct.Items.Weapons.Ranged.Restrictions
	
	name = Fine Short Bow V
    pluralName = Fine Short Bows V

	buffScale = 8
    
	stats =
	[
		{
			type InstancedItemStat
			name = Damage
			formula = QualityHalfDurability
			min = 13
			max = 40
		}
		{
			type InstancedItemStat
			name = Value
			formula = QualityQualityHalfDurability
			min = 42
			max = 376
		}
	]

    bitSlots =
    [
        <Oct.Items.Weapons.Bow.Fine.Slot.End>
        <Oct.Items.Weapons.Bow.Fine.Slot.Grip>
        <Oct.Items.Weapons.Bow.Fine.Slot.InBetween>
        <Oct.Items.Weapons.Bow.Fine.Slot.String>
    ]
    
    prefabBits =
    [
        <Oct.Items.Weapons.Bow.Fine.End.Razor>
        <Oct.Items.Weapons.Bow.Fine.Grip.Razor>
        <Oct.Items.Weapons.Bow.Fine.InBetween.Razor>
        <Oct.Items.Weapons.Bow.Fine.String.Thin>
    ]
    
    iconColorSlot = <Oct.Items.Weapons.Bow.Fine.Slot.End>

	salvage =
	[
		{
			type ExplicitItemSalvageComponent
			itemType = <Oct.Items.Stackable.Wood>
			count = 50
		}
	]
}
{
	id Oct.Items.Recipes.ShortBow.Fine.V
	type ItemRecipe
	
	type = <Oct.Items.Weapons.ShortBow.Fine.V>
	commandType = CraftItemCommand
	progressMultiplier = .4
	researchEntry = <Oct.Research.Crafting.ShortBow.Fine.V>
	craftingLoop = WoodCraftingLoop
	qualityDifficulty = 1.4
    skillCurve = 1.225
    
    craftAt = 
    [
        <Oct.Props.CraftingTable>
    ]
	
	ingredients =
	[
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Weapons.ShortBow.Fine.IV>
			count = 1
		}
		{
			type ItemRecipeIngredient
			filter = <Oct.Items.Stackable.Wood>
			count = 50
		}
	]
}